/**
 * @file coupled/NodeSwitch.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_COUPLED_NODE_SWITCH_HPP
#define ARTIS_TRAFFIC_MICRO_COUPLED_NODE_SWITCH_HPP

#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <artis-traffic/micro/core/Node.hpp>
#include <artis-traffic/micro/utils/Switch.hpp>

namespace artis::traffic::micro::coupled {

struct NodeSwitchParameters {
  artis::traffic::micro::core::NodeParameters node_parameters;
  artis::traffic::micro::utils::SwitchParameters switch_parameters;
};

class NodeSwitchGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime, NodeSwitchParameters> {
public:
  struct submodels {
    enum values {
      NODE, SWITCH
    };
  };
  struct inputs {
    enum values {
      IN, OPEN, CLOSE, STATE
    };
  };
  struct outputs {
    enum values {
      OPEN, CLOSE, STATE, OUT
    };
  };

  NodeSwitchGraphManager(
    artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
    const NodeSwitchParameters &parameters,
    const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime, NodeSwitchParameters>(
      coordinator, parameters, graph_parameters),
    _node("node", parameters.node_parameters),
    _switch("switch", parameters.switch_parameters) {
    add_child(submodels::NODE, &_node);
    add_child(submodels::SWITCH, &_switch);

    coordinator->input_ports({{inputs::IN,    "in"},
                              {inputs::OPEN,  "open"},
                              {inputs::CLOSE, "close"},
                              {inputs::STATE, "state"}});
    coordinator->output_ports({{outputs::OPEN,  "open"},
                               {outputs::CLOSE, "close"},
                               {outputs::STATE, "state"}});

    unsigned int index = 0;

    for (const std::string &output_name: parameters.switch_parameters.outputs) {
      coordinator->output_port({outputs::OUT + index, output_name});
      ++index;
    }

    in({coordinator, inputs::IN})
      >> in({&_node, artis::traffic::micro::core::Node::inputs::IN});
    in({coordinator, inputs::OPEN})
      >> in({&_node, artis::traffic::micro::core::Node::inputs::OPEN});
    in({coordinator, inputs::CLOSE})
      >> in({&_node, artis::traffic::micro::core::Node::inputs::CLOSE});
    in({coordinator, inputs::STATE})
      >> in({&_node, artis::traffic::micro::core::Node::inputs::STATE});

    out({&_node, artis::traffic::micro::core::Node::outputs::OPEN})
      >> out({coordinator, outputs::OPEN});
    out({&_node, artis::traffic::micro::core::Node::outputs::CLOSE})
      >> out({coordinator, outputs::CLOSE});
    out({&_node, artis::traffic::micro::core::Node::outputs::STATE})
      >> out({coordinator, outputs::STATE});
    out({&_node, artis::traffic::micro::core::Node::outputs::OUT})
      >> in({&_switch, artis::traffic::micro::utils::Switch::inputs::IN});

    for (unsigned int i = 0; i < (unsigned int) parameters.switch_parameters.outputs.size(); ++i) {
      out({&_switch, artis::traffic::micro::utils::Switch::outputs::OUT + i})
        >> out({coordinator, outputs::OUT + i});
    }
  }

  ~NodeSwitchGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Node,
    artis::traffic::micro::core::NodeParameters> _node;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::Switch,
    artis::traffic::micro::utils::SwitchParameters> _switch;
};

}

#endif