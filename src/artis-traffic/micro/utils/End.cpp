/**
 * @file End.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/Data.hpp>
#include <artis-traffic/micro/utils/End.hpp>

namespace artis::traffic::micro::utils {

void End::dconf(const artis::traffic::core::Time &t,
                const artis::traffic::core::Time &e,
                const artis::traffic::core::Bag &bag) {
  dint(t);
  dext(t, e, bag);
}

void End::dint(const artis::traffic::core::Time & /* t */) {
  switch (_phase) {
    case Phase::INIT: {
      _phase = Phase::SEND_OPEN;
      break;
    }
    case Phase::SEND_OPEN: {
      _phase = Phase::OPEN;
      break;
    }
    case Phase::OPEN:
      break;
  }
}

void End::dext(const artis::traffic::core::Time & /* t */,
               const artis::traffic::core::Time & /* e */,
               const artis::traffic::core::Bag &bag) {
  std::for_each(bag.begin(), bag.end(),
                [this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {

                    assert(_phase == Phase::OPEN);

                  }
                });
}

void End::start(const artis::traffic::core::Time & /* t */) {
  _phase = Phase::INIT;
}

artis::traffic::core::Time End::ta(const artis::traffic::core::Time & /* t */) const {
  if (_phase == Phase::INIT or _phase == Phase::SEND_OPEN) {
    return 0;
  } else {
    return artis::common::DoubleTime::infinity;
  }
}

artis::traffic::core::Bag End::lambda(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Bag bag;

  if (_phase == Phase::SEND_OPEN) {
    for (int i = -1; i < 1; i++) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN, core::open_data{-1, i}));
    }
  }
  return bag;
}

artis::common::event::Value End::observe(const artis::traffic::core::Time & /* t */,
                                         unsigned int /* index */) const {
  return artis::common::event::Value();
}

}