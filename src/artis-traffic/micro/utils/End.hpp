/**
 * @file End.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_END_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_END_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>

namespace artis::traffic::micro::utils {

class End
  : public artis::pdevs::Dynamics<common::DoubleTime, End> {
public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OPEN
    };
  };

  End(const std::string &name,
      const artis::pdevs::Context<common::DoubleTime, End> &context)
    : artis::pdevs::Dynamics<common::DoubleTime, End>(name, context) {
    input_ports({{inputs::IN, "in"}});
    output_ports({{outputs::OPEN, "open"}});
  }

  ~End() override = default;

  void dconf(const artis::traffic::core::Time & /* t* */,
             const artis::traffic::core::Time & /* e */,
             const artis::traffic::core::Bag & /* bag */) override;

  void dint(const artis::traffic::core::Time & /* t */) override;

  void dext(const artis::traffic::core::Time & /* t */,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t, unsigned int index) const override;

private:
  struct Phase {
    enum values {
      INIT,
      OPEN,
      SEND_OPEN
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case OPEN:
          return "OPEN";
        case SEND_OPEN:
          return "SEND_OPEN";
      }
      return "";
    }
  };

// state
  Phase::values _phase;
};

}

#endif