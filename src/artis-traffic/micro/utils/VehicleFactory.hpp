/**
 * @file tests/VehicleFactory.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_TESTS_VEHICLE_FACTORY_HPP
#define ARTIS_TRAFFIC_MICRO_TESTS_VEHICLE_FACTORY_HPP

#include <artis-traffic/micro/core/Vehicle.hpp>

struct VehicleFactory {
  artis::traffic::micro::core::Vehicle build(unsigned int id, const std::vector<unsigned int> &path) const {
    return {id, 4.5, 3, 0, 10, 1, 1, 1, path, {artis::traffic::micro::core::VehicleData::State::STOPPED}};
  }
};

#endif //ARTIS_TRAFFIC_MICRO_TESTS_VEHICLE_FACTORY_HPP