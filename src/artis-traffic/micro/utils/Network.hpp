/**
 * @file artis-traffic/micro/utils/Network.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_NETWORK_HPP
#define ARTIS_TRAFFIC_MICRO_NETWORK_HPP

#include <map>
#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Stop.hpp>

namespace artis::traffic::micro::utils {

struct Entity {
  std::string ID;
};

template<typename Data>
struct Link : Entity {
  Data data;
};

template<typename Data>
struct Node : Entity {
  Data data;
};

template<typename Data>
struct Junction : Entity {
  Data data;
};

template<typename Data>
struct Stop : Entity {
  Data data;
};

template<typename Data>
struct Input : Entity {
  Data data;
};

template<typename Data>
struct Output : Entity {
  Data data;
};

struct InputData {
  std::string id;
  unsigned int start_index{};
  double min{};
  double mean{};
  double stddev{};
  unsigned long seed{};
  std::vector<std::vector<unsigned int> > paths;
  std::string out_port_id;

  void parse(const nlohmann::json &data) {
    id = data["id"].get<std::string>();
    start_index = data["start index"].get<unsigned int>();
    min = data["min"].get<double>();
    mean = data["mean"].get<double>();
    stddev = data["stddev"].get<double>();
    seed = data["seed"].get<unsigned long>();
    for (const nlohmann::json &path: data["paths"]) {
      paths.emplace_back();
      for (const nlohmann::json &choice: path) {
        paths.back().push_back(choice.get<unsigned int>());
      }
    }
    for (auto &elem: data["output_ports_id"])
      out_port_id = elem;
//    out_port_id = data["output_port_id"].get<std::string>();
  }
};

struct OutputData {
  enum Type {
    NONE, COUNTER, END
  };

  Type type{NONE};
  std::string id;
  std::string input_port_id;

  void parse(const nlohmann::json &data) {
    id = data["id"].get<std::string>();
//    input_port_id = data["input_port_id"].get<std::string>();
    for (auto &elem: data["input_ports_id"])
      input_port_id = elem;
    type = END;
  }
};

struct NodeData {
  std::string id;
  double open_duration{};
  double close_duration{};
  double occupied_duration{};
  double delay_start{};
  std::vector<std::string> input_ports_id;
  std::vector<std::string> output_ports_id;
  unsigned int in_number{};
  unsigned int out_number{};

  void parse(const nlohmann::json &data) {
    id = data["id"].get<std::string>();;
    open_duration = data["open duration"].get<double>();
    close_duration = data["close duration"].get<double>();
    occupied_duration = data["occupied_duration"].get<double>();
    delay_start = data["delay start"].get<double>();
    for (auto &elem: data["input_ports_id"])
      input_ports_id.push_back(elem);
    in_number = input_ports_id.size();
    for (auto &elem: data["output_ports_id"])
      output_ports_id.push_back(elem);
    out_number = output_ports_id.size();
  }
};

struct LinkData {
  std::string id;
  double max_speed{};
  double length{};
  double length_threshold{};
  unsigned int downstream_link_number{};
  unsigned int concurrent_link_number{};
  std::vector<std::string> concurrent_links_id;
  std::vector<std::string> concurrent_stops_id;
  std::vector<std::string> downstream_links_id;
  std::vector<bool> priorities;
  unsigned int concurrent_stop_number{};
  std::vector<std::string> input_ports_id;
  std::vector<std::string> output_ports_id;
  std::vector<std::string> merged_links_id;
  std::vector<std::vector<std::vector<double>>> merged_links_coords;

  void parse(const nlohmann::json &data) {
    id = data["id"].get<std::string>();
    max_speed = data["max_speed"].get<double>();
    length = data["length"].get<double>();
    length_threshold = 7.5;
    //downstream link number
    for (auto &elem: data["concurrent_links_id"])
      concurrent_links_id.push_back(elem);
    concurrent_link_number = concurrent_links_id.size();
    for (auto &elem: data["concurrent_stops_id"])
      concurrent_stops_id.push_back(elem);
    concurrent_stop_number = concurrent_stops_id.size();
    for (auto &elem: data["downstream_links_id"])
      downstream_links_id.push_back(elem);
    downstream_link_number = downstream_links_id.size();
    for (auto &elem: data["priorities"])
      priorities.push_back(elem);
    for (auto &elem: data["input_ports_id"])
      input_ports_id.push_back(elem);
    for (auto &elem: data["output_ports_id"]) {
      output_ports_id.push_back(elem);
      if (std::string(elem).find("end") == 0) {
        downstream_link_number++;
      }
    }
    for (auto &elem: data["merged_links_id"]) {
      merged_links_id.push_back(elem);
    }
    for (auto &elem: data["merged_links_coords"]) {
      std::vector<std::vector<double>> L;

      for (auto &a: elem) {
        std::vector<double> M;

        for (auto &b: a) {
          M.push_back(b.get<double>());
        }
        L.push_back(M);
      }
      merged_links_coords.push_back(L);
    }
  }

  core::LinkParameters to_parameters() const {
    return {max_speed, length, downstream_link_number, concurrent_link_number, concurrent_stop_number, priorities};
  }
};

struct JunctionData {
  std::string id;
  std::vector<std::string> input_ports_id;
  std::vector<std::string> output_ports_id;
  unsigned int in_number{};
  unsigned int out_number{};

  void parse(const nlohmann::json &data) {
    id = data["id"].get<std::string>();
    for (auto &elem: data["input_ports_id"])
      input_ports_id.push_back(elem);
    in_number = input_ports_id.size();
    for (auto &elem: data["output_ports_id"])
      output_ports_id.push_back(elem);
    out_number = output_ports_id.size();
  }
};

struct StopData {
  std::string id;
  double threshold{};
  std::vector<double> exits_links_speeds;
  unsigned int concurrent_link_number{};
  unsigned int concurrent_stop_number{};
  std::vector<std::string> concurrent_links_id;
  std::vector<std::string> input_ports_id;
  std::vector<std::string> output_ports_id;
  std::vector<std::string> concurrent_stops_id;
  std::vector<bool> priorities;

  void parse(const nlohmann::json &data) {
    id = data["id"].get<std::string>();
    threshold = 7.5;
    for (auto &elem: data["exits_links_speeds"]) {
      exits_links_speeds.push_back(elem);
    }
    for (auto &elem: data["concurrent_links_id"]) {
      concurrent_links_id.push_back(elem);
    }
    concurrent_link_number = concurrent_links_id.size();
    for (auto &elem: data["concurrent_stops_id"]) {
      concurrent_stops_id.push_back(elem);
    }
    concurrent_stop_number = concurrent_stops_id.size();
    for (auto &elem: data["input_ports_id"]) {
      input_ports_id.push_back(elem);
    }
    for (auto &elem: data["output_ports_id"]) {
      output_ports_id.push_back(elem);
    }
    for (auto &elem: data["priorities"]) {
      priorities.push_back(elem);
    }

  }

    core::StopParameters to_parameters() const {
        return {exits_links_speeds, concurrent_link_number, concurrent_stop_number,
                priorities};
    }
};

template<typename InputData, typename OutputData, typename NodeData, typename JunctionData,
  typename StopData, typename LinkData>
struct Network {
  using input_type = Input<InputData>;
  using output_type = Output<OutputData>;
  using node_type = Node<NodeData>;
  using junction_type = Junction<JunctionData>;
  using stop_type = Stop<StopData>;
  using link_type = Link<LinkData>;

  std::map<std::string, input_type> inputs;
  std::map<std::string, output_type> outputs;
  std::map<std::string, node_type> nodes;
  std::map<std::string, junction_type> junctions;
  std::map<std::string, stop_type> stops;
  std::map<std::string, link_type> links;
};

}

#endif //ARTIS_TRAFFIC_MICRO_NETWORK_HPP
