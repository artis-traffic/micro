/**
 * @file traffic/micro/utils/JSONNetworkView.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_JSON_NETWORK_VIEW_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_JSON_NETWORK_VIEW_HPP

#include "JSONNetworkGraphManager.hpp"
#include "VehicleFactory.hpp"

namespace artis::traffic::micro::utils {

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
class JSONLinkView : public artis::traffic::core::View {
public:
  JSONLinkView(const Network &network) {
    int i = 0;

    for (const auto &p: network.links) {
      selector("Link_" + p.first + ":vehicle_number",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
                Link::vars::VEHICLE_NUMBER});
      selector("Link_" + p.first + ":vehicle_positions",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
                Link::vars::VEHICLE_POSITIONS});
      selector("Link_" + p.first + ":vehicle_speed",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
                Link::vars::VEHICLE_SPEEDS});
      selector("Link_" + p.first + ":vehicle_indexes",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
                Link::vars::VEHICLE_INDEXES});
      selector("Link_" + p.first + ":event_count",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                       Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
                Link::vars::EVENT_COUNT});
      i++;
    }
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters, class Network,
  typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
class JSONStopView : public artis::traffic::core::View {
public:
  JSONStopView(const Network &network) {
    int i = 0;

    for (const auto &p: network.stops) {
      selector("Stop_" + p.first + ":vehicle_index",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::STOP + i,
                Stop::vars::VEHICLE_INDEX});
      i++;
    }
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters, class Network,
  typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
class JSONJunctionView : public artis::traffic::core::View {
public:
  JSONJunctionView(const Network &network) {
    int i = 0;

    for (const auto &p: network.junctions) {
      selector("Junction_" + p.first + ":vehicle_index",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::JUNCTION + i,
                artis::traffic::micro::core::Junction<Vehicle>::vars::VEHICLE_INDEX});
      i++;
    }
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters, class Network,
  typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
class JSONGeneratorView : public artis::traffic::core::View {
public:
  JSONGeneratorView(const Network &network) {
    int i = 0;

    for (const auto &p: network.inputs) {
      selector("Generator_" + p.first + ":counter",
               {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::GENERATOR +
                i, artis::traffic::micro::utils::Generator<VehicleFactory>::vars::COUNTER});
      i++;
    }
  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_UTILS_JSON_NETWORK_VIEW_HPP
