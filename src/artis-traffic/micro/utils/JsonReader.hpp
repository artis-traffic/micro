/**
 * @file artis-traffic/micro/utils/JsonReader.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_JSON_READER_HPP
#define ARTIS_TRAFFIC_MICRO_JSON_READER_HPP

//#include <artis-traffic/utils/JsonReader.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/utils/Network.hpp>

#include <iostream>
#include <vector>

#include <nlohmann/json.hpp>

namespace artis::traffic::micro::utils {

template<typename Network>
class AbstractJsonReader {
public:
  AbstractJsonReader() = default;

  const Network &network() const { return _network; }

  Network &network() { return _network; }

  void parse_inputs(const nlohmann::json &data) {
    const nlohmann::json &inputs = data["inputs"];

    for (const nlohmann::json &input: inputs) {
      typename Network::input_type new_input{{input["id"].get<std::string>()},
                                             {}};

      new_input.data.parse(input);
      _network.inputs[new_input.ID] = new_input;
    }
  }

  void parse_outputs(const nlohmann::json &data) {
    const nlohmann::json &outputs = data["outputs"];

    for (const nlohmann::json &output: outputs) {
      typename Network::output_type new_output{{output["id"].get<std::string>()},
                                               {}};

      new_output.data.parse(output);
      _network.outputs[new_output.ID] = new_output;
    }
  }

  void parse_nodes(const nlohmann::json &data) {
    const nlohmann::json &nodes = data["nodes"];

    for (const nlohmann::json &node: nodes) {
      typename Network::node_type new_node{{node["id"].get<std::string>()},
                                           {}};

      new_node.data.parse(node);
      _network.nodes[new_node.ID] = new_node;
    }
  }

  void parse_junctions(const nlohmann::json &data) {
    const nlohmann::json &junctions = data["junctions"];

    for (const nlohmann::json &junction: junctions) {
      typename Network::junction_type new_junction{{junction["id"].get<std::string>()},
                                                   {}};

      new_junction.data.parse(junction);
      _network.junctions[new_junction.ID] = new_junction;
    }
  }

  void parse_stops(const nlohmann::json &data) {
    const nlohmann::json &stops = data["stops"];

    for (const nlohmann::json &stop: stops) {
      typename Network::stop_type new_stop{{stop["id"].get<std::string>()},
                                           {}};

      new_stop.data.parse(stop);
      _network.stops[new_stop.ID] = new_stop;
    }
  }

  void parse_links(const nlohmann::json &data) {
    const nlohmann::json &links = data["links"];

    for (const nlohmann::json &link: links) {
      typename Network::link_type new_link{{link["id"].get<std::string>()},
                                           {}};

      new_link.data.parse(link);
      _network.links[new_link.ID] = new_link;
    }
  }

  virtual void parse_network(const std::string &str) {
    nlohmann::json data = nlohmann::json::parse(str);
    std::string level_str = data["level"].get<std::string>();

    parse_inputs(data);
    parse_outputs(data);
    parse_nodes(data);
    parse_junctions(data);
    parse_stops(data);
    parse_links(data);
  }

protected:
  Network _network;
};

typedef artis::traffic::micro::utils::Network<InputData, OutputData, NodeData, JunctionData, StopData, LinkData> MicroNetwork;

typedef artis::traffic::micro::utils::AbstractJsonReader<MicroNetwork> JsonReader;

}

#endif //ARTIS_TRAFFIC_MICRO_JSON_READER_HPP
