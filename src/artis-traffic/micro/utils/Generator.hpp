/**
 * @file utils/Generator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_GENERATOR_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_GENERATOR_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include <artis-traffic/core/Base.hpp>

#include <artis-traffic/micro/core/Vehicle.hpp>

#include <random>

namespace artis::traffic::micro::utils {

struct GeneratorParameters {
  unsigned int start_index;
  double min;
  double mean;
  double stddev;
  unsigned long seed;
  std::vector<std::vector<unsigned int> > paths;
};

template<class VehicleFactory>
class Generator
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Generator<VehicleFactory>, GeneratorParameters> {
public:
  struct inputs {
    enum values {
      OPEN, CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
      COUNTER, CLOSE
    };
  };

  Generator(const std::string &name,
            const artis::pdevs::Context<artis::common::DoubleTime,
              Generator,
              GeneratorParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Generator<VehicleFactory>, GeneratorParameters>(name, context),
    _start_index(context.parameters().start_index), _min(context.parameters().min),
    _paths(context.parameters().paths),
    _distribution(context.parameters().mean, context.parameters().stddev),
    _path_distribution(0, context.parameters().paths.size() - 1) {
    _generator.seed(context.parameters().seed);

    this->input_ports({{inputs::CLOSE, "close"},
                       {inputs::OPEN,  "open"}});
    this->output_ports({{outputs::OUT, "out"}});
    this->observables({{vars::COUNTER, "counter"},
                       {vars::CLOSE,   "close"}});
  }

  ~Generator() override = default;

  void dint(const artis::traffic::core::Time &t) override {
    ++_index;
    if (_paths.size() > 1) {
      _path_index = _path_distribution(_generator);
    } else {
      _path_index = 0;
    }

    _sigma = std::ceil(_distribution(_generator));
    _sigma = _sigma <= _min ? _min : _sigma;
    _sigma = std::ceil(t + _sigma) - t;
  }

  void dext(const artis::traffic::core::Time &t, const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag &bag) override {
    std::for_each(bag.begin(), bag.end(),
                  [this, t](const artis::traffic::core::ExternalEvent &e) {
                    if (e.on_port(inputs::CLOSE)) {

                      // assert(not _next_close);

                      _next_close = true;
                      _sigma = artis::common::DoubleTime::infinity;
                    } else if (e.on_port(inputs::OPEN)) {

                      assert(_next_close);

                      _next_close = false;
                      _sigma = std::ceil(_distribution(_generator));
                      _sigma = _sigma <= _min ? _min : _sigma;
                      _sigma = std::ceil(t + _sigma) - t;
                    }
                  });
  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _index = _start_index;
    if (_paths.size() > 1) {
      _path_index = _path_distribution(_generator);
    } else {
      _path_index = 0;
    }
    _next_close = true;
    _sigma = artis::common::DoubleTime::infinity;
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    return _sigma;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
    artis::traffic::core::Bag bag;

    if (t > 0 and not _next_close) {
      if (_paths.empty()) {
        bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, _vehicle_factory.build(_index, {})));
      } else {
        bag.push_back(
          artis::traffic::core::ExternalEvent(outputs::OUT, _vehicle_factory.build(_index, _paths.at(_path_index))));
      }
#ifdef WITH_TRACE
      common::Trace<common::DoubleTime>::trace()
          << common::TraceElement<common::DoubleTime>(get_name(), t,
                                                      common::FormalismType::PDEVS,
                                                      common::FunctionType::LAMBDA,
                                                      common::LevelType::USER)
          << "vehicle = " << vehicle.to_string();
      common::Trace<common::DoubleTime>::trace().flush();
#endif

    }
    return bag;
  }

  artis::common::event::Value observe(
    const artis::traffic::core::Time & /* t */,
    unsigned int index) const override {
    switch (index) {
      case vars::COUNTER:
        return _index - _start_index;
      case vars::CLOSE:
        return _next_close;
      default:
        return {};
    }
  }

private:
  // parameters
  unsigned int _start_index;
  double _min;
  std::vector<std::vector<unsigned int> > _paths;

  // state
  unsigned int _index;
  artis::traffic::core::Time _sigma;
  bool _next_close;
  std::default_random_engine _generator;
  std::normal_distribution<double> _distribution;
  std::uniform_int_distribution<size_t> _path_distribution;
  size_t _path_index;

  // vehicle factory
  VehicleFactory _vehicle_factory;
};

}

#endif