/**
 * @file traffic/micro/utils/JSONNetworkGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_JSON_NETWORK_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_JSON_NETWORK_GRAPH_MANAGER_HPP

#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Node.hpp>
#include <artis-traffic/micro/core/Stop.hpp>
#include <artis-traffic/micro/utils/End.hpp>
#include <artis-traffic/micro/utils/Generator.hpp>
#include <artis-traffic/micro/utils/JsonReader.hpp>

#include "MicroGraphManager.hpp"
#include "VehicleFactory.hpp"

namespace artis::traffic::micro::utils {

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters,
  typename Time, typename Parameters = artis::common::NoParameters,
  typename GraphParameters = artis::traffic::micro::utils::Network<InputData, OutputData, NodeData, JunctionData, StopData, LinkData>>
class JSONNetworkGraphManager
  : public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters, Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters, GraphParameters> {
public:
  enum sub_models {
    GENERATOR = 0,
    LINK = 100000,
    JUNCTION = 200000,
    STOP = 300000,
    NODE = 400000,
    END = 500000,
    LAST = END
  };

  JSONNetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                          const artis::common::NoParameters &parameters, const GraphParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters, Stop,
      StopParameters, Generator, GeneratorParameters, Time, Parameters, GraphParameters>(coordinator, parameters,
                                                                                         graph_parameters) {
    this->add_generators(graph_parameters);
    this->add_links(graph_parameters);
    this->add_nodes(graph_parameters);
    this->add_junctions(graph_parameters);
    this->add_stops(graph_parameters);
    this->add_ends(graph_parameters);

    // generator -> link
    // generator -> junction
    for (const auto &p: graph_parameters.inputs) {
      auto out = p.second.data.out_port_id;

      if (out.find("way") == 0) {
        this->connect_generator_to_link(*_generators[p.first], *_links[out]);
      }
      if (out.find("junction") == 0) {
        for (const auto &j: graph_parameters.junctions) {
          if (j.first == out) {
            int j_in_idx = 0;

            for (const auto &j_in: j.second.data.input_ports_id) {
              if (j_in == p.first) {
                this->connect_generator_to_junction(*_generators[p.first], *_junctions[out],
                                                    j_in_idx);
              }
              j_in_idx++;
            }
          }
        }
      }
    }

    std::vector<std::pair<std::string, std::string> > visited_pairs;
    for (const auto &p: graph_parameters.links) {
      int out_idx = 0;

      for (const auto &out: p.second.data.output_ports_id) {
        // link -> end
        if (out.find("end") == 0) {
          this->connect_link_to_end(*_links[p.first], *_ends[out]);
        }

        // link -> junction
        if (out.find("junction") == 0) {
          for (const auto &j: graph_parameters.junctions) {
            if (j.first == out) {
              int j_in_idx = 0;
              for (const auto &j_in: j.second.data.input_ports_id) {
                if (j_in == p.first) {
                  this->connect_upstream_link_to_junction(*_links[p.first], *_junctions[out],
                                                          j_in_idx);
                }
                j_in_idx++;
              }
            }
          }

        }

        // link -> stop
        if (out.find("stop") == 0) {
          this->connect_upstream_link_to_stop(*_links[p.first], *_stops[out], out_idx, 1);
        }
        out_idx++;
      }

      // junction -> link
      for (const auto &in: p.second.data.input_ports_id) {
        if (in.find("junction") == 0) {
          for (const auto &j: graph_parameters.junctions) {
            if (j.first == in) {
              int j_out_idx = 0;

              for (const auto &j_out: j.second.data.output_ports_id) {
                if (j_out == p.first) {
                  this->connect_downstream_link_to_junction(*_links[p.first], *_junctions[in],
                                                            j_out_idx, j_out_idx);
                }
                j_out_idx++;
              }
            }
          }

        }
      }

      // concurrents
      int l1_state_idx = 0;
      for (const auto &c_id: p.second.data.concurrent_links_id) {
        // link -> concurrent link
        for (const auto &concurrent: graph_parameters.links) {
          if (concurrent.first == c_id) {
            int l2_state_idx = 0;

            for (const auto &cc: concurrent.second.data.concurrent_links_id) {
              if (cc == p.first) {
                std::pair<std::string, std::string> link_pair = std::make_pair(p.first, c_id);
                std::pair<std::string, std::string> r_link_pair = std::make_pair(c_id, p.first);
                if (std::find(visited_pairs.begin(), visited_pairs.end(), link_pair) ==
                    visited_pairs.end()) {
                  this->connect_concurrent_links(*_links[p.first], *_links[c_id], l1_state_idx,
                                                 l2_state_idx);
                  visited_pairs.push_back(link_pair);
                  visited_pairs.push_back(r_link_pair);
                }
              }
              l2_state_idx++;
            }
          }
        }
        l1_state_idx++;
      }

      // link -> concurrent stop
      for (const auto &cs_id: p.second.data.concurrent_stops_id) {
        for (const auto &concurrent_s: graph_parameters.stops) {
          if (concurrent_s.first == cs_id) {
            int s_state_idx = 0;

            for (const auto &cc_s: concurrent_s.second.data.concurrent_links_id) {
              if (cc_s == p.first) {
                this->connect_concurrent_link_to_stop(*_links[p.first], *_stops[cs_id], s_state_idx,
                                                      l1_state_idx);
              }
              s_state_idx++;
            }
          }
        }
        l1_state_idx++;
      }
    }

    // stop -> junction
    for (const auto &p: graph_parameters.stops) {
      for (const auto &out: p.second.data.output_ports_id) {
        if (out.find("junction") == 0) {
          for (const auto &j: graph_parameters.junctions) {
            if (j.first == out) {
              int j_in_idx = 0;

              for (const auto &j_in: j.second.data.input_ports_id) {
                if (j_in == p.first) {
                  this->connect_stop_to_junction(*_stops[p.first], *_junctions[out], j_in_idx);
                }
                j_in_idx++;
              }
            }
          }
        }
      }
    }

    // stop -> concurrent stop
    std::vector<std::pair<std::string, std::string> > visited_s_pairs;
    for (const auto &p: graph_parameters.stops) {
      int s1_stop_only_idx = 0;

      for (const auto &c: p.second.data.concurrent_stops_id) {
        for (const auto &cc_s: graph_parameters.stops) {
          if (cc_s.first == c) {
            int s1_sl_idx = p.second.data.concurrent_link_number + s1_stop_only_idx;
            int s2_stop_only_idx = 0;

            for (const auto &cc_s_c: cc_s.second.data.concurrent_stops_id) {
              if (p.first == cc_s_c) {
                break;
              }
              s2_stop_only_idx++;
            }
            std::pair<std::string, std::string> s_pair = std::make_pair(p.first, c);
            std::pair<std::string, std::string> r_s_pair = std::make_pair(c, p.first);
            if (std::find(visited_s_pairs.begin(), visited_s_pairs.end(), s_pair) ==
                visited_s_pairs.end()) {
              int s2_sl_idx = cc_s.second.data.concurrent_link_number + s2_stop_only_idx;
              this->connect_concurrent_stops(*_stops[p.first], *_stops[c], s1_stop_only_idx,
                                             s2_stop_only_idx, s1_sl_idx, s2_sl_idx);
              visited_s_pairs.push_back(s_pair);
              visited_s_pairs.push_back(r_s_pair);
            }
          }
        }
        s1_stop_only_idx++;
      }
    }
  }

  ~JSONNetworkGraphManager() override {
    for (const auto &p: _generators) { delete p.second; }
    for (const auto &p: _links) { delete p.second; }
    for (const auto &p: _junctions) { delete p.second; }
    for (const auto &p: _nodes) { delete p.second; }
    for (const auto &p: _stops) { delete p.second; }
    for (const auto &p: _ends) { delete p.second; }
  }

  virtual void add_generators(const GraphParameters &graph_parameters) {// inputs
    int i = 0;

    for (const auto &p: graph_parameters.inputs) {
      GeneratorParameters generator_parameters{p.second.data.start_index,
                                               p.second.data.min,
                                               p.second.data.mean,
                                               p.second.data.stddev,
                                               p.second.data.seed,
                                               p.second.data.paths};

      _generators[p.first] = new GeneratorSimulator("generator_" + p.first, generator_parameters);
      this->add_child(GENERATOR + i, _generators[p.first]);
      i++;
    }
  }

  virtual void add_links(const GraphParameters &graph_parameters) {// links
    int i = 0;

    for (const auto &p: graph_parameters.links) {
      LinkParameters link_parameters = p.second.data.to_parameters();

      _links[p.first] = new LinkSimulator("link_" + p.first, link_parameters);
      this->add_child(LINK + i, _links[p.first]);
      i++;
    }
  }

  virtual void add_nodes(const GraphParameters &graph_parameters) {// nodes
    int i = 0;

    for (const auto &p: graph_parameters.nodes) {
      core::NodeParameters node_parameters{p.second.data.open_duration,
                                           p.second.data.close_duration,
                                           p.second.data.occupied_duration,
                                           p.second.data.delay_start,
                                           p.second.data.in_number,
                                           p.second.data.out_number};

      _nodes[p.first] = new NodeSimulator<Vehicle>("node_" + p.first, node_parameters);
      this->add_child(NODE + i, _nodes[p.first]);
    }
  }

  virtual void add_junctions(const GraphParameters &graph_parameters) {// junctions
    int i = 0;

    for (const auto &p: graph_parameters.junctions) {
      core::JunctionParameters junction_parameters{p.second.data.in_number,
                                                   p.second.data.out_number};

      _junctions[p.first] = new JunctionSimulator<Vehicle>("junction_" + p.first, junction_parameters);
      this->add_child(JUNCTION + i, _junctions[p.first]);
      i++;
    }
  }

  virtual void add_stops(const GraphParameters &graph_parameters) {// stops
    int i = 0;

    for (const auto &p: graph_parameters.stops) {
        StopParameters stop_parameters = p.second.data.to_parameters();

        _stops[p.first] = new StopSimulator("stop_" + p.first, stop_parameters);
        this->add_child(STOP + i, _stops[p.first]);
        i++;
    }
  }

  virtual void add_ends(const GraphParameters &graph_parameters) { // outputs
    int i = 0;

    for (const auto &p: graph_parameters.outputs) {
      _ends[p.first] = new EndSimulator("end_" + p.first, artis::common::NoParameters());
      this->add_child(END + i, _ends[p.first]);
      i++;
    }
  }

protected:
  using GeneratorSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    Generator, GeneratorParameters>;
  typedef std::map<std::string, GeneratorSimulator *> Generators;

  using LinkSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    Link, LinkParameters>;
  typedef std::map<std::string, LinkSimulator *> Links;

  template<class A>
  using JunctionSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<A>, artis::traffic::micro::core::JunctionParameters>;
  typedef std::map<std::string, JunctionSimulator<Vehicle> *> Junctions;

  using StopSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    Stop, StopParameters>;
  typedef std::map<std::string, StopSimulator *> Stops;

  using EndSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::utils::End>;
  typedef std::map<std::string, EndSimulator *> Ends;

  template<class A>
  using NodeSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Node<A>,
    artis::traffic::micro::core::NodeParameters>;
  typedef std::map<std::string, NodeSimulator<Vehicle> *> Nodes;

  Generators _generators;
  Links _links;
  Junctions _junctions;
  Stops _stops;
  Ends _ends;
  Nodes _nodes;
};
}
#endif //ARTIS_TRAFFIC_MICRO_UTILS_JSON_NETWORK_GRAPH_MANAGER_HPP
