/**
 * @file utils/Switch.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_SWITCH_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_SWITCH_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Vehicle.hpp>

#include <random>

namespace artis::traffic::micro::utils {

struct SwitchParameters {
  std::vector<std::string> outputs;
  unsigned long seed;
};

class Switch
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Switch, SwitchParameters> {
public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  Switch(const std::string &name,
         const artis::pdevs::Context<artis::common::DoubleTime, Switch, SwitchParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Switch, SwitchParameters>(name, context),
    _output_number(context.parameters().outputs.size()),
    _distribution(0, _output_number - 1) {

    assert(not context.parameters().outputs.empty());

    _generator.seed(context.parameters().seed);

    unsigned int index = 0;

    input_ports({{inputs::IN, "in"}});
    for (const std::string &output_name: context.parameters().outputs) {
      output_port({outputs::OUT + index, output_name});
      ++index;
    }
  }

  ~Switch() override = default;

  void dint(const artis::traffic::core::Time & /* t */) override {
    switch (_phase) {
      case Phase::INIT:
        _phase = Phase::WAIT;
        break;
      case Phase::WAIT:
        assert(false);
        break;
      case Phase::SEND:
        _phase = Phase::WAIT;
        break;
    }
  }

  void dext(const artis::traffic::core::Time &t,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag &bag) override {
    std::for_each(bag.begin(), bag.end(),
                  [this, t](const artis::traffic::core::ExternalEvent &e) {
                    if (e.on_port(inputs::IN)) {
                      e.data()(_vehicle);
                      _phase = Phase::SEND;
                      _index = _distribution(_generator);
                    }
                  });
  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _phase = Phase::INIT;
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    switch (_phase) {
      case Phase::INIT:
        return 0;
      case Phase::WAIT:
        return artis::common::DoubleTime::infinity;
      case Phase::SEND:
        return 0;
    }
    return artis::common::DoubleTime::infinity;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override {
    artis::traffic::core::Bag bag;

    if (_phase == Phase::SEND) {
      bag.push_back(artis::traffic::core::ExternalEvent(
        outputs::OUT + _index, _vehicle));
    }
    return bag;
  }

private:
  struct Phase {
    enum values {
      INIT, WAIT, SEND
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case WAIT:
          return "WAIT";
        case SEND:
          return "SEND";
      }
      return "";
    }
  };

  // parameters
  unsigned int _output_number;

  // state
  Phase::values _phase;
  core::Vehicle _vehicle;
  int _index;
  std::default_random_engine _generator;
  std::uniform_int_distribution<int> _distribution;
};

}

#endif