/**
 * @file utils/Counter.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_COUNTER_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_COUNTER_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include <artis-traffic/micro/core/AbstractLink.hpp>
#include <artis-traffic/micro/core/Data.hpp>

namespace artis::traffic::micro::utils {

class Counter
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Counter> {
public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OPEN
    };
  };

  struct vars {
    enum values {
      COUNTER
    };
  };

  Counter(const std::string &name,
          const artis::pdevs::Context<artis::common::DoubleTime, Counter> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Counter>(name, context) {
    input_port({inputs::IN, "in"});
    output_port({outputs::OPEN, "open"});
    observable({vars::COUNTER, "counter"});
  }

  ~Counter() override = default;

  void dint(const artis::traffic::core::Time & /* t */) override { _sigma = artis::common::DoubleTime::infinity; }

  void dext(const artis::traffic::core::Time & /* t */, const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag &bag) override {
    _counter += bag.size();
  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _counter = 0;
    _sigma = 0;
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override {
    return _sigma;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override {
    artis::traffic::core::Bag bag;

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN, artis::traffic::micro::core::open_data{-1, 0}));
    return bag;
  }

  artis::common::event::Value
  observe(const artis::traffic::core::Time & /* t */, unsigned int index) const override {
    if (index == vars::COUNTER) {
      return _counter;
    } else {
      return artis::common::event::Value();
    }
  }

private:
  unsigned int _counter;
  artis::traffic::core::Time _sigma;
};

}

#endif