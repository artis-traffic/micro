/**
 * @file utils/VehicleListGenerator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_VEHICLE_LIST_GENERATOR_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_VEHICLE_LIST_GENERATOR_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>

#include <vector>

namespace artis::traffic::micro::utils {

template<class Vehicle>
struct VehicleListGeneratorParameters {
  std::vector<Vehicle> vehicles;
  std::vector<double> times;
};

template<class Vehicle>
class VehicleListGenerator
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, VehicleListGenerator<Vehicle>, VehicleListGeneratorParameters<Vehicle>> {
public:
  struct inputs {
    enum values {
      OPEN, CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  VehicleListGenerator(const std::string &name,
                       const artis::pdevs::Context<artis::common::DoubleTime, VehicleListGenerator<Vehicle>, VehicleListGeneratorParameters<Vehicle>> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, VehicleListGenerator<Vehicle>, VehicleListGeneratorParameters<Vehicle>>(
      name, context) {
    _vehicles = context.parameters().vehicles;
    _times = context.parameters().times;
    this->output_ports({{outputs::OUT, "out"}});
  }

  ~VehicleListGenerator() override = default;

  void dint(const artis::traffic::core::Time & /* t */) override {
    ++_index;
  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _index = 0;
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
    return _index < _times.size() ? _times[_index] - t : artis::common::DoubleTime::infinity;
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &  /* t */) const override {
    artis::traffic::core::Bag bag;
    Vehicle vehicle{};

    if (_index < _vehicles.size()) {
      vehicle = _vehicles[_index];
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
    }
    return bag;
  }

private:
  unsigned int _index;
  std::vector<Vehicle> _vehicles;
  std::vector<double> _times;
};

}

#endif //ARTIS_TRAFFIC_MICRO_UTILS_VEHICLE_LIST_GENERATOR_HPP
