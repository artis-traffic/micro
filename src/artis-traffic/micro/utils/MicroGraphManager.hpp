/**
 * @file artis-traffic/micro/utils/MicroGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_UTILS_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_MICRO_UTILS_GRAPH_MANAGER_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Disruptor.hpp>
#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Node.hpp>
#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/core/Stop.hpp>
#include <artis-traffic/micro/core/Roundabout.hpp>

#include <artis-traffic/micro/utils/Generator.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include "VehicleFactory.hpp"

namespace artis::traffic::micro::utils {

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters,
  typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
class MicroGraphManager : public artis::pdevs::GraphManager<Time, Parameters, GraphParameters> {
public:
  MicroGraphManager(artis::common::Coordinator<Time> *coordinator, const Parameters &parameters,
                    const GraphParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<Time, Parameters, GraphParameters>(coordinator, parameters, graph_parameters) {}

  void connect_generator_to_link(
    artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> &generator,
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link) {
    this->out({&generator, Generator::outputs::OUT})
      >> this->in({&link, Link::inputs::IN});

    this->out({&link, Link::outputs::CLOSE})
      >> this->in({&generator, Generator::inputs::CLOSE});
    this->out({&link, Link::outputs::OPEN})
      >> this->in({&generator, Generator::inputs::OPEN});
  }

  void connect_concurrent_links(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link_1,
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link_2,
    unsigned int l1_state_index = 0, unsigned int l2_state_index = 0) {
    this->out({&link_1, Link::outputs::STATE + l1_state_index})
      >> this->in({&link_2, Link::inputs::STATE + l2_state_index});
    this->out({&link_2, Link::outputs::STATE + l2_state_index})
      >> this->in({&link_1, Link::inputs::STATE + l1_state_index});

    this->out({&link_1, Link::outputs::STATE_DATA + l1_state_index})
      >> this->in({&link_2, Link::inputs::STATE_DATA + l2_state_index});
    this->out({&link_2, Link::outputs::STATE_DATA + l2_state_index})
      >> this->in({&link_1, Link::inputs::STATE_DATA + l1_state_index});
  }

  void connect_link_to_link(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link_1,
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link_2) {
    this->out({&link_1, Link::outputs::OUT})
      >> this->in({&link_2, Link::inputs::IN});

    this->out({&link_2, Link::outputs::CLOSE})
      >> this->in({&link_1, Link::inputs::CLOSE});
    this->out({&link_2, Link::outputs::OPEN})
      >> this->in({&link_1, Link::inputs::OPEN});
  }

  void connect_upstream_link_to_junction(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Junction<Vehicle>, artis::traffic::micro::core::JunctionParameters> &junction,
    unsigned int link_out_index) {
    this->out({&link, Link::outputs::OUT})
      >> this->in({&junction, artis::traffic::micro::core::Junction<Vehicle>::inputs::IN + link_out_index});

    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::CLOSE + link_out_index})
      >> this->in({&link, Link::inputs::CLOSE});
    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::OPEN + link_out_index})
      >> this->in({&link, Link::inputs::OPEN});
  }

  void connect_upstream_link_to_node(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Node<Vehicle>, artis::traffic::micro::core::NodeParameters> &node,
    unsigned int link_out_index) {
    this->out({&link, Link::outputs::OUT})
      >> this->in({&node, artis::traffic::micro::core::Node<Vehicle>::inputs::IN + link_out_index});

    this->out({&node, artis::traffic::micro::core::Node<Vehicle>::outputs::CLOSE + link_out_index})
      >> this->in({&link, Link::inputs::CLOSE});
    this->out({&node, artis::traffic::micro::core::Node<Vehicle>::outputs::OPEN + link_out_index})
      >> this->in({&link, Link::inputs::OPEN});
  }

  void connect_upstream_link_to_roundabout(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Roundabout<Vehicle>, artis::traffic::micro::core::RoundaboutParameters> &roundabout,
    unsigned int link_out_index, unsigned int roundabout_upstream_number) {
    this->out({&link, Link::outputs::OUT})
      >> this->in({&roundabout, artis::traffic::micro::core::Roundabout<Vehicle>::inputs::IN + link_out_index});

    for (unsigned int i = 0; i < roundabout_upstream_number; i++) {
      this->out({&roundabout, artis::traffic::micro::core::Roundabout<Vehicle>::outputs::CLOSE + i})
        >> this->in({&link, Link::inputs::CLOSE});
      this->out({&roundabout, artis::traffic::micro::core::Roundabout<Vehicle>::outputs::OPEN + i})
        >> this->in({&link, Link::inputs::OPEN});
    }
  }

  void connect_upstream_link_to_stop(artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
                                     artis::pdevs::Simulator<artis::common::DoubleTime, Stop, StopParameters> &stop,
                                     unsigned int link_out_index, unsigned int /* stop_upstream_number */) {
    this->out({&link, Link::outputs::OUT})
      >> this->in(
        {&stop, Stop::inputs::IN + link_out_index});
    this->out({&link, Link::outputs::ARRIVED})
      >> this->in({&stop, Stop::inputs::ARRIVED});

    this->out({&stop, Stop::outputs::CLOSE})
      >> this->in({&link, Link::inputs::CLOSE});
    this->out({&stop, Stop::outputs::OPEN})
      >> this->in({&link, Link::inputs::OPEN});
  }

  void connect_downstream_link_to_junction(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Junction<Vehicle>, artis::traffic::micro::core::JunctionParameters> &junction,
    unsigned int junction_out_index, unsigned int junction_downstream_index) {
    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::OUT + junction_out_index})
      >> this->in({&link, Link::inputs::IN});

    this->out({&link, Link::outputs::CLOSE})
      >> this->in(
        {&junction, artis::traffic::micro::core::Junction<Vehicle>::inputs::CLOSE + junction_downstream_index});
    this->out({&link, Link::outputs::OPEN})
      >> this->in(
        {&junction, artis::traffic::micro::core::Junction<Vehicle>::inputs::OPEN + junction_downstream_index});
  }

  void connect_downstream_link_to_node(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Node<Vehicle>, artis::traffic::micro::core::NodeParameters> &node,
    unsigned int node_out_index, unsigned int node_downstream_index) {
    this->out({&node, artis::traffic::micro::core::Node<Vehicle>::outputs::OUT + node_out_index})
      >> this->in({&link, Link::inputs::IN});

    this->out({&link, Link::outputs::CLOSE})
      >> this->in({&node, artis::traffic::micro::core::Node<Vehicle>::inputs::CLOSE + node_downstream_index});
    this->out({&link, Link::outputs::OPEN})
      >> this->in({&node, artis::traffic::micro::core::Node<Vehicle>::inputs::OPEN + node_downstream_index});
  }

  void connect_downstream_link_to_roundabout(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Roundabout<Vehicle>, artis::traffic::micro::core::RoundaboutParameters> &roundabout,
    unsigned int roundabout_out_index,
    unsigned int roundabout_downstream_index) {
    this->out({&roundabout, artis::traffic::micro::core::Roundabout<Vehicle>::outputs::OUT + roundabout_out_index})
      >> this->in({&link, Link::inputs::IN});

    this->out({&link, Link::outputs::CLOSE})
      >> this->in(
        {&roundabout, artis::traffic::micro::core::Roundabout<Vehicle>::inputs::CLOSE + roundabout_downstream_index});
    this->out({&link, Link::outputs::OPEN})
      >> this->in(
        {&roundabout, artis::traffic::micro::core::Roundabout<Vehicle>::inputs::OPEN + roundabout_downstream_index});
  }

  void connect_stop_to_junction(
    artis::pdevs::Simulator<artis::common::DoubleTime, Stop, StopParameters> &stop,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Junction<Vehicle>, artis::traffic::micro::core::JunctionParameters> &junction,
    unsigned int junction_in_index) {
    this->out({&stop, Stop::outputs::OUT})
      >> this->in({&junction, artis::traffic::micro::core::Junction<Vehicle>::inputs::IN + junction_in_index});

    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::CLOSE + junction_in_index})
      >> this->in({&stop, Stop::inputs::CLOSE});
    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::OPEN + junction_in_index})
      >> this->in({&stop, Stop::inputs::OPEN});
  }

  void connect_concurrent_link_to_stop(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, Stop, StopParameters> &stop,
    unsigned int stop_state_index = 0,
    unsigned int link_state_index = 0) {
    this->out({&link, Link::outputs::STATE_DATA + link_state_index})
      >> this->in({&stop, Stop::inputs::STATE_DATA + stop_state_index});

    this->out({&stop, Stop::outputs::STATE + stop_state_index})
      >> this->in({&link, Link::inputs::STATE + link_state_index});
  }

  void connect_disruptor_to_link(
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Disruptor, artis::traffic::micro::core::DisturbanceParameters> &disruptor,
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link) {
    this->out({&disruptor, artis::traffic::micro::core::Disruptor::outputs::OUT})
      >> this->in({&link, Link::inputs::DISRUPT});
  }

  void connect_link_to_end(
    artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> &end) {
    this->out({&link, Link::outputs::OUT})
      >> this->in({&end, artis::traffic::micro::utils::End::inputs::IN});
    this->out({&end, artis::traffic::micro::utils::End::outputs::OPEN})
      >> this->in({&link, Link::inputs::OPEN});
  }

  void connect_concurrent_stops(
    artis::pdevs::Simulator<artis::common::DoubleTime, Stop, StopParameters> &stop_1,
    artis::pdevs::Simulator<artis::common::DoubleTime, Stop, StopParameters> &stop_2,
    unsigned int s1_stop_only_index = 0, unsigned int s2_stop_only_index = 0,
    unsigned int s1_sl_index = 0, unsigned int s2_sl_index = 0) {
    this->out({&stop_1, Stop::outputs::STATE + s1_sl_index})
      >> this->in({&stop_2, Stop::inputs::STATE + s2_stop_only_index});
    this->out({&stop_2, Stop::outputs::STATE + s2_sl_index})
      >> this->in({&stop_1, Stop::inputs::STATE + s1_stop_only_index});

    this->out({&stop_1, Stop::outputs::STATE_DATA + s1_stop_only_index})
      >> this->in({&stop_2, Stop::inputs::STATE_DATA + s2_sl_index});
    this->out({&stop_2, Stop::outputs::STATE_DATA + s2_stop_only_index})
      >> this->in({&stop_1, Stop::inputs::STATE_DATA + s1_sl_index});
  }

  void connect_generator_to_junction(
    artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> &generator,
    artis::pdevs::Simulator<artis::common::DoubleTime,
      artis::traffic::micro::core::Junction<Vehicle>,
      artis::traffic::micro::core::JunctionParameters> &junction,
    unsigned int junction_in_idx) {
    this->out({&generator, Generator::outputs::OUT})
      >> this->in({&junction, artis::traffic::micro::core::Junction<Vehicle>::inputs::IN + junction_in_idx});

    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::CLOSE})
      >> this->in({&generator, Generator::inputs::CLOSE});
    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::OPEN})
      >> this->in({&generator, Generator::inputs::OPEN});
  }

  void connect_junction_to_end(artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::micro::core::Junction<Vehicle>,
    artis::traffic::micro::core::JunctionParameters> &junction,
                               artis::pdevs::Simulator<artis::common::DoubleTime,
                                 artis::traffic::micro::utils::End> &end) {
    this->out({&junction, artis::traffic::micro::core::Junction<Vehicle>::outputs::OUT})
      >> this->in({&end, artis::traffic::micro::utils::End::inputs::IN});
    this->out({&end, artis::traffic::micro::utils::End::outputs::OPEN})
      >> this->in({&junction, artis::traffic::micro::core::Junction<Vehicle>::inputs::OPEN});
  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_UTILS_GRAPH_MANAGER_HPP
