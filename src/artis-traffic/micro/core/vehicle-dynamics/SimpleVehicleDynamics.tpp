/**
 * @file artis-traffic/micro/core/vehicle-dynamics/SimpleVehicleDynamics.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SimpleVehicleDynamics.hpp"

#include <cmath>

namespace artis::traffic::micro::core {

template<class Vehicle>
double SimpleVehicleDynamics<Vehicle>::compute_position(const artis::traffic::core::Time &t) const {
  const std::deque<SimpleVehicleState> &vehicle_states = _states;
  double position = 0;

  for (size_t k = 0; k < vehicle_states.size(); ++k) {
    const SimpleVehicleState &vehicle_state = vehicle_states[k];
    artis::traffic::core::Time next_time = k == vehicle_states.size() - 1 ? t : vehicle_states[k + 1].begin();
    artis::traffic::core::Time duration = next_time - vehicle_state.begin();

    if (vehicle_state.state() == VehicleData::State::RUNNING) {
      position += vehicle_state.initial_speed() * duration;
    }
  }
  return position;
}

template<class Vehicle>
double SimpleVehicleDynamics<Vehicle>::compute_security_distance() const {
  return (this->vehicle().length() + this->vehicle().gap()) / _states.back().initial_speed();
}

template<class Vehicle>
void SimpleVehicleDynamics<Vehicle>::init(const artis::traffic::core::Time &t, double final_position,
                                          const VehicleDynamics<Vehicle> * /* leader */) {
  this->update_vehicle_max_speed();
  update(t, t + final_position / this->speed_limit(), VehicleData::State::RUNNING, false, this->speed_limit());
}

template<class Vehicle>
void
SimpleVehicleDynamics<Vehicle>::next_state(const artis::traffic::core::Time &t, double final_position,
                                           VehicleDynamics<Vehicle> *follower,
                                           const VehicleDynamics<Vehicle> *leader,
                                           bool go, const artis::traffic::core::Time & /* next_opening_time */,
                                           const artis::traffic::core::Time &next_closing_time) {
  double position = compute_position(t);
  bool apply_disturbance = apply_next_disturbance(t, follower);

  if (not apply_disturbance) {
    const auto &current_state = _states.back();

    switch (current_state.state()) {
      case VehicleData::State::RUNNING: {
        if (std::abs(position - this->link_length()) < 1e-6 and go) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, this->speed_limit());
        } else if (canRun(t, final_position, leader, go, next_closing_time, position)) {
          update(t, t + (this->link_length() - position) / this->speed_limit(), VehicleData::State::RUNNING, false,
                 this->speed_limit());
        } else {
          double distance_to_final_position = final_position - position;

          if (std::abs(distance_to_final_position) < 1e-6) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          } else {
            update(t, t + (final_position - position) / this->speed_limit(), VehicleData::State::RUNNING, false,
                   this->speed_limit());
          }
        }
        break;
      }
      case VehicleData::State::RESTART: {
        if (go) {
          if (std::abs(position - this->link_length()) < 1e-6) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, this->speed_limit());
          } else {
            update(t, t + (final_position - position) / this->speed_limit(), VehicleData::State::RUNNING, false,
                   this->speed_limit());
          }
          if (follower and follower->last_state() == VehicleData::State::STOPPED) {
            follower->update(t, t + follower->vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
          }
        } else {
          if (std::abs(position - this->link_length()) > 1e-6) {
            update(t, t + (final_position - position) / this->speed_limit(), VehicleData::State::RUNNING, false,
                   this->speed_limit());
          } else {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          }
        }
        break;
      }
      case VehicleData::State::HALT:
      case VehicleData::State::STOPPED:
      case VehicleData::State::IN_ACCELERATION:
      case VehicleData::State::IN_DECELERATION:
      case VehicleData::State::TRANSITION_STOPPED: {
        assert(false);
      }
    }
  }
}

template<class Vehicle>
void SimpleVehicleDynamics<Vehicle>::process_disturbance(const artis::traffic::core::Time & /* t */,
                                                         const Disturbance & /* disturbance */, size_t /* index */,
                                                         VehicleDynamics<Vehicle> * /* follower */) {
}

template<class Vehicle>
void
SimpleVehicleDynamics<Vehicle>::update(const artis::traffic::core::Time &t, const artis::traffic::core::Time &next_time,
                                       const VehicleData::State::values &new_state,
                                       bool ready_to_exit, double final_speed) {
  artis::traffic::core::Time adjusted_next_time = std::abs(t - next_time) < 1e-6 ? t : next_time;
  auto current_state = _states.rbegin();
  SimpleVehicleState next_state(new_state, ready_to_exit, t, final_speed, final_speed);

  if (current_state == _states.rend() or *current_state != next_state) {
    _states.push_back(next_state);
    this->update_vehicle_state(new_state);
  }
  this->update_next_time(adjusted_next_time);
  this->update_vehicle_speed(final_speed);
}

}