/**
 * @file artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EventGippsVehicleDynamics.hpp"

#include <cmath>

namespace artis::traffic::micro::core {

const double epsilon = 1.e-6;

template<class Vehicle>
double EventGippsVehicleDynamics<Vehicle>::compute_position(const artis::traffic::core::Time &t) const {
  const std::deque<EventGippsVehicleState> &vehicle_states = _states;
  double position = 0;

  for (size_t k = 0; k < vehicle_states.size(); ++k) {
    const EventGippsVehicleState &vehicle_state = vehicle_states[k];
    artis::traffic::core::Time next_time = k == vehicle_states.size() - 1 ? t : vehicle_states[k + 1].begin();
    artis::traffic::core::Time duration = next_time - vehicle_state.begin();

    if (vehicle_state.state() == VehicleData::State::RUNNING) {
      position += vehicle_state.initial_speed() * duration;
    } else if (vehicle_state.state() == VehicleData::State::IN_DECELERATION) {
      position += (vehicle_state.initial_speed() - 0.5 * this->vehicle().acceleration()
                                                   * duration) * duration;
    } else if (vehicle_state.state() == VehicleData::State::IN_ACCELERATION) {
      //TODO: OK if acceleration is linear
      position += (vehicle_state.initial_speed() + 0.5 * this->vehicle().acceleration()
                                                   * duration) * duration;
    }
  }

  double p = std::round(position * 1e4) / 1e4;

  return std::abs(position - p) < 1e-6 ? p : position;
}

template<class Vehicle>
double EventGippsVehicleDynamics<Vehicle>::compute_speed(const artis::traffic::core::Time &t) const {
  const auto &last_state = _states.back();

  if (last_state.state() == VehicleData::State::IN_ACCELERATION) {
    return last_state.initial_speed() + (t - last_state.begin()) * this->vehicle().acceleration();
  } else if (last_state.state() == VehicleData::State::IN_DECELERATION) {
    return last_state.initial_speed() - (t - last_state.begin()) * this->vehicle().acceleration();
  } else {
    return last_state.initial_speed();
  }
}

template<class Vehicle>
double EventGippsVehicleDynamics<Vehicle>::compute_speed_limit(const artis::traffic::core::Time &t,
                                                               const VehicleDynamics<Vehicle> *leader,
                                                               double distance, double final_position) const {
  double limit = sqrt(
    this->vehicle().acceleration() * distance + this->vehicle().speed() * this->vehicle().speed() / 2);

  if (leader and leader->last_state() == VehicleData::State::IN_DECELERATION and
      leader->compute_position(t) < final_position) {
    double a = -this->vehicle().acceleration();
//    double b =
//      -2 * (this->vehicle().speed() + this->vehicle().reaction_time() * this->vehicle().acceleration());
    double b = -2 * this->vehicle().speed();
    double c = leader->compute_position(t) - compute_position(t) - this->vehicle().gap() - this->vehicle().length() +
               0.5 * leader->compute_speed(t) * leader->compute_speed(t) / leader->vehicle().acceleration() -
               0.5 * this->vehicle().speed() * this->vehicle().speed() / this->vehicle().acceleration();

//    c -= 2 * this->vehicle().speed() * this->vehicle().reaction_time() +
//         this->vehicle().acceleration() * this->vehicle().reaction_time() * this->vehicle().reaction_time();

    double delta = b * b - 4 * a * c;
    double duration = (-b - sqrt(delta)) / (2 * a);

    if (this->vehicle().speed() + duration * this->vehicle().acceleration() < this->speed_limit()) {
//    return this->vehicle().speed() + (duration + this->vehicle().reaction_time()) * this->vehicle().acceleration();
      return this->vehicle().speed() + duration * this->vehicle().acceleration();
    } else {
      return this->speed_limit();
    }
  }
  if (std::abs(limit - this->speed_limit()) < epsilon) {
    return this->speed_limit();
  } else {
    return limit < this->speed_limit() ? limit : this->speed_limit();
  }
}

template<class Vehicle>
double EventGippsVehicleDynamics<Vehicle>::compute_security_distance() const {
  return 0.5 * this->vehicle().speed() * this->vehicle().speed() / this->vehicle().acceleration();
}

template<class Vehicle>
bool EventGippsVehicleDynamics<Vehicle>::critical_position(const artis::traffic::core::Time &t,
                                                           double final_position) const {
  // kinematic
  artis::traffic::core::Time max_duration = compute_speed(t) / this->vehicle().max_acceleration();
  double max_deceleration_distance =
    (compute_speed(t) - 0.5 * this->vehicle().max_acceleration() * max_duration) * max_duration;
  double distance_to_final_position = final_position - compute_position(t);
  double distance = distance_to_final_position - max_deceleration_distance;

  return std::abs(distance) < epsilon or std::abs(compute_position(t) - this->link_length()) < epsilon;
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::init(const artis::traffic::core::Time &t,
                                              double final_position,
                                              const VehicleDynamics<Vehicle> *leader) {
  this->update_vehicle_max_speed();
  if (this->vehicle().speed() > this->vehicle().max_speed()) {
    this->update_vehicle_speed(this->vehicle().max_speed());
  }

//  assert(this->speed_limit() * this->speed_limit() / (2 * this->vehicle().acceleration()) <= final_position);

  if (this->speed_limit() * this->speed_limit() / (2 * this->vehicle().acceleration()) > final_position) {
    update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
  } else if (this->vehicle().speed() < this->vehicle().max_speed()) {
    restart(t, 0, final_position, leader);
  } else if (this->vehicle().speed() > this->vehicle().max_speed()) {

    assert((this->vehicle().speed() - this->speed_limit()) / this->vehicle().acceleration() > 0);

    update(t, t + (this->vehicle().speed() - this->speed_limit()) / this->vehicle().acceleration(),
           VehicleData::State::IN_DECELERATION, false, this->speed_limit());
  } else { // vehicle.speed == vehicle.max_speed
    if (std::abs(
      this->vehicle().speed() * this->vehicle().speed() / (2 * this->vehicle().acceleration()) - final_position) <
        epsilon) {

      assert(this->vehicle().speed() / this->vehicle().acceleration() > 0);

      update(t, t + this->vehicle().speed() / this->vehicle().acceleration(), VehicleData::State::IN_DECELERATION,
             false, 0);
    } else {

      assert(
        final_position / this->vehicle().speed() - 0.5 * this->vehicle().speed() / this->vehicle().acceleration() > 0);

      update(t, t + final_position / this->vehicle().speed() -
                0.5 * this->vehicle().speed() / this->vehicle().acceleration(),
             VehicleData::State::RUNNING, false, this->speed_limit());
    }
  }
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::next_state(const artis::traffic::core::Time &t,
                                                    double final_position,
                                                    VehicleDynamics<Vehicle> *follower,
                                                    const VehicleDynamics<Vehicle> *leader,
                                                    bool go,
                                                    const artis::traffic::core::Time &next_opening_time,
                                                    const artis::traffic::core::Time &next_closing_time) {
  double position = compute_position(t);
  bool apply_disturbance = this->apply_next_disturbance(t, follower);

  if (not apply_disturbance) {
    const auto &current_state = _states.back();
//    double leader_position = leader != nullptr ? leader->compute_position(t) : -1;
//    double follower_position = follower != nullptr ? follower->compute_position(t) : -1;

//    if (not(leader == nullptr or (std::abs(leader_position - position - (this->vehicle().length() + this->vehicle().gap())) < 1e-6 or
//            leader_position - position > this->vehicle().length() + this->vehicle().gap()))) {
//      std::cerr << "warning: leader distance difference < 0 (" << (leader_position - position) << ")" <<
//                " time: " << t << " vehicle: " << this->vehicle().index() << std::endl;
//    }
//    if (not(
//      follower == nullptr or (std::abs(position - follower_position - (this->vehicle().length() + this->vehicle().gap())) < 1e-6 or
//                              position - follower_position > this->vehicle().length() + this->vehicle().gap()))) {
//      std::cerr << "warning: follower distance difference < 0 (" << (position - follower_position) << ")" <<
//                " time: " << t << " vehicle: " << this->vehicle().index() << std::endl;
//    }

    switch (current_state.state()) {
      case VehicleData::State::RUNNING: {
        double deceleration_duration = this->vehicle().speed() / this->vehicle().acceleration();
        double deceleration_distance =
          (this->vehicle().speed() - 0.5 * this->vehicle().acceleration() * deceleration_duration) *
          deceleration_duration;
        double duration_to_final_position = (final_position - position) / this->vehicle().speed();

        if (std::abs(position - this->link_length()) < epsilon and go) {
          assert(this->is_first());
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, this->vehicle().speed());
        } else if (final_position - position - deceleration_distance > 1e-4) {

          assert(duration_to_final_position - 0.5 * deceleration_duration > 0);

          update(t, t + duration_to_final_position - 0.5 * deceleration_duration, VehicleData::State::RUNNING, false,
                 this->vehicle().speed());
        } else if (this->canRun(t, final_position, leader, go, next_closing_time, position)) {

          assert((this->link_length() - position) / this->speed_limit() > 0);

          update(t, t + (this->link_length() - position) / this->speed_limit(), VehicleData::State::RUNNING, false,
                 this->speed_limit());
        } else {
          double distance_to_final_position = final_position - position;
          double running_distance = distance_to_final_position - deceleration_distance;

          if (std::abs(running_distance) < 1e-5) {
            if ((next_opening_time > 0 and next_opening_time - t > deceleration_duration) or next_opening_time == -1) {

              assert(deceleration_duration > 0);

              update(t, t + deceleration_duration, VehicleData::State::IN_DECELERATION, false, 0);
            } else {
              update(t, next_opening_time, VehicleData::State::IN_DECELERATION, false, 0);
            }
          } else {
            if (running_distance < 0) {
              double adjust_speed = std::sqrt(2 * distance_to_final_position * this->vehicle().acceleration());

              deceleration_duration = adjust_speed / this->vehicle().acceleration();
              this->update_vehicle_speed(adjust_speed);
              update(t, t + deceleration_duration, VehicleData::State::IN_DECELERATION, false, 0);
            } else {
              update(t, t + running_distance / this->vehicle().speed(), VehicleData::State::RUNNING, false,
                     this->vehicle().speed());
            }
          }
        }
        break;
      }
      case VehicleData::State::RESTART: {
        if (std::abs(position - this->link_length()) < epsilon) {
          assert(this->is_first());
          if (go) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::IN_ACCELERATION, true,
                   this->vehicle().speed());
          } else {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
          }
        } else {
          restart(t, position, final_position, leader);
        }
        if (follower and follower->last_state() == VehicleData::State::STOPPED and
            last_state() != VehicleData::State::STOPPED) {
          follower->update(t, t + follower->vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
        }
        break;
      }
      case VehicleData::State::IN_ACCELERATION: {
        next_state_after_acceleration(t, final_position, leader, go, next_closing_time, position);
        break;
      }
      case VehicleData::State::IN_DECELERATION: {
        if (std::abs(this->link_length() - position) < epsilon) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
          if (go and follower and follower->last_state() == VehicleData::State::STOPPED) {
            follower->update(t, t + follower->vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
          }
        } else if (std::abs(final_position - position) < epsilon) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
        } else if (this->vehicle().speed() == this->speed_limit()) {
          next_state_after_acceleration(t, final_position, leader, go, next_closing_time, position);
        } else {
          double distance_to_final_position = final_position - position;
          double max_speed = compute_speed_limit(t, leader, distance_to_final_position, final_position);

          if (std::abs(max_speed) < epsilon) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          } else if (max_speed < this->vehicle().speed()) {
            update(t, t + this->vehicle().speed() / this->vehicle().acceleration(), VehicleData::State::IN_DECELERATION,
                   false, 0);
          } else {
            double new_duration = (max_speed - this->vehicle().speed()) / this->vehicle().acceleration();

            if (std::abs(new_duration) < 1e-5) {
              update(t, t + this->vehicle().speed() / this->vehicle().acceleration(),
                     VehicleData::State::IN_DECELERATION,
                     false, 0);
            } else {

              assert(new_duration > 0);

              update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
            }
          }
        }
        break;
      }
      case VehicleData::State::HALT:
      case VehicleData::State::STOPPED:
      case VehicleData::State::TRANSITION_STOPPED: {
        assert(false);
      }
    }
    if (follower and follower->last_state() != VehicleData::State::RESTART and
        follower->last_state() != VehicleData::State::STOPPED) {
      follower->stop(t, follower, true);
    }
  }
}

template<class Vehicle>
void
EventGippsVehicleDynamics<Vehicle>::next_state_after_acceleration(const traffic::core::Time &t, double final_position,
                                                                  const VehicleDynamics<Vehicle> *leader, bool go,
                                                                  const traffic::core::Time &next_closing_time,
                                                                  double position) {
  double deceleration_duration = this->vehicle().speed() / this->vehicle().acceleration();
  double deceleration_distance = (this->vehicle().speed() - 0.5 * this->vehicle().acceleration() *
                                                            deceleration_duration) * deceleration_duration;
  double distance_to_final_position = final_position - position;
  double running_distance = distance_to_final_position - deceleration_distance;
  double max_speed = compute_speed_limit(t, leader, distance_to_final_position, final_position);

  if (std::abs(position - this->link_length()) < epsilon and go) {
    assert(this->is_first());
    update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, this->vehicle().speed());
  }
    // if vehicle can accelerate again?
  else if (std::abs(this->vehicle().speed() - this->speed_limit()) > epsilon and
           this->vehicle().speed() < this->speed_limit() and running_distance > epsilon and
           max_speed > this->vehicle().speed()) {
    double new_duration = (max_speed - this->vehicle().speed()) / this->vehicle().acceleration();

    assert(new_duration);

    update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
  } else if (std::abs(this->vehicle().speed() - this->speed_limit()) > epsilon and
             this->vehicle().speed() < this->speed_limit() and
             this->canRun(t, final_position, leader, go, next_closing_time, position)) {
    double a = 0.5 * this->vehicle().acceleration();
    double b = this->vehicle().speed();
    double c = -distance_to_final_position;
    double delta = b * b - 4 * a * c;
    double d = -b + sqrt(delta) / (2 * a);

    assert(std::min((this->speed_limit() - this->vehicle().speed()) / this->vehicle().acceleration(), d) > 0);

    update(t, t + std::min((this->speed_limit() - this->vehicle().speed()) / this->vehicle().acceleration(), d),
           VehicleData::State::IN_ACCELERATION, false, this->speed_limit());
  } else {
    double duration_to_final_position = (final_position - position) / this->vehicle().speed();

    // if possible to run before decelerate and stop
    if (final_position - position - deceleration_distance > epsilon) {

      assert(duration_to_final_position - 0.5 * deceleration_duration > 0);

      update(t, t + duration_to_final_position - 0.5 * deceleration_duration, VehicleData::State::RUNNING, false,
             this->vehicle().speed());
    } else if (this->canRun(t, final_position, leader, go, next_closing_time, position)) {

      assert(duration_to_final_position);

      update(t, t + duration_to_final_position, VehicleData::State::RUNNING, false, this->vehicle().speed());
    } else {

      assert(deceleration_duration);

      update(t, t + deceleration_duration, VehicleData::State::IN_DECELERATION, false, 0);
    }
  }
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::process_disturbance(const artis::traffic::core::Time &t,
                                                             const Disturbance &disturbance, size_t index,
                                                             VehicleDynamics<Vehicle> *follower) {
  VehicleData::State::values new_state;
  double final_speed = 0;

  if (index == 0) {
    this->disturbance.disturbance = std::make_unique<Disturbance>(disturbance);
    this->disturbance.index = 0;
  }
  if (this->disturbance.disturbance->type == DisturbanceTypes::ACCELERATION) {
    switch (index) {
      case 0: {
        new_state = VehicleData::State::IN_ACCELERATION;
        final_speed =
          this->vehicle().speed() + this->vehicle().acceleration() * this->disturbance.disturbance->durations[index];
        break;
      }
      case 1: {
        new_state = VehicleData::State::RUNNING;
        final_speed = this->vehicle().speed();
        break;
      }
      case 2: {
        new_state = VehicleData::State::IN_DECELERATION;
        final_speed = this->vehicle().max_speed();
        break;
      }
      default:
        assert(false);
    }
  } else {
    switch (index) {
      case 0: {
        new_state = VehicleData::State::IN_DECELERATION;
        final_speed =
          this->vehicle().speed() - this->vehicle().acceleration() * this->disturbance.disturbance->durations[index];
        break;
      }
      case 1: {
        if (std::abs(this->vehicle().speed()) < epsilon) {
          new_state = VehicleData::State::HALT;
          final_speed = 0;
        } else {
          new_state = VehicleData::State::RUNNING;
          final_speed = this->vehicle().speed();
        }
        break;
      }
      case 2: {
        new_state = VehicleData::State::IN_ACCELERATION;
        final_speed = this->vehicle().max_speed();
        break;
      }
      default:
        assert(false);
    }
  }
  update(t, t + this->disturbance.disturbance->durations[index], new_state, false, final_speed);
  if (follower) {
    follower->propagate_disturbance(t);
  }
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::propagate_disturbance(const artis::traffic::core::Time &t) {
  const VehicleData::State::values &state = states().back().state();
  double final_speed = 0;
  double duration = this->vehicle().reaction_time();

  switch (state) {
    case VehicleData::State::STOPPED:
      return;
    case VehicleData::State::HALT:
    case VehicleData::State::RESTART:
      final_speed = 0;
      break;
    case VehicleData::State::RUNNING:
      final_speed = states().back().final_speed();
      if (this->next_time() - t < duration) {
        duration = this->next_time() - t;
      }
      break;
    case VehicleData::State::IN_ACCELERATION:
      this->update_vehicle_speed(
        states().back().final_speed() - this->vehicle().acceleration() * (this->next_time() - t));
      if (this->next_time() - t > duration) {
        final_speed =
          states().back().final_speed() - this->vehicle().acceleration() * (this->next_time() - t - duration);
      } else {
        duration = this->next_time() - t;
        final_speed = states().back().final_speed();
      }
      break;
    case VehicleData::State::IN_DECELERATION:
      this->update_vehicle_speed(
        states().back().final_speed() + this->vehicle().acceleration() * (this->next_time() - t));
      if (this->next_time() - t > duration) {
        final_speed =
          states().back().final_speed() + this->vehicle().acceleration() * (this->next_time() - t - duration);
      } else {
        duration = this->next_time() - t;
        final_speed = states().back().final_speed();
      }
      break;
    case VehicleData::State::TRANSITION_STOPPED:
      return;
  }
  if (std::abs(duration) > epsilon) {
    update(t, t + duration, states().back().state(), false, final_speed);
  }
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::restart(const traffic::core::Time &t, double position, double final_position,
                                                 const VehicleDynamics<Vehicle> *leader) {
  double speed_difference = this->speed_limit() - this->vehicle().speed();
  double acceleration_duration = speed_difference / this->vehicle().acceleration();
  double future_position =
    position + (this->vehicle().speed() + 0.5 * this->vehicle().acceleration() * acceleration_duration) *
               acceleration_duration;
  double deceleration_duration = this->speed_limit() / this->vehicle().acceleration();
  double deceleration_distance = (this->speed_limit() - 0.5 * this->vehicle().acceleration() * deceleration_duration) *
                                 deceleration_duration;

  // if vehicle can accelerate to speed limit and decelerate
  if ((final_position - future_position) - deceleration_distance > epsilon) {
    update(t, t + acceleration_duration, VehicleData::State::IN_ACCELERATION, false, this->speed_limit());
  } else {
    double limit_speed = compute_speed_limit(t, leader, final_position - position, final_position);

    if (limit_speed < this->vehicle().speed()) {
      double new_duration = (this->vehicle().speed() - limit_speed) / this->vehicle().acceleration();

      update(t, t + new_duration, VehicleData::State::IN_DECELERATION, false, limit_speed);
    } else {
      double new_duration = (limit_speed - this->vehicle().speed()) / this->vehicle().acceleration();

      update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, limit_speed);
    }
  }
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::stop(const artis::traffic::core::Time &t,
                                              const VehicleDynamics<Vehicle> *follower,
                                              bool immediate) {
  auto current_state = _states.rbegin();
  double t_next = immediate ? t : t + this->vehicle().reaction_time();
  double new_final_speed = follower->compute_speed(t_next);

  if (immediate or (not immediate and this->next_time() > t_next)) {
    current_state->update_final_speed(new_final_speed);
    this->update_next_time(t_next);
    this->update_vehicle_speed(new_final_speed);
  }
}

template<class Vehicle>
void EventGippsVehicleDynamics<Vehicle>::update(const artis::traffic::core::Time &t,
                                                const artis::traffic::core::Time &next_time,
                                                const VehicleData::State::values &new_state,
                                                bool ready_to_exit, double final_speed) {
  artis::traffic::core::Time adjusted_next_time = std::abs(t - next_time) < 1e-6 ? t : next_time;
  auto current_state = _states.rbegin();
  EventGippsVehicleState next_state(new_state, ready_to_exit, t, this->vehicle().speed(), final_speed);

  assert(current_state == _states.rend() or *current_state != next_state or
         (not(*current_state != next_state) and t < adjusted_next_time));

  if (current_state == _states.rend()) {
    _states.push_back(next_state);
    this->update_vehicle_state(new_state);
  } else {
    if (current_state->state() == next_state.state() and current_state->ready_to_exit() == next_state.ready_to_exit()
        and current_state->final_speed() == next_state.initial_speed()) {
      current_state->update_final_speed(next_state.final_speed());
    } else {
      _states.push_back(next_state);
      this->update_vehicle_state(new_state);
    }
  }

//  std::cout << std::setprecision(15) << adjusted_next_time << " s" << std::endl;

  this->update_next_time(adjusted_next_time);
  this->update_vehicle_speed(final_speed);
}

}