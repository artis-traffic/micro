/**
 * @file artis-traffic/micro/core/vehicle-dynamics/KinematicVehicleDynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_KINEMATIC_VEHICLE_DYNAMICS_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_KINEMATIC_VEHICLE_DYNAMICS_HPP

#include <artis-traffic/core/Base.hpp>
#include "src/artis-traffic/micro/core/VehicleData.hpp"
#include "src/artis-traffic/micro/core/Disturbance.hpp"

namespace artis::traffic::micro::core {

struct KinematicState {
  VehicleData::State::values state;
  bool ready_to_exit;
  artis::traffic::core::Time begin;
  double initial_speed;
  double final_speed;
  double acceleration;

  bool operator!=(const KinematicState &s) const {
    return state != s.state or ready_to_exit != s.ready_to_exit or begin != s.begin or initial_speed != s.initial_speed
           or final_speed != s.final_speed or acceleration != s.acceleration;
  }
};

template<class Vehicle>
  struct KinematicVehicleDynamics {
  Vehicle vehicle;
  std::deque<KinematicState> states;
  artis::traffic::core::Time next_time;
  double link_length;
  double speed_limit;
  bool is_first;
  ActiveDisturbance disturbance;
  //TODO
  double start_speed;

  // debug
  double last_final_position;

  double compute_position(const artis::traffic::core::Time &t) const;

  double compute_security_distance() const;

  void init(const artis::traffic::core::Time &t, double final_position, const KinematicVehicleDynamics *leader);

  void next_state(const artis::traffic::core::Time &t, double final_position, KinematicVehicleDynamics *follower,
                  const KinematicVehicleDynamics *leader,
                  bool go, const artis::traffic::core::Time &next_opening_time,
                  const artis::traffic::core::Time &next_closing_time);

  void
  next_state_without_constraint(const traffic::core::Time &t, double final_position, KinematicVehicleDynamics *follower,
                                const KinematicVehicleDynamics *leader,
                                double position, double leader_speed, double speed_difference,
                                double leader_position,
                                double position_difference, bool go,
                                const artis::traffic::core::Time &next_opening_time,
                                const artis::traffic::core::Time &next_closing_time);

  void next_state_constraint(const traffic::core::Time &t, double final_position, KinematicVehicleDynamics *follower,
                             const KinematicVehicleDynamics *leader,
                             double position, double leader_speed, double speed_difference, double leader_position,
                             double position_difference, bool go, const artis::traffic::core::Time &next_opening_time,
                             const artis::traffic::core::Time &next_closing_time);

  void process_disturbance(const artis::traffic::core::Time &t, const Disturbance &disturbance, size_t index,
                           KinematicVehicleDynamics *follower);

  void propagate_disturbance(const artis::traffic::core::Time &t);

  void update(const artis::traffic::core::Time &t, const artis::traffic::core::Time &next_time,
              const VehicleData::State::values &new_state, bool ready_to_exit, double final_speed);

  void update_acceleration_leader_follower(const traffic::core::Time &t, double position,
                                           const KinematicVehicleDynamics *leader,
                                           double leader_position, double leader_speed, double leader_final_speed,
                                           double leader_acceleration_duration, double future_leader_position);

  void update_deceleration_leader_follower(const traffic::core::Time &t, double position,
                                           const KinematicVehicleDynamics *leader,
                                           double leader_speed, double leader_position);

  void
  init_with_speed_smaller(const traffic::core::Time &t, double final_position, const KinematicVehicleDynamics *leader);

  void
  init_with_speed_bigger(const traffic::core::Time &t, double final_position, const KinematicVehicleDynamics *leader);

  void
  init_with_same_speed(const traffic::core::Time &t, double final_position, const KinematicVehicleDynamics *leader);
};

}

#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.tpp>

#endif