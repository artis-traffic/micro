/**
 * @file artis-traffic/micro/core/vehicle-dynamics/KinematicVehicleDynamics.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "KinematicVehicleDynamics.hpp"

#include <cmath>
#include <iostream>

namespace artis::traffic::micro::core {

double KinematicVehicleDynamics::compute_position(const artis::traffic::core::Time &t) const {
  const std::deque<KinematicState> &vehicle_states = states;
  double position = 0;

  for (size_t k = 0; k < vehicle_states.size(); ++k) {
    const KinematicState &vehicle_state = vehicle_states[k];
    artis::traffic::core::Time next_time = k == vehicle_states.size() - 1 ? t : vehicle_states[k + 1].begin;
    artis::traffic::core::Time duration = next_time - vehicle_state.begin;

    if (vehicle_state.state == VehicleData::State::RUNNING) {
      position += vehicle_state.initial_speed * duration;
    } else if (vehicle_state.state == VehicleData::State::IN_DECELERATION) {
      position += (vehicle_state.initial_speed - 0.5 * vehicle_state.acceleration
                                                 * duration) * duration;
    } else if (vehicle_state.state == VehicleData::State::IN_ACCELERATION) {
      position += (vehicle_state.initial_speed + 0.5 * vehicle_state.acceleration
                                                 * duration) * duration;
    }
  }
  return position;
}

double KinematicVehicleDynamics::compute_security_distance() const {
  double b = states.back().initial_speed;
  double a = 0.5 * states.back().acceleration;
  double c = -(vehicle.length() + vehicle.gap());
  double delta = (b * b) - 4 * a * c;

  return (-b + std::sqrt(delta)) / (2 * a);
}

void KinematicVehicleDynamics::init(const artis::traffic::core::Time &t, double final_position,
                                    const KinematicVehicleDynamics *leader) {
  vehicle.update_max_speed(speed_limit);
  start_speed = vehicle.speed();
  last_final_position = final_position;
  if (vehicle.speed() < vehicle.max_speed()) {
    init_with_speed_smaller(t, final_position, leader);
  } else if (vehicle.speed() > vehicle.max_speed()) {
    init_with_speed_bigger(t, final_position, leader);
  } else { // vehicle.speed == vehicle.max_speed
    init_with_same_speed(t, final_position, leader);
  }
}

void KinematicVehicleDynamics::init_with_same_speed(const traffic::core::Time &t, double final_position,
                                                    const KinematicVehicleDynamics * /* leader */) {
  switch (vehicle.data().state) {
    case VehicleData::State::STOPPED:
    case VehicleData::State::RESTART:
    case VehicleData::State::HALT: {
      assert(false);
    }
    case VehicleData::State::IN_ACCELERATION:
    case VehicleData::State::IN_DECELERATION:
    case VehicleData::State::RUNNING:

      assert(vehicle.speed() * vehicle.speed() / (2 * vehicle.acceleration()) <= final_position);

      if (std::abs(vehicle.speed() * vehicle.speed() / (2 * vehicle.acceleration()) - final_position) < 1e-6) {
        update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
      } else {
        update(t, t + final_position / vehicle.speed() - 0.5 * vehicle.speed() / vehicle.acceleration(),
               VehicleData::State::RUNNING, false, speed_limit);
      }
  }
}

void KinematicVehicleDynamics::init_with_speed_bigger(const traffic::core::Time &t, double final_position,
                                                      const KinematicVehicleDynamics * /* leader */) {
  switch (vehicle.data().state) {
    case VehicleData::State::STOPPED:
    case VehicleData::State::RESTART:
    case VehicleData::State::HALT: {
      assert(false);
    }
    case VehicleData::State::IN_ACCELERATION:
    case VehicleData::State::IN_DECELERATION:
    case VehicleData::State::RUNNING:

//      assert(vehicle.speed() * vehicle.speed() / (2 * vehicle.acceleration()) <= final_position);
//
//      traffic::core::Time duration = (vehicle.speed() - speed_limit) / vehicle.acceleration();
//      double future_position = (vehicle.speed() + 0.5 * vehicle.acceleration()
//                                                  * duration) * duration;
//      double deceleration_distance = (vehicle.speed() - 0.5 * vehicle.acceleration() * duration) * duration;
//      if ((final_position - future_position) - deceleration_distance < 1e-6) {
//        update(t, t + vehicle.speed() / vehicle.acceleration(),
//               VehicleData::KinematicState::IN_DECELERATION, false, 0);
//      } else {
//        update(t, t + (vehicle.speed() - speed_limit) / vehicle.acceleration(),
//               VehicleData::KinematicState::IN_DECELERATION, false, speed_limit);
//      }

      start_speed = vehicle.speed();
//    double new_acceleration = (start_speed - speed_limit) / (0.5);
//    vehicle.update_acceleration(new_acceleration);
//    update(t, t + 0.5, VehicleData::KinematicState::IN_DECELERATION, false, speed_limit);
      vehicle.update_speed(0);
      update(t, t + 0.5, VehicleData::State::TRANSITION_STOPPED, false, 0);
  }
}

void KinematicVehicleDynamics::init_with_speed_smaller(const traffic::core::Time &t, double final_position,
                                                       const KinematicVehicleDynamics * /* leader */) {
  switch (vehicle.data().state) {
    case VehicleData::State::HALT: {
      assert(false);
    }
    case VehicleData::State::STOPPED: {
      update(t, t, VehicleData::State::RESTART, false, 0);
      break;
    }
    case VehicleData::State::RESTART:
    case VehicleData::State::IN_ACCELERATION:
    case VehicleData::State::IN_DECELERATION:
    case VehicleData::State::RUNNING:

      assert(vehicle.speed() * vehicle.speed() / (2 * vehicle.acceleration()) <= final_position);

      traffic::core::Time duration = (speed_limit - vehicle.speed()) / vehicle.acceleration();
      double future_position = ((speed_limit - vehicle.speed()) + 0.5 * vehicle.acceleration()
                                                                  * duration) * duration;
      double deceleration_distance = (speed_limit - 0.5 * vehicle.acceleration() * duration) * duration;

      // TODO
      if ((final_position - future_position) - deceleration_distance > 1e-6) {
        traffic::core::Time new_duration = (speed_limit - vehicle.speed()) / vehicle.acceleration();

        update(t, t + new_duration, VehicleData::State::IN_ACCELERATION,
               false, speed_limit);
      } else {
        double distance = final_position - deceleration_distance;
        double limit_speed = sqrt(vehicle.acceleration() * distance + vehicle.speed() * vehicle.speed() / 2);
        if (limit_speed < vehicle.speed()) {
          traffic::core::Time new_duration = (vehicle.speed()) - limit_speed / vehicle.acceleration();

          update(t, t + new_duration, VehicleData::State::IN_DECELERATION,
                 false, limit_speed);
        } else {
          traffic::core::Time new_duration = (limit_speed - vehicle.speed()) / vehicle.acceleration();

          update(t, t + new_duration, VehicleData::State::IN_ACCELERATION,
                 false, limit_speed);
        }
      }
  }
}

void KinematicVehicleDynamics::next_state(const artis::traffic::core::Time &t, double final_position,
                                          KinematicVehicleDynamics *follower,
                                          const KinematicVehicleDynamics *leader,
                                          bool go, const artis::traffic::core::Time &next_opening_time,
                                          const artis::traffic::core::Time &next_closing_time) {
  double position = compute_position(t);
  bool apply_disturbance = false;

  assert(std::abs(last_final_position - final_position) < 1e-6 or last_final_position <= final_position);
  assert(std::abs(vehicle.speed() - states.back().final_speed) < 1e-6);

  last_final_position = final_position;

  // TODO
  if (vehicle.acceleration() > vehicle.max_acceleration()) {
    vehicle.update_acceleration(vehicle.max_acceleration());
  }
//  vehicle.update_acceleration(vehicle.max_acceleration());
  if (disturbance.disturbance) {
    ++disturbance.index;
    if (disturbance.index < 3) {

      process_disturbance(t, *disturbance.disturbance, disturbance.index, follower);
      apply_disturbance = true;
    } else {
      disturbance.disturbance.reset(nullptr);
    }
  }
  if (not apply_disturbance) {
    if (leader) {
      const KinematicState &current_leader_state = leader->states.back();
      double leader_speed =
        current_leader_state.initial_speed + (t - current_leader_state.begin) *
                                             (current_leader_state.final_speed - current_leader_state.initial_speed) /
                                             (leader->next_time - current_leader_state.begin);
      double speed_difference = leader_speed - vehicle.speed();
      double leader_position = leader->compute_position(t);
      double follower_position = follower != nullptr ? follower->compute_position(t) : -1;
      double position_difference = leader_position - position;

      if (not(std::abs(position_difference - (vehicle.length() + vehicle.gap())) < 1e-6 or
              position_difference > vehicle.length() + vehicle.gap())) {
        std::cerr << "warning: distance difference < 0 (" << position_difference << ")" <<
                  " time: " << t << " vehicle: " << vehicle.index() << std::endl;
      }
      if (not(
        follower == nullptr or (std::abs(position - follower_position - (vehicle.length() + vehicle.gap())) < 1e-6 or
                                position - follower_position > vehicle.length() + vehicle.gap()))) {
        std::cerr << "warning: distance difference < 0 (" << (position - follower_position) << ")" <<
                  " time: " << t << " vehicle: " << vehicle.index() << std::endl;
      }

      double criterion = speed_difference / position_difference;

      if (std::abs(criterion) < 0.01) {
        next_state_without_constraint(t, final_position, follower, leader, position, leader_speed, speed_difference,
                                      leader_position, position_difference, go, next_opening_time, next_closing_time);
      } else {
        next_state_constraint(t, final_position, follower, leader, position, leader_speed, speed_difference,
                              leader_position, position_difference, go, next_opening_time, next_closing_time);
      }
    } else {
      next_state_without_constraint(t, final_position, follower, leader, position, 0, 0, 0, 0, go, next_opening_time,
                                    next_closing_time);
    }
  }

  // control
  {
    position = compute_position(t);
    assert(states.back().state != VehicleData::State::STOPPED or
           (states.back().state == VehicleData::State::STOPPED and std::abs(position - final_position) < 1e-6));
    // TODO: add back the assert only if previous state != TRANSITION_STOPPED
//    assert(states.back().state != VehicleData::KinematicState::RUNNING or (states.back().state == VehicleData::KinematicState::RUNNING and
//                                                                  states.back().initial_speed > 0 and
//                                                                  states.back().initial_speed ==
//                                                                  states.back().final_speed));
  }

}

void
KinematicVehicleDynamics::next_state_constraint(const traffic::core::Time &t, double final_position,
                                                KinematicVehicleDynamics *follower,
                                                const KinematicVehicleDynamics *leader,
                                                double position, double leader_speed, double speed_difference,
                                                double leader_position,
                                                double position_difference, bool go,
                                                const artis::traffic::core::Time & /* next_opening_time */,
                                                const artis::traffic::core::Time &next_closing_time) {
  const auto &current_state = states.back();

  switch (states.back().state) {
    // ***************
    // state = running
    case VehicleData::State::RUNNING: {
      switch (leader->vehicle.data().state) {
        case VehicleData::State::HALT: {
          assert(false);
        }
        case VehicleData::State::STOPPED:
        case VehicleData::State::RESTART: {
          artis::traffic::core::Time duration = vehicle.speed() / vehicle.acceleration();
          double deceleration_distance = (vehicle.speed() - 0.5 * vehicle.acceleration() * duration) * duration;
          double distance_to_final_position = final_position - position;
          double distance = distance_to_final_position - deceleration_distance;

          assert(final_position > position + deceleration_distance or
                 std::abs(final_position - (position + deceleration_distance)) < 1e-6);

          if (std::abs(distance) < 1e-6 /*and _stop*/) {
            update(t, t + duration, VehicleData::State::IN_DECELERATION, false, 0);
          } else {
            if (distance > 1e-6) {
              update(t, t + distance / vehicle.speed(), VehicleData::State::RUNNING, false, vehicle.speed());
            } else { // distance < -1e-6

              assert(false);

            }
          }
          break;
        }
        case VehicleData::State::RUNNING: {
          if (leader->vehicle.speed() < leader->vehicle.max_speed()) {
            double distance_to_final = final_position - position;
            double deceleration_duration = vehicle.speed() / vehicle.acceleration();
            double deceleration_distance =
              (vehicle.speed() - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;

            if (distance_to_final > deceleration_distance) {
              double new_duration = (distance_to_final - deceleration_distance) / vehicle.speed();

              update(t, t + new_duration, VehicleData::State::RUNNING, false, vehicle.speed());
            } else {

              assert(std::abs(distance_to_final - deceleration_distance) < 1e-6);

              double max_speed = sqrt(
                vehicle.acceleration() * distance_to_final + vehicle.speed() * vehicle.speed() / 2);
              max_speed = max_speed < speed_limit ? max_speed : speed_limit;
              double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

              if (std::abs(new_duration) < 1e-6) {
                update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
              } else {
                update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
              }
            }
          } else {
            // TODO: to review
            update(t, t + (final_position - position) / vehicle.speed(), VehicleData::State::RUNNING, false,
                   speed_limit);
          }
          break;
        }
        case VehicleData::State::IN_ACCELERATION: {
          if (vehicle.speed() < speed_limit) {
            artis::traffic::core::Time duration = (speed_limit - vehicle.speed()) / vehicle.acceleration();
            double acceleration_distance = (vehicle.speed() + 0.5 * vehicle.acceleration() * duration) * duration;
            artis::traffic::core::Time duration2 = speed_limit / vehicle.acceleration();
            double deceleration_distance = (speed_limit - 0.5 * vehicle.acceleration() * duration2) * duration2;

            if (position + acceleration_distance + deceleration_distance <= final_position) {
              update(t, t + (speed_limit - vehicle.speed()) / vehicle.acceleration(),
                     VehicleData::State::IN_ACCELERATION,
                     false, speed_limit);
            } else {

              // TODO: add RUNNING phase ?

              update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
            }
          } else { // vehicle.speed == speed_limit
            double distance = final_position - position;
            double deceleration_duration = vehicle.speed() / vehicle.acceleration();
            double deceleration_distance =
              (speed_limit - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;

            // if possible to decelerate and stop
            if (std::abs(distance - deceleration_distance) < 1e-6) {
              update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
            } else if (distance > deceleration_distance) {
              update(t, t + (distance - deceleration_distance) / vehicle.speed(), VehicleData::State::RUNNING, false,
                     speed_limit);
            } else {

              assert(false);

            }
          }
          break;
        }
        case VehicleData::State::IN_DECELERATION: {
          double b_n = -vehicle.max_acceleration();
          double b_ = -leader->vehicle.acceleration();
          double g = vehicle.gap() + vehicle.length();
          double v_n = vehicle.speed();
          double tau = 1;
          double c0 = 3 * b_n * tau * v_n + 2 * b_n * (g - position_difference)
                      + b_n / b_ * leader_speed * leader_speed - v_n * v_n;
          double c1 = b_n * tau * (2 * v_n - tau * b_);
          double K = ceil(-c0 / c1);
          K = K < 0 ? 0 : K;
          double T = t + K * tau;

          double leader_duration = leader_speed / leader->vehicle.acceleration();
          double leader_position_to_stop =
            leader_position + (leader_speed - 0.5 * leader->vehicle.acceleration() * leader_duration) * leader_duration;
          double duration = vehicle.speed() / vehicle.acceleration();
          double position_to_stop = position + (vehicle.speed() - 0.5 * vehicle.acceleration() * duration) * duration;
          double running_duration =
            (leader_position_to_stop - position_to_stop - vehicle.gap() - vehicle.length()) / vehicle.speed();

          running_duration = running_duration < K * tau ? running_duration : K * tau;
          if (leader->states.back().final_speed == 0 and running_duration > 1e-6) {
            update(t, t + running_duration, VehicleData::State::RUNNING, false, vehicle.speed());
          } else {
            if (std::abs(T - t) < 1e-6) {
              vehicle.update_acceleration(std::min(vehicle.max_acceleration(),
                                                   0.5 * vehicle.speed() * vehicle.speed() /
                                                   (leader_position_to_stop - vehicle.gap() - vehicle.length() -
                                                    position)));
              update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
            } else {
              if (speed_difference < 0) { // leader speed < vehicle speed
                update_deceleration_leader_follower(t, position, leader, leader_speed, leader_position);
              } else {
                update(t, T, VehicleData::State::RUNNING, false, vehicle.speed());
              }
            }
          }
          break;
        }
      }
      break;
    }
      // ***********************
      // state = in deceleration
    case VehicleData::State::IN_DECELERATION:
      if (vehicle.speed() == 0) {
        if (std::abs(final_position - position) < 1e-6) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
        } else {
          double distance = final_position - position;
          double max_speed = sqrt(vehicle.acceleration() * distance);
          max_speed = max_speed < speed_limit ? max_speed : speed_limit;
          double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

          update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
        }
      } else {
        if (leader->vehicle.data().state == VehicleData::State::RUNNING) {
          if (speed_difference < 0) {  // leader speed < vehicle speed
//            vehicle.acceleration = std::min(vehicle.max_acceleration,
//                                            (speed_difference * speed_difference / 2) /
//                                            (position_difference - vehicle.gap - vehicle.length));

            double duration =
              (vehicle.gap() + vehicle.length() - position_difference) / (speed_difference) + 0.5 * speed_difference;

            vehicle.update_acceleration(-speed_difference / duration);

            assert(vehicle.acceleration() > 0);

            update(t, t - speed_difference / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false,
                   leader_speed);
          } else { // leader speed > vehicle speed AND vehicle state = IN_DECELERATION AND leader state = RUNNING

            // TODO: on peut complexifier en calculant un nouveau comportement

            update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::RUNNING, false,
                   vehicle.speed());

          }
        } else if (leader->vehicle.data().state == VehicleData::State::IN_ACCELERATION) {
          double leader_final_speed = leader->states.back().final_speed;
          double leader_acceleration = leader->vehicle.acceleration();
          double leader_acceleration_duration = (leader_final_speed - leader_speed) / leader_acceleration;
          double future_leader_position = leader_position + (leader_speed +
                                                             0.5 * leader->vehicle.acceleration() *
                                                             leader_acceleration_duration) *
                                                            leader_acceleration_duration;

          if (leader_final_speed > vehicle.speed()) {
            double alpha = future_leader_position - (position + vehicle.speed() * leader_acceleration_duration) -
                           vehicle.gap() - vehicle.length();
            double follower_acceleration_duration =
              2 * alpha / (leader_final_speed - vehicle.speed()) + leader_acceleration_duration;
            double distance = final_position - position;
            double max_speed = std::min(
              sqrt(vehicle.max_acceleration() * distance + 0.5 * vehicle.speed() * vehicle.speed()),
              speed_limit);
            double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

            if (follower_acceleration_duration > 0 and
                leader_acceleration_duration + follower_acceleration_duration < new_duration) {
              double new_acceleration = std::min(vehicle.max_acceleration(), (leader_final_speed - vehicle.speed()) /
                                                                             (leader_acceleration_duration +
                                                                              follower_acceleration_duration));
              vehicle.update_acceleration(new_acceleration);
              update(t, t + leader_acceleration_duration + follower_acceleration_duration,
                     VehicleData::State::IN_ACCELERATION, false, leader_final_speed);
            } else {
              if (std::abs(new_duration) < 1e-6) {
                vehicle.update_acceleration(vehicle.max_acceleration());
                update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
              } else {
                update_acceleration_leader_follower(t, position, leader, leader_position, leader_speed,
                                                    leader_final_speed,
                                                    leader_acceleration_duration, future_leader_position);

              }
            }
          } else { // final leader speed <= follower speed
//            double new_acceleration = 2. * (vehicle.gap() + vehicle.length() + position +
//                                            vehicle.speed() * leader_acceleration_duration - future_leader_position) /
//                                      (leader_acceleration_duration * leader_acceleration_duration);
//            double follower_final_speed = vehicle.speed() - new_acceleration * leader_acceleration_duration;
//
//            update(t, t + leader_acceleration_duration, VehicleData::KinematicState::IN_DECELERATION, false,
//                   follower_final_speed);

            double distance = final_position - position;
            double deceleration_duration = vehicle.speed() / vehicle.acceleration();
            double deceleration_distance =
              (vehicle.speed() - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;

            if (std::abs(distance - deceleration_distance) < 1e-6) {
              vehicle.update_acceleration(vehicle.max_acceleration());
              update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
            } else {
              vehicle.update_acceleration(std::min(vehicle.max_acceleration(),
                                                   (vehicle.speed() - leader_final_speed) /
                                                   leader_acceleration_duration));
              update(t, t + leader_acceleration_duration, VehicleData::State::IN_DECELERATION, false,
                     vehicle.speed() - vehicle.acceleration() * leader_acceleration_duration);
            }
          }
        } else { // leader state = IN_DECELERATION
          double leader_final_speed = leader->states.back().final_speed;
          double leader_acceleration = leader->vehicle.acceleration();
          double leader_deceleration_duration = (leader_speed - leader_final_speed) / leader_acceleration;
          double future_leader_position = leader_position + (leader_speed -
                                                             0.5 * leader->vehicle.acceleration() *
                                                             leader_deceleration_duration) *
                                                            leader_deceleration_duration;
          double alpha = future_leader_position - (position + vehicle.speed() * leader_deceleration_duration) -
                         vehicle.gap() - vehicle.length();
          double follower_deceleration_duration =
            2 * alpha / (vehicle.speed() - leader_final_speed) + leader_deceleration_duration;
          double new_acceleration = std::min(vehicle.max_acceleration(), (vehicle.speed() - leader_final_speed) /
                                                                         (leader_deceleration_duration +
                                                                          follower_deceleration_duration));
          double future_position =
            position + vehicle.speed() * (leader_deceleration_duration + follower_deceleration_duration)
            - 0.5 * new_acceleration * (leader_deceleration_duration + follower_deceleration_duration) *
              (leader_deceleration_duration + follower_deceleration_duration) +
            0.5 * leader_final_speed * leader_final_speed / vehicle.max_acceleration();

          if (follower_deceleration_duration > 0 and new_acceleration > 0 and
              (std::abs(future_position - final_position) < 1e-6 or future_position <= final_position)) {
            vehicle.update_acceleration(new_acceleration);
            update(t, t + leader_deceleration_duration + follower_deceleration_duration,
                   VehicleData::State::IN_DECELERATION, false, leader_final_speed);
          } else {
            new_acceleration = 0.5 * vehicle.speed() * vehicle.speed() / (final_position - position);
            vehicle.update_acceleration(
              new_acceleration < vehicle.max_acceleration() ? new_acceleration : vehicle.max_acceleration());
            update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
          }
        }
      }
      break;
      // ***********************
      // state = in acceleration
    case VehicleData::State::IN_ACCELERATION: {
      if (leader->vehicle.data().state == VehicleData::State::RUNNING or
          leader->vehicle.data().state == VehicleData::State::IN_ACCELERATION) {
        double deceleration_duration = vehicle.speed() / vehicle.acceleration();
        double deceleration_distance =
          (vehicle.speed() - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;
        double distance = final_position - position - deceleration_distance;

        // it's possible to decelerate and speed < speed max
        if (distance > 1e-6 and vehicle.speed() < speed_limit) {
          double max_speed = sqrt(
            vehicle.acceleration() * (final_position - position) + vehicle.speed() * vehicle.speed() / 2);
          max_speed = max_speed < speed_limit ? max_speed : speed_limit;
          double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

          update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
        } else {
          // if possible to decelerate and stop
          if ((final_position - position) / vehicle.speed() > vehicle.speed() / vehicle.acceleration()) {
            update(t, t + (final_position - position) / vehicle.speed() - vehicle.speed() / vehicle.acceleration(),
                   VehicleData::State::RUNNING, false, vehicle.speed());
          } else if (go and leader->is_first and
                     link_length - leader_position < leader_speed / leader->vehicle.acceleration()) {
            double new_distance = final_position - position;
            double new_duration =
              (std::sqrt(vehicle.speed() * vehicle.speed() + 2 * vehicle.acceleration() * new_distance) -
               vehicle.speed()) / vehicle.acceleration();
            double final_speed = vehicle.speed() + vehicle.acceleration() * new_duration;

            if (final_speed < speed_limit) {
              update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, final_speed);
            } else {
              update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
            }
          } else {
            update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
          }
        }
      } else { // leader state = IN_DECELERATION or STOPPED AND vehicle state = IN_ACCELERATION
        double leader_final_speed = leader->states.back().final_speed;
        double leader_acceleration = leader->vehicle.acceleration();
        double leader_deceleration_duration = (leader_speed - leader_final_speed) / leader_acceleration;
        double future_leader_position = leader_position + (leader_speed -
                                                           0.5 * leader_acceleration * leader_deceleration_duration) *
                                                          leader_deceleration_duration;
        double alpha = future_leader_position - leader_final_speed * leader_deceleration_duration - position -
                       vehicle.gap() - vehicle.length();
        double new_acceleration =
          (leader_final_speed - vehicle.speed()) * (leader_final_speed - vehicle.speed()) / (2 * alpha);

        if (new_acceleration > vehicle.max_acceleration() and
            std::abs(new_acceleration - vehicle.max_acceleration()) > 1e-6) {

          std::cerr << "warning: adjust speed (" << vehicle.speed() << " to "
                    << std::abs(leader_final_speed - std::sqrt(2 * alpha)) << ")" << std::endl;

          new_acceleration = vehicle.max_acceleration();
          vehicle.update_speed(std::abs(leader_final_speed - std::sqrt(2 * alpha)));
        }
        new_acceleration = std::min(vehicle.max_acceleration(), new_acceleration);

        double follower_deceleration_duration = (vehicle.speed() - leader_final_speed) / new_acceleration -
                                                leader_deceleration_duration;
        double future_position =
          position + vehicle.speed() * (leader_deceleration_duration + follower_deceleration_duration)
          - 0.5 * new_acceleration * (leader_deceleration_duration + follower_deceleration_duration) *
            (leader_deceleration_duration + follower_deceleration_duration) +
          0.5 * leader_final_speed * leader_final_speed / vehicle.max_acceleration();

        if (follower_deceleration_duration > 0 and new_acceleration > 0 and
            (std::abs(future_position - final_position) < 1e-6 or future_position <= final_position)) {
          double follower_final_speed =
            vehicle.speed() - new_acceleration * (leader_deceleration_duration + follower_deceleration_duration);

          follower_final_speed = std::abs(follower_final_speed) < 1e-6 ? 0 : follower_final_speed;
          vehicle.update_acceleration(new_acceleration);
          update(t, t + leader_deceleration_duration + follower_deceleration_duration,
                 VehicleData::State::IN_DECELERATION, false, follower_final_speed);
        } else {
          new_acceleration = 0.5 * vehicle.speed() * vehicle.speed() / (final_position - position);
          vehicle.update_acceleration(
            new_acceleration < vehicle.max_acceleration() ? new_acceleration : vehicle.max_acceleration());
          update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
        }
      }
      break;
    }
      // ***************
      // state = stopped
    case VehicleData::State::STOPPED: {
      assert(false);
    }
      // ***********************
      // state = restart or halt
    case VehicleData::State::HALT:
    case VehicleData::State::RESTART: {
      switch (leader->vehicle.data().state) {
        case VehicleData::State::STOPPED:
        case VehicleData::State::RESTART:
        case VehicleData::State::HALT: {
          assert(false);
        }
        case VehicleData::State::IN_ACCELERATION:
        case VehicleData::State::RUNNING:
        case VehicleData::State::IN_DECELERATION: {
          double distance = final_position - position;
          double max_speed = sqrt(vehicle.acceleration() * distance + vehicle.speed() * vehicle.speed() / 2);
          max_speed = max_speed < speed_limit ? max_speed : speed_limit;
          double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

          update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
          break;
        }
      }
      if (follower) {
        if (follower->states.back().state == VehicleData::State::STOPPED) {
          follower->update(t, t + follower->vehicle.reaction_time(), VehicleData::State::RESTART, false, 0);
        }
      }
      break;
    }
      // TODO
    case VehicleData::State::TRANSITION_STOPPED: {
      vehicle.update_speed(vehicle.max_speed());
      update(t, t, VehicleData::State::RUNNING, false, vehicle.max_speed());
    }
  }
  if (follower and
      (current_state.state != states.back().state or current_state.acceleration != states.back().acceleration)) {
    follower->propagate_disturbance(t);
  }
}

void KinematicVehicleDynamics::next_state_without_constraint(const traffic::core::Time &t, double final_position,
                                                             KinematicVehicleDynamics *follower,
                                                             const KinematicVehicleDynamics *leader,
                                                             double position, double leader_speed,
                                                             double /* speed_difference */,
                                                             double leader_position,
                                                             double /* position_difference */, bool go,
                                                             const artis::traffic::core::Time &next_opening_time,
                                                             const artis::traffic::core::Time &next_closing_time) {
  const auto &current_state = states.back();

  vehicle.update_acceleration(vehicle.max_acceleration());
  switch (states.back().state) {
    case VehicleData::State::RUNNING: {
      if (std::abs(position - link_length) < 1e-6) {
        update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, speed_limit);
      } else if ((is_first or
                  (leader and leader->vehicle.data().state == VehicleData::State::RUNNING and
                   vehicle.speed() == vehicle.max_speed() and
                   leader->is_first)) and go and
                 (next_closing_time == -1 or next_closing_time > t + (final_position - position) / vehicle.speed())) {
        artis::traffic::core::Time deceleration_duration = vehicle.speed() / vehicle.acceleration();
        double deceleration_distance =
          (vehicle.speed() - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;
        double distance_to_final_position = final_position - position;
        double distance = distance_to_final_position - deceleration_distance;

        if (distance < 1e-6) {
          update(t, t + (link_length - position) / vehicle.speed(), VehicleData::State::RUNNING, false, speed_limit);
        } else {
          update(t, t + distance / vehicle.speed(), VehicleData::State::RUNNING, false, vehicle.speed());
        }
      } else {
        artis::traffic::core::Time duration = vehicle.speed() / vehicle.acceleration();
        double deceleration_distance = (vehicle.speed() - 0.5 * vehicle.acceleration() * duration) * duration;
        double distance_to_final_position = final_position - position;
        double distance = distance_to_final_position - deceleration_distance;

        if (std::abs(distance) < 1e-6 and
            ((next_opening_time > 0 and next_opening_time - t > duration) or next_opening_time == -1)) {
          update(t, t + duration, VehicleData::State::IN_DECELERATION, false, 0);
        } else {
          if (distance > 1e-6) {
            update(t, t + distance / vehicle.speed(), VehicleData::State::RUNNING, false, vehicle.speed());
          } else { // distance < 1e-6
            if (next_opening_time > 0 and next_opening_time - t <= duration) {
              update(t, next_opening_time, VehicleData::State::IN_DECELERATION, false, 0);
            } else {
              assert(false);
            }
          }
        }
      }
      break;
    }
    case VehicleData::State::IN_DECELERATION: {
      if (vehicle.speed() == speed_limit) {
        update(t, t + (final_position - position) / vehicle.speed() - vehicle.speed() / vehicle.acceleration(),
               VehicleData::State::RUNNING, false, speed_limit);
      } else if (vehicle.speed() == 0) {
        if (std::abs(link_length - position) < 1e-6) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
        } else {
          if (is_first) {
            if (std::abs(final_position - position) < 1e-6) {
              update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
            } else {
              double distance = final_position - position;
              double max_speed = sqrt(vehicle.acceleration() * distance);
              max_speed = max_speed < speed_limit ? max_speed : speed_limit;
              double new_duration = max_speed / vehicle.acceleration();

              update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false,
                     vehicle.acceleration() * new_duration);
            }
          } else {
            if (leader and leader->states.back().state == VehicleData::State::HALT) {
              update(t, artis::common::DoubleTime::infinity, VehicleData::State::HALT, false, 0);
            } else if (std::abs(position - final_position) < 1e-6) {
              update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
            } else if (leader and leader->states.back().state == VehicleData::State::RESTART) {
              update(t, artis::common::DoubleTime::infinity, VehicleData::State::HALT, true, 0);
            } else {
              if (leader and leader->states.back().state == VehicleData::State::IN_ACCELERATION) {
                double leader_final_speed = leader->states.back().final_speed;
                double leader_acceleration = leader->vehicle.acceleration();
                double leader_acceleration_duration = (leader_final_speed - leader_speed) / leader_acceleration;
                double future_leader_position = leader_position + (leader_speed +
                                                                   0.5 * leader->vehicle.acceleration() *
                                                                   leader_acceleration_duration) *
                                                                  leader_acceleration_duration;
                double alpha = future_leader_position - (position + vehicle.speed() * leader_acceleration_duration) -
                               vehicle.gap() - vehicle.length();
                double follower_acceleration_duration =
                  2 * alpha / (leader_final_speed - vehicle.speed()) + leader_acceleration_duration;
                double new_acceleration = std::min(vehicle.max_acceleration(), (leader_final_speed - vehicle.speed()) /
                                                                               (leader_acceleration_duration +
                                                                                follower_acceleration_duration));
                vehicle.update_acceleration(new_acceleration);
                update(t, t + leader_acceleration_duration + follower_acceleration_duration,
                       VehicleData::State::IN_ACCELERATION, false, leader_final_speed);
              } else {
                double distance = final_position - position;
                double max_speed = sqrt(vehicle.acceleration() * distance);
                max_speed = max_speed < speed_limit ? max_speed : speed_limit;
                double new_duration = max_speed / vehicle.acceleration();

                update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false,
                       vehicle.acceleration() * new_duration);
              }
            }
          }
        }
      } else if (vehicle.speed() < speed_limit) {
        if (std::abs(position - final_position) < 1e-6) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
        } else {
          double distance = final_position - position;
          double deceleration_duration = vehicle.speed() / vehicle.acceleration();
          double deceleration_distance =
            (vehicle.speed() - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;

          if (std::abs(distance - deceleration_distance) < 1e-6) {
            update(t, t + deceleration_duration, VehicleData::State::IN_DECELERATION, false, 0);
          } else {
            if (leader and leader->vehicle.data().state == VehicleData::State::RUNNING) {
              const KinematicState &current_leader_state = leader->states.back();
              double leader_speed =
                current_leader_state.initial_speed + (t - current_leader_state.begin) *
                                                     (current_leader_state.final_speed -
                                                      current_leader_state.initial_speed) /
                                                     (leader->next_time - current_leader_state.begin);
              double speed_difference = leader_speed - vehicle.speed();

              if (speed_difference < 0) {  // leader speed < vehicle speed
                double leader_position = leader->compute_position(t);
                double position_difference = leader_position - position;

                vehicle.update_acceleration(std::min(vehicle.max_acceleration(),
                                                     (speed_difference * speed_difference / 2) /
                                                     (position_difference - vehicle.gap() - vehicle.length())));

                assert(vehicle.acceleration() > 0);

                update(t, t - speed_difference / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false,
                       leader_speed);
              } else { // leader speed > vehicle speed AND vehicle state = IN_DECELERATION AND leader state = RUNNING

                // TODO: on peut complexifier en calculant un nouveau comportement

                update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::RUNNING, false,
                       vehicle.speed());

              }
            } else {
              if (is_first and go) {
                double delta = vehicle.speed() * vehicle.speed() + 2 * vehicle.acceleration() * distance;
                double duration = (-vehicle.speed() + std::sqrt(delta)) / vehicle.acceleration();
                double max_speed = std::min(vehicle.max_speed(), vehicle.speed() + vehicle.acceleration() * duration);

                update(t, t + (max_speed - vehicle.speed()) / vehicle.acceleration(),
                       VehicleData::State::IN_ACCELERATION,
                       false, max_speed);
              } else {
                double max_speed = sqrt(vehicle.acceleration() * distance + vehicle.speed() * vehicle.speed() / 2);
                max_speed = max_speed < speed_limit ? max_speed : speed_limit;
                double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

                // TODO
                if (max_speed > vehicle.speed()) {
                  update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
                } else {
                  new_duration = (vehicle.speed() - max_speed) / vehicle.acceleration();
                  update(t, t + new_duration, VehicleData::State::IN_DECELERATION, false, max_speed);
                }
              }
            }
          }
        }
      } else if (vehicle.speed() > speed_limit) {
        // TODO: (final_position - position) / vehicle.speed() - vehicle.speed() / vehicle.acceleration() < 0
        update(t, t + (final_position - position) / vehicle.speed() - vehicle.speed() / vehicle.acceleration(),
               VehicleData::State::RUNNING, false, vehicle.speed());
      }
      break;
    }
    case VehicleData::State::IN_ACCELERATION: {
      artis::traffic::core::Time deceleration_duration = vehicle.speed() / vehicle.acceleration();
      double deceleration_distance = 0.5 * vehicle.acceleration() * deceleration_duration * deceleration_duration;
      // TODO
      artis::traffic::core::Time max_deceleration_duration = vehicle.max_speed() / vehicle.acceleration();
      double max_deceleration_distance =
        0.5 * vehicle.acceleration() * max_deceleration_duration * max_deceleration_duration;
      bool reached_critical = final_position - position - max_deceleration_distance < 1e-6;
      if (vehicle.speed() < speed_limit) {
        if (std::abs(position - link_length) < 1e-6) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::IN_ACCELERATION, true, speed_limit);
        } else if (is_first and go) {
          // TODO
          double delta = 0;
          if (reached_critical) {
            delta =
              vehicle.speed() * vehicle.speed() +
              2 * vehicle.acceleration() * std::abs(final_position - position);
          } else {
            delta =
              vehicle.speed() * vehicle.speed() +
              2 * vehicle.acceleration() * std::abs(final_position - position - max_deceleration_distance);
          }
          artis::traffic::core::Time new_duration = (std::sqrt(delta) - vehicle.speed()) / vehicle.acceleration();
          if (vehicle.speed() + new_duration * vehicle.acceleration() < speed_limit) {
            update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false,
                   vehicle.speed() + new_duration * vehicle.acceleration());
          } else {
            new_duration = (speed_limit - vehicle.speed()) / vehicle.acceleration();
            update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, speed_limit);
          }
        } else if ((final_position - position) - deceleration_distance > 1e-6) {
          double distance = final_position - position;
          double limit_speed = std::sqrt(vehicle.acceleration() * distance
                                         + vehicle.speed() * vehicle.speed() / 2);
          artis::traffic::core::Time
            new_duration = ((limit_speed < speed_limit ? limit_speed : speed_limit)
                            - vehicle.speed()) / vehicle.acceleration();

          update(t, t + new_duration, VehicleData::State::IN_ACCELERATION,
                 false, (limit_speed < speed_limit ? limit_speed : speed_limit));
        } else if (std::abs(deceleration_distance - (final_position - position)) < 1e-6) {
          update(t, t + deceleration_duration, VehicleData::State::IN_DECELERATION, false, 0);
        } else {

          // TODO: acceleration can > max_acceleration
          vehicle.update_acceleration(0.5 * vehicle.speed() * vehicle.speed() / (final_position - position));
          update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
//          assert(false);

        }
      } else { // vehicle.speed == speed_limit
        if (is_first and go) {
          double deceleration_distance =
            (vehicle.speed() - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;
          double distance_to_final_position = final_position - position;
          double distance = distance_to_final_position - deceleration_distance;

          if (distance > 0) {
            update(t, t + distance / vehicle.speed(), VehicleData::State::RUNNING, false, speed_limit);
          } else {
            update(t, t + distance_to_final_position / vehicle.speed(), VehicleData::State::RUNNING, false,
                   speed_limit);
          }
        } else {
          double distance = final_position - position;
          double deceleration_duration = vehicle.speed() / vehicle.acceleration();
          double deceleration_distance =
            (speed_limit - 0.5 * vehicle.acceleration() * deceleration_duration) * deceleration_duration;

          // if possible to decelerate and stop
          if (std::abs(distance - deceleration_distance) < 1e-6) {
            update(t, t + vehicle.speed() / vehicle.acceleration(), VehicleData::State::IN_DECELERATION, false, 0);
          } else if (distance > deceleration_distance) {
            update(t, t + (distance - deceleration_distance) / vehicle.speed(), VehicleData::State::RUNNING, false,
                   speed_limit);
          } else if (is_first) {

            assert(false);

          } else {

            assert(false);

          }
        }
      }
      break;
    }
    case VehicleData::State::HALT:
    case VehicleData::State::STOPPED: {
      assert(false);
    }
    case VehicleData::State::RESTART: {
      // TODO: merge with else ?
      if (is_first and go) {
        if (std::abs(position - link_length) < 1e-6) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::IN_ACCELERATION, true, speed_limit);
        } else {
          artis::traffic::core::Time duration = speed_limit / vehicle.acceleration();
          double acceleration_distance = 0.5 * vehicle.acceleration() * duration * duration;

          // TODO
          double max_deceleration_duration = speed_limit / vehicle.acceleration();
          double max_deceleration_distance =
            (speed_limit - 0.5 * vehicle.acceleration() * max_deceleration_duration) * max_deceleration_duration;
          if (acceleration_distance <= final_position - position - max_deceleration_distance) {
            update(t, t + duration, VehicleData::State::IN_ACCELERATION, false, speed_limit);
          } else {
            artis::traffic::core::Time new_duration = std::sqrt(
              2 * std::abs(final_position - position - max_deceleration_distance) / vehicle.acceleration());

            update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false,
                   vehicle.acceleration() * new_duration);
          }
        }
      } else {
        artis::traffic::core::Time duration = speed_limit / vehicle.acceleration();
        double acceleration_distance = 0.5 * vehicle.acceleration() * duration * duration;

        // TODO
        double max_deceleration_duration = speed_limit / vehicle.acceleration();
        double max_deceleration_distance =
          (speed_limit - 0.5 * vehicle.acceleration() * max_deceleration_duration) * max_deceleration_duration;
        if (acceleration_distance * 2 <= final_position - position - max_deceleration_distance) {
          update(t, t + duration, VehicleData::State::IN_ACCELERATION, false, speed_limit);
        } else {
          if (std::abs(position - link_length) < 1e-6) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, true, 0);
          } else {
            // TODO
//            artis::traffic::core::Time new_duration = std::sqrt(
//              std::abs(final_position - position - max_deceleration_distance) / vehicle.acceleration());
//
//            if (std::abs(new_duration) > 1e-6) {
//              update(t, t + new_duration, VehicleData::KinematicState::IN_ACCELERATION, false,
//                     vehicle.acceleration() * new_duration);
//            } else {
//              update(t, artis::common::DoubleTime::infinity, VehicleData::KinematicState::STOPPED, false, 0);
//            }
            double distance = final_position - position;
            double max_speed = sqrt(vehicle.acceleration() * distance + vehicle.speed() * vehicle.speed() / 2);
            max_speed = max_speed < speed_limit ? max_speed : speed_limit;
            double new_duration = (max_speed - vehicle.speed()) / vehicle.acceleration();

            update(t, t + new_duration, VehicleData::State::IN_ACCELERATION, false, max_speed);
          }
        }
      }
      if (follower) {
        if (follower->states.back().state == VehicleData::State::STOPPED) {
          follower->update(t, t + follower->vehicle.reaction_time(), VehicleData::State::RESTART, false, 0);
        } else if (current_state.state != states.back().state or
                   current_state.acceleration != states.back().acceleration) {
          follower->propagate_disturbance(t);
        }
      }
      break;
    }
      // TODO
    case VehicleData::State::TRANSITION_STOPPED: {
      vehicle.update_speed(vehicle.max_speed());
      update(t, t, VehicleData::State::RUNNING, false, vehicle.max_speed());
    }
  }
}

void
KinematicVehicleDynamics::process_disturbance(const artis::traffic::core::Time &t, const Disturbance &disturbance,
                                              size_t index,
                                              KinematicVehicleDynamics *follower) {
  VehicleData::State::values new_state;
  double final_speed = 0;

  if (index == 0) {
    this->disturbance.disturbance = std::make_unique<Disturbance>(disturbance);
    this->disturbance.index = 0;
  }
  if (this->disturbance.disturbance->type == DisturbanceTypes::ACCELERATION) {
    switch (index) {
      case 0: {
        new_state = VehicleData::State::IN_ACCELERATION;
        final_speed = vehicle.speed() + vehicle.acceleration() * this->disturbance.disturbance->durations[index];
        break;
      }
      case 1: {
        new_state = VehicleData::State::RUNNING;
        final_speed = vehicle.speed();
        break;
      }
      case 2: {
        new_state = VehicleData::State::IN_DECELERATION;
        final_speed = vehicle.max_speed();
        break;
      }
      default:
        assert(false);
    }
  } else {
    switch (index) {
      case 0: {
        new_state = VehicleData::State::IN_DECELERATION;
        final_speed = vehicle.speed() - vehicle.acceleration() * this->disturbance.disturbance->durations[index];
        break;
      }
      case 1: {
        if (std::abs(vehicle.speed()) < 1e-6) {
          new_state = VehicleData::State::HALT;
          final_speed = 0;
        } else {
          new_state = VehicleData::State::RUNNING;
          final_speed = vehicle.speed();
        }
        break;
      }
      case 2: {
        new_state = VehicleData::State::IN_ACCELERATION;
        final_speed = vehicle.max_speed();
        break;
      }
      default:
        assert(false);
    }
  }
  update(t, t + this->disturbance.disturbance->durations[index], new_state, false, final_speed);
  if (follower) {
    follower->propagate_disturbance(t);
  }
}

void KinematicVehicleDynamics::propagate_disturbance(const artis::traffic::core::Time &t) {
  const VehicleData::State::values &state = states.back().state;
  double final_speed = 0;
  double duration = vehicle.reaction_time();

  switch (state) {
    case VehicleData::State::STOPPED:
      return;
    case VehicleData::State::HALT:
    case VehicleData::State::RESTART:
      final_speed = 0;
      break;
    case VehicleData::State::RUNNING:
      final_speed = states.back().final_speed;
      if (next_time - t < duration) {
        duration = next_time - t;
      }
      break;
    case VehicleData::State::IN_ACCELERATION:
      vehicle.update_speed(states.back().final_speed - vehicle.acceleration() * (next_time - t));
      if (next_time - t > duration) {
        final_speed = states.back().final_speed - vehicle.acceleration() * (next_time - t - duration);
      } else {
        duration = next_time - t;
        final_speed = states.back().final_speed;
      }
      break;
    case VehicleData::State::IN_DECELERATION:
      vehicle.update_speed(states.back().final_speed + vehicle.acceleration() * (next_time - t));
      if (next_time - t > duration) {
        final_speed = states.back().final_speed + vehicle.acceleration() * (next_time - t - duration);
      } else {
        duration = next_time - t;
        final_speed = states.back().final_speed;
      }
      break;
  }
  if (std::abs(duration) > 1e-6) {
    update(t, t + duration, states.back().state, false, final_speed);
  }
}

void KinematicVehicleDynamics::update(const artis::traffic::core::Time &t, const artis::traffic::core::Time &next_time,
                                      const VehicleData::State::values &new_state,
                                      bool ready_to_exit, double final_speed) {
  artis::traffic::core::Time adjusted_next_time = std::abs(t - next_time) < 1e-6 ? t : next_time;

  assert(t <= adjusted_next_time);
  assert(new_state != VehicleData::State::IN_ACCELERATION or
         (new_state == VehicleData::State::IN_ACCELERATION and vehicle.speed() < final_speed));
  assert(new_state != VehicleData::State::IN_DECELERATION or
         (new_state == VehicleData::State::IN_DECELERATION and vehicle.speed() > final_speed));

  auto current_state = states.rbegin();
  KinematicState next_state{new_state, ready_to_exit, t, vehicle.speed(), final_speed, vehicle.acceleration()};

  if (current_state == states.rend() or *current_state != next_state) {
    states.push_back(next_state);
    vehicle.data().state = new_state;
  }
  this->next_time = adjusted_next_time;
  // if speed is modified by propagate_disturbance
  vehicle.update_speed(final_speed);
}

void KinematicVehicleDynamics::update_acceleration_leader_follower(const traffic::core::Time &t, double position,
                                                                   const KinematicVehicleDynamics *leader,
                                                                   double leader_position, double leader_speed,
                                                                   double leader_final_speed,
                                                                   double leader_acceleration_duration,
                                                                   double future_leader_position) {
//  double leader_speed = leader->vehicle.speed();
  double leader_acceleration = leader->vehicle.acceleration();
  double future_leader_position_to_final =
    future_leader_position +
    0.5 * leader_final_speed * leader_final_speed / leader->vehicle.max_acceleration();
  double follower_acceleration_duration = leader_acceleration_duration + vehicle.reaction_time();
  double C = 1. / vehicle.max_acceleration() * vehicle.speed() * vehicle.speed() -
             2. * (future_leader_position_to_final - vehicle.gap() - vehicle.length() - position -
                   vehicle.speed() * follower_acceleration_duration);
  double B = follower_acceleration_duration *
             (follower_acceleration_duration + 2. / vehicle.max_acceleration() * vehicle.speed());
  double A =
    1. / vehicle.max_acceleration() * follower_acceleration_duration * follower_acceleration_duration;
  double delta = B * B - 4 * A * C;
  double new_acceleration_1 = (-B + sqrt(delta)) / (2 * A);
  double new_acceleration_2 = (-B - sqrt(delta)) / (2 * A);
  double new_acceleration =
    (new_acceleration_1 < new_acceleration_2 and new_acceleration_1 > 0) or new_acceleration_2 < 0
    ? new_acceleration_1 : new_acceleration_2;
  double follower_final_speed = vehicle.speed() + new_acceleration * follower_acceleration_duration;

  // assertions
  {
    double future_follower_position = position + (vehicle.speed() + 0.5 * new_acceleration *
                                                                    follower_acceleration_duration) *
                                                 follower_acceleration_duration;
    double future_follower_position_to_final =
      future_follower_position + 0.5 * (follower_final_speed * follower_final_speed) /
                                 vehicle.max_acceleration();

    assert(std::abs(future_leader_position_to_final - future_follower_position_to_final - vehicle.gap() -
                    vehicle.length()) < 1e-6);
  }

  if (follower_final_speed > vehicle.max_speed()) {
    new_acceleration = (vehicle.max_speed() - vehicle.speed()) / follower_acceleration_duration;
    follower_final_speed = vehicle.max_speed();
  }

  // verify security distance in [t, t + follower_acceleration_duration]
//  double a1 = -0.5;
//  double a0 = 0.5 * leader_acceleration;
//  double b1 = -vehicle.reaction_time();
//  double b0 = leader_speed - vehicle.speed();
//  double c1 = -0.5 * vehicle.reaction_time() * vehicle.reaction_time();
//  double c0 = leader_position - position - vehicle.speed() * vehicle.reaction_time() - vehicle.gap() - vehicle.length();
//  double a1 = -0.5;
//  double a0 = 0.5 * leader_acceleration;
//  double b1 = -vehicle.reaction_time();
//  double b0 = leader_speed + leader_acceleration * vehicle.reaction_time() - vehicle.speed();
//  double c1 = -0.5 * vehicle.reaction_time() * vehicle.reaction_time();
//  double c0 = leader_position + leader_speed * vehicle.reaction_time() - position - vehicle.gap() - vehicle.length() -
//              0.5 * leader->vehicle.max_acceleration() * vehicle.reaction_time() * vehicle.reaction_time() -
//              vehicle.speed() * vehicle.reaction_time();
//  double A_ = a0 + a1 * new_acceleration;
//  double B_ = b0 + b1 * new_acceleration;
//  double C_ = c0 + c1 * new_acceleration;
//  double delta_ = B_ * B_ - 4 * A_ * C_;
//  double t1 = (-B_ - std::sqrt(delta_)) / (2 * A_);
//  double t2 = (-B_ + std::sqrt(delta_)) / (2 * A_);

//  double v_l = leader_speed + leader_acceleration * t1 - 1;
//  double v_f = vehicle.speed() + new_acceleration * t2;
//  double x_l = leader_position + leader_speed * t1 + 0.5 * leader_acceleration * t1 * t1 +
//               (leader_speed + leader_acceleration * t1) * 1 - 0.5 * 1 * 1 * 1 +
//               v_l * v_l / leader->vehicle.max_acceleration() -
//               0.5 * leader->vehicle.max_acceleration() * v_l / leader->vehicle.max_acceleration() * v_l /
//               leader->vehicle.max_acceleration();
//  double x_f = position + vehicle.speed() * t2 + 0.5 * new_acceleration * t2 * t2 + +
//                                                                                      v_f * v_f /
//                                                                                    vehicle.max_acceleration() -
//               0.5 * vehicle.max_acceleration() * v_f / vehicle.max_acceleration() * v_f /
//               vehicle.max_acceleration();
//
//  if ((t1 > 0 and t1 < follower_acceleration_duration) or (t2 > 0 and t2 < follower_acceleration_duration)) {
//    double A = b1 * b1 - 4 * a1 * c1;
//    double B = 2 * b0 * b1 - 4 * (a0 * c1 + a1 * c0);
//    double C = b0 * b0 - 4 * a0 * c0;
//
////    new_acceleration = -C / B;
//  }

//  double c0 = leader_position + 0.5 * leader_speed * leader_speed / leader->vehicle.max_acceleration() - position -
//              vehicle.speed() * vehicle.reaction_time() -
//              0.5 * vehicle.speed() * vehicle.speed() / vehicle.max_acceleration() - vehicle.gap() - vehicle.length();
//  double c1 = -0.5 * vehicle.reaction_time() * vehicle.reaction_time() -
//              vehicle.reaction_time() * vehicle.speed() / vehicle.max_acceleration();
//  double c2 = -0.5 * vehicle.reaction_time() * vehicle.reaction_time() / vehicle.max_acceleration();
//
//  double b0 = leader_speed + leader_speed * leader_acceleration / leader->vehicle.max_acceleration() - vehicle.speed();
//  double b1 = -vehicle.reaction_time() - vehicle.speed() / vehicle.max_acceleration();
//  double b2 = -vehicle.reaction_time();
//
//  double a0 =
//    0.5 * leader_acceleration + 0.5 * leader_acceleration * leader_acceleration / leader->vehicle.max_acceleration();
//  double a1 = -0.5;
//  double a2 = -0.5 / vehicle.max_acceleration();
//
//  double delta_ = c1 * c1 - 4 * c2 * c0;
//  double t1 = (-c1 - std::sqrt(delta_)) / (2 * c2);
//  double t2 = (-c1 + std::sqrt(delta_)) / (2 * c2);
//
//  double p0 = c0 + c1 * new_acceleration + c2 * new_acceleration * new_acceleration;
//  double p1 = b0 + b1 * new_acceleration + b2 * new_acceleration * new_acceleration;
//  double p2 = a0 + a1 * new_acceleration + a2 * new_acceleration * new_acceleration;
//
//  double p = p0 + p1 * leader_acceleration_duration + p2 * leader_acceleration_duration * leader_acceleration_duration;

  // TODO
  if (new_acceleration < 0 or new_acceleration > vehicle.max_acceleration()) {
    std::cerr << "warning: acceleration > max_acceleration (" << new_acceleration << ")" <<
              " time: " << t << " vehicle: " << vehicle.index() << std::endl;
  }
//  assert(new_acceleration > 0 and new_acceleration < vehicle.max_acceleration());

  if (follower_final_speed > vehicle.max_speed()) {
    follower_final_speed = vehicle.max_speed();
    follower_acceleration_duration = vehicle.speed() / new_acceleration;
  }
  vehicle.update_acceleration(new_acceleration);
  update(t, t + follower_acceleration_duration, VehicleData::State::IN_ACCELERATION, false, follower_final_speed);
}

void KinematicVehicleDynamics::update_deceleration_leader_follower(const traffic::core::Time &t, double position,
                                                                   const KinematicVehicleDynamics *leader,
                                                                   double leader_speed, double leader_position) {
  double leader_final_speed = leader->states.back().final_speed;
  double leader_acceleration = leader->vehicle.acceleration();
  double leader_deceleration_duration = (leader_speed - leader_final_speed) / leader_acceleration;
  double future_leader_position = leader_position + (leader_speed - 0.5 * leader->vehicle.acceleration() *
                                                                    leader_deceleration_duration) *
                                                    leader_deceleration_duration;
  double future_leader_position_to_final = future_leader_position + 0.5 * leader_final_speed * leader_final_speed /
                                                                    leader->vehicle.max_acceleration();
  double follower_deceleration_duration = leader_deceleration_duration + vehicle.reaction_time();

  double C_ = future_leader_position_to_final - vehicle.gap() - vehicle.length() - position -
              vehicle.speed() * follower_deceleration_duration -
              0.5 / vehicle.max_acceleration() * vehicle.speed() * vehicle.speed();
  double B_ = follower_deceleration_duration *
              (0.5 * follower_deceleration_duration + 1. / vehicle.max_acceleration() * vehicle.speed());
  double A_ = -0.5 / vehicle.max_acceleration() * follower_deceleration_duration * follower_deceleration_duration;
  double delta = B_ * B_ - 4 * A_ * C_;
  double new_acceleration_1 = (-B_ + std::sqrt(delta)) / (2 * A_);
  double new_acceleration_2 = (-B_ - std::sqrt(delta)) / (2 * A_);

  double new_acceleration =
    (new_acceleration_1 < new_acceleration_2 and new_acceleration_1 > 0) or new_acceleration_2 < 0
    ? new_acceleration_1 : new_acceleration_2;

  new_acceleration = std::min(vehicle.max_acceleration(), new_acceleration);

//  assert(
//    std::abs(new_acceleration - vehicle.max_acceleration()) < 1e-6 or new_acceleration <= vehicle.max_acceleration());


  // verify security distance in [t, t + follower_deceleration_duration]
  double a1 = 0.5;
  double a0 = -0.5 * leader_acceleration;
  double b1 = vehicle.reaction_time();
  double b0 = leader_speed - vehicle.speed();
  double c1 = 0.5 * vehicle.reaction_time() * vehicle.reaction_time();
  double c0 = leader_position - position - vehicle.speed() * vehicle.reaction_time() - vehicle.gap() - vehicle.length();
  double A__ = a0 + a1 * new_acceleration;
  double B__ = b0 + b1 * new_acceleration;
  double C__ = c0 + c1 * new_acceleration;
  double delta__ = B__ * B__ - 4 * A__ * C__;
  double t1 = (-B__ - std::sqrt(delta__)) / (2 * A__);
  double t2 = (-B__ + std::sqrt(delta__)) / (2 * A__);

  if ((t1 > 0 and t1 < follower_deceleration_duration) or (t2 > 0 and t2 < follower_deceleration_duration)) {
    double B = 2 * b0 * b1 - 4 * (a0 * c1 + a1 * c0);
    double C = b0 * b0 - 4 * a0 * c0;

    new_acceleration = -C / B;
  }

//  double new_acceleration_3 = 2. * (vehicle.gap() + vehicle.length() + position +
//                                    vehicle.speed() * follower_deceleration_duration -
//                                    future_leader_position) /
//                              (follower_deceleration_duration * follower_deceleration_duration);
//
//  new_acceleration = std::max(new_acceleration, new_acceleration_3);

  double follower_final_speed = vehicle.speed() - new_acceleration * follower_deceleration_duration;

  if (follower_final_speed < 0) {
    follower_final_speed = 0;
    follower_deceleration_duration = vehicle.speed() / new_acceleration;
  }

  // assertions
  {
    double future_follower_position = position + (vehicle.speed() - 0.5 * new_acceleration *
                                                                    follower_deceleration_duration) *
                                                 follower_deceleration_duration;
    double future_follower_position_to_final =
      future_follower_position + 0.5 * follower_final_speed * follower_final_speed /
                                 vehicle.max_acceleration();

    assert(follower_final_speed >= 0);
    assert(
      std::abs(future_leader_position - future_follower_position - vehicle.gap() - vehicle.length()) <
      1e-6 or future_leader_position - future_follower_position - vehicle.gap() - vehicle.length() > 0);
    assert(std::abs(future_leader_position_to_final - future_follower_position_to_final - vehicle.gap() -
                    vehicle.length()) < 1e-6 or
           future_leader_position_to_final - future_follower_position_to_final - vehicle.gap() -
           vehicle.length() > 0);
  }

  vehicle.update_acceleration(new_acceleration);
  update(t, t + follower_deceleration_duration, VehicleData::State::IN_DECELERATION, false, follower_final_speed);
}

}