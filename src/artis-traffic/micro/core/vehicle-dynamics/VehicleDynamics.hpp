/**
 * @file artis-traffic/micro/core/vehicle-dynamics/VehicleDynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_VEHICLE_DYNAMICS_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_VEHICLE_DYNAMICS_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Disturbance.hpp>
#include <artis-traffic/micro/core/VehicleData.hpp>

namespace artis::traffic::micro::core {

class VehicleState {
public:
  VehicleState(const VehicleData::State::values &state, bool ready_to_exit, const artis::traffic::core::Time &begin,
               double initial_speed, double final_speed) : _state(state), _ready_to_exit(ready_to_exit), _begin(begin),
                                                           _initial_speed(initial_speed), _final_speed(final_speed) {}

  const artis::traffic::core::Time &begin() const { return _begin; }

  double final_speed() const { return _final_speed; }

  double initial_speed() const { return _initial_speed; }

  bool operator!=(const VehicleState &s) const {
    return _state != s._state or _ready_to_exit != s._ready_to_exit or _begin != s._begin or
           _initial_speed != s._initial_speed or _final_speed != s._final_speed;
  }

  bool ready_to_exit() const { return _ready_to_exit; }

  const VehicleData::State::values &state() const { return _state; }

  void update_final_speed(double final_speed) { _final_speed = final_speed; }

  std::string to_string() const {
    return "<" + VehicleData::State::to_string(_state) + " / " + std::to_string(_ready_to_exit) + " / "
           + std::to_string(_begin) + " / " + std::to_string(_initial_speed) + " / " + std::to_string(_final_speed)
           + ">";
  }

  virtual ~VehicleState() = default;

private:
  VehicleData::State::values _state; // RUNNING, IN_ACCELERATION, IN_DECELERATION, STOPPED, RESTART, HALT
  bool _ready_to_exit;
  artis::traffic::core::Time _begin;
  double _initial_speed;
  double _final_speed;
};

template<class Vehicle>
class VehicleDynamics {
public:
  VehicleDynamics(const Vehicle &vehicle, const artis::traffic::core::Time &next_time, double link_length,
                  double speed_limit, bool is_first) : _vehicle(vehicle),
                                                       _next_time(next_time),
                                                       _link_length(link_length),
                                                       _speed_limit(speed_limit),
                                                       _is_first(is_first),
                                                       _disturbance({nullptr, 0}) {}

  bool apply_next_disturbance(const artis::traffic::core::Time &t, VehicleDynamics *follower) {
    bool apply_disturbance = false;

    if (_disturbance.disturbance) {
      ++_disturbance.index;
      if (_disturbance.index < 3) {

        process_disturbance(t, *_disturbance.disturbance, _disturbance.index, follower);
        apply_disturbance = true;
      } else {
        _disturbance.disturbance = nullptr;
      }
    }
    return apply_disturbance;
  }

  bool vehicleArrivedToLinkEnd(double position) const { return std::abs(position - link_length()) < 1e-6; }

  bool canRun(const traffic::core::Time &t, double final_position, const VehicleDynamics * /* leader */,
              bool go, const traffic::core::Time &next_closing_time, double position) const {
    return (is_first()) and go and
           (next_closing_time == -1 or
            next_closing_time > t + (final_position - position) / vehicle().speed());
//    return (is_first() or
//            (leader and leader->vehicle().data().state == VehicleData::State::RUNNING and
//             //             vehicle().speed() == vehicle().max_speed() and
//             leader->is_first())) and go and
//           (next_closing_time == -1 or
//            next_closing_time > t + (final_position - position) / vehicle().speed());
  }

  virtual double compute_position(const artis::traffic::core::Time &t) const = 0;

  virtual double compute_speed(const artis::traffic::core::Time &t) const = 0;

  virtual double compute_security_distance() const = 0;

  virtual bool critical_position(const artis::traffic::core::Time &t, double final_position) const = 0;

  virtual void init(const artis::traffic::core::Time &t, double final_position, const VehicleDynamics *leader) = 0;

  bool is_first() const { return _is_first; }

  void first() { _is_first = true; }

  virtual const VehicleData::State::values &last_state() const = 0;

  double link_length() const { return _link_length; }

  virtual void next_state(const artis::traffic::core::Time &t, double final_position, VehicleDynamics *follower,
                          const VehicleDynamics *leader,
                          bool go, const artis::traffic::core::Time &next_opening_time,
                          const artis::traffic::core::Time &next_closing_time) = 0;

  const artis::traffic::core::Time &next_time() const { return _next_time; }

  virtual void propagate_disturbance(const artis::traffic::core::Time & /* t */) {}

  virtual void process_disturbance(const artis::traffic::core::Time &t, const Disturbance &disturbance, size_t index,
                                   VehicleDynamics *follower) = 0;

  double speed_limit() const { return _speed_limit; }

  virtual void stop(const artis::traffic::core::Time &t, const VehicleDynamics *leader, bool immediate) = 0;

  const Vehicle &vehicle() const { return _vehicle; }

  Vehicle &vehicle() { return _vehicle; }

  virtual void update(const artis::traffic::core::Time &t, const artis::traffic::core::Time &next_time,
                      const VehicleData::State::values &new_state, bool ready_to_exit, double final_speed) = 0;

  void update_next_time(const artis::traffic::core::Time &next_time) { _next_time = next_time; }

  void update_vehicle_acceleration(double acceleration) { _vehicle.update_acceleration(acceleration); }

  void update_vehicle_max_speed() { _vehicle.update_max_speed(_speed_limit); }

  void update_vehicle_speed(double final_speed) { _vehicle.update_speed(final_speed); }

  void update_vehicle_state(const VehicleData::State::values &new_state) { _vehicle.data().state = new_state; }

  virtual ~VehicleDynamics() = default;

private:
  Vehicle _vehicle;
  artis::traffic::core::Time _next_time;
  double _link_length;
  double _speed_limit;
  bool _is_first;
  ActiveDisturbance _disturbance;
};

}

#endif