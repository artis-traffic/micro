/**
 * @file artis-traffic/micro/core/vehicle-dynamics/SimpleVehicleDynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_SIMPLE_VEHICLE_DYNAMICS_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_SIMPLE_VEHICLE_DYNAMICS_HPP

#include <artis-traffic/micro/core/vehicle-dynamics/VehicleDynamics.hpp>
#include <artis-traffic/micro/core/VehicleData.hpp>

namespace artis::traffic::micro::core {

class SimpleVehicleState : public VehicleState {
public:
  SimpleVehicleState(const VehicleData::State::values &state, bool ready_to_exit,
                     const artis::traffic::core::Time &begin,
                     double initial_speed, double final_speed) : VehicleState(state, ready_to_exit, begin,
                                                                              initial_speed, final_speed) {}

  ~SimpleVehicleState() override = default;
};

template <class Vehicle>
class SimpleVehicleDynamics : public VehicleDynamics<Vehicle> {
public:
  SimpleVehicleDynamics(const Vehicle &vehicle, const artis::traffic::core::Time &next_time, double link_length,
                        double speed_limit, bool is_first) : VehicleDynamics<Vehicle>(vehicle, next_time, link_length,
                                                                             speed_limit, is_first) {}

  double compute_position(const artis::traffic::core::Time &t) const override;

  double compute_security_distance() const override;

  void init(const artis::traffic::core::Time &t, double final_position, const VehicleDynamics<Vehicle> *leader) override;

  const VehicleData::State::values &last_state() const override { return _states.back().state(); }

  void next_state(const artis::traffic::core::Time &t, double final_position, VehicleDynamics<Vehicle> *follower,
                  const VehicleDynamics<Vehicle> *leader,
                  bool go, const artis::traffic::core::Time &next_opening_time,
                  const artis::traffic::core::Time &next_closing_time) override;

  void process_disturbance(const artis::traffic::core::Time &t, const Disturbance &disturbance, size_t index,
                           VehicleDynamics<Vehicle> *follower) override;

  const std::deque<SimpleVehicleState> &states() const { return _states; }

  void update(const artis::traffic::core::Time &t, const artis::traffic::core::Time &next_time,
              const VehicleData::State::values &new_state, bool ready_to_exit, double final_speed) override;

  virtual ~SimpleVehicleDynamics() = default;

private:
  std::deque<SimpleVehicleState> _states;
};

}

#include <artis-traffic/micro/core/vehicle-dynamics/SimpleVehicleDynamics.tpp>

#endif