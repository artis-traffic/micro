/**
 * @file artis-traffic/micro/core/vehicle-dynamics/CellVehicleDynamics.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CellVehicleDynamics.hpp"

#include <cmath>
#include <iostream>

namespace artis::traffic::micro::core {

template<class Vehicle>
double CellVehicleDynamics<Vehicle>::compute_position(const artis::traffic::core::Time &t) const {
  auto it = _states.crbegin();
  const auto &current_state = *it;
  unsigned int previous_cell_index = _states.size() == 1 ? 0 : (it + 1)->cell_index();
  double duration = t - current_state.begin();
  double position = previous_cell_index * link_length() / _cell_number +
                    duration * current_state.initial_speed() +
                    duration * duration *
                    (current_state.state() == VehicleData::State::IN_ACCELERATION ? 0.5 * current_state.acceleration()
                                                                                  : current_state.state() ==
                                                                                    VehicleData::State::IN_DECELERATION
                                                                                    ? -0.5 *
                                                                                      current_state.acceleration() : 0);

  return position > link_length() ? link_length() : position;
}

template<class Vehicle>
double CellVehicleDynamics<Vehicle>::compute_security_distance() const {
  return 0.5 * vehicle().speed() * vehicle().speed() / vehicle().acceleration();
}

template<class Vehicle>
void CellVehicleDynamics<Vehicle>::init(const artis::traffic::core::Time &t, double final_position,
                                        const VehicleDynamics<Vehicle> *leader) {

  std::cout << t << ": vehicle = " << vehicle().index() << std::endl;

  update_vehicle_max_speed();
  process(t, leader, final_position, false);
}

template<class Vehicle>
void
CellVehicleDynamics<Vehicle>::next_state(const artis::traffic::core::Time &t, double final_position,
                                         VehicleDynamics<Vehicle> *follower,
                                         const VehicleDynamics<Vehicle> *leader,
                                         bool go, const artis::traffic::core::Time & /* next_opening_time */,
                                         const artis::traffic::core::Time &next_closing_time) {
  double position = compute_position(t);
  bool apply_disturbance = apply_next_disturbance(t, follower);
  double distance_to_final_position = final_position - position;

//  std::cout << t << ": vehicle = " << vehicle().index() << " ==> position = " << position << " ; final position = "
//            << final_position << " ; distance to final = " << distance_to_final_position << " ; security distance = "
//            << compute_security_distance() << " ; speed = " << vehicle().speed() << " ; acceleration = "
//            << vehicle().acceleration() << " ; leader position = " << (leader ? leader->compute_position(t) : -1)
//            << " ; leader distance = " << (leader ? std::to_string(leader->compute_position(t) - position) : "+++++")
//            << " ; leader ID = " << (leader ? leader->vehicle().index() : -1) << " ; go = " << go << std::endl;

  if (not apply_disturbance) {
    const auto &current_state = _states.back();

    switch (current_state.state()) {
      case VehicleData::State::IN_ACCELERATION:
      case VehicleData::State::IN_DECELERATION:
      case VehicleData::State::RUNNING: {
        if (vehicleArrivedToLinkEnd(position) and go) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, vehicle().speed());
        } else if (canRun(t, final_position, leader, go, next_closing_time, position)) {
          process(t, leader, distance_to_final_position, go);
        } else {
          if (std::abs(distance_to_final_position) < 1e-6 or distance_to_final_position < 0) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          } else {
            process(t, leader, distance_to_final_position, go);
          }
        }
        break;
      }
      case VehicleData::State::RESTART: {
        if (go) {
          if (vehicleArrivedToLinkEnd(position)) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, speed_limit());
          } else {
            process(t, leader, distance_to_final_position, go);
          }
          if (follower and follower->last_state() == VehicleData::State::STOPPED) {
            follower->update(t, t + follower->vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
          }
        } else {
          if (vehicleArrivedToLinkEnd(position)) {
            process(t, leader, distance_to_final_position, go);
          } else {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          }
        }
        break;
      }
      case VehicleData::State::HALT:
      case VehicleData::State::STOPPED:
      case VehicleData::State::TRANSITION_STOPPED: {
        assert(false);
      }
    }
  }
}

template<class Vehicle>
void CellVehicleDynamics<Vehicle>::process(const traffic::core::Time &t, const VehicleDynamics<Vehicle> *leader,
                                           double distance_to_final_position, bool go) {
  unsigned int actual_cell_index = _states.empty() ? 0 : _states.back().cell_index();
  double speed = 0;

  if (leader) {
    unsigned int leader_index = dynamic_cast<const CellVehicleDynamics *>(leader)->states().back().cell_index();
    unsigned int leader_distance = leader_index - actual_cell_index - std::ceil(vehicle().length() + vehicle().gap());

    if (std::ceil(compute_security_distance()) >= leader_distance) {
      if (std::ceil(compute_security_distance()) > leader_distance) {
        speed = std::floor(std::sqrt(2 * vehicle().acceleration() * std::ceil(leader_distance)));
      } else {
        speed = vehicle().speed() - vehicle().acceleration();
      }
      speed = speed >= 0 ? speed : 0;
    } else if (vehicle().speed() < speed_limit()) {
      speed = vehicle().speed() + vehicle().acceleration();
    } else {
      speed = speed_limit();
    }
  } else {
    if (std::ceil(compute_security_distance()) >= distance_to_final_position) {
      if (not go) {
        if (std::ceil(compute_security_distance()) > distance_to_final_position) {
          speed = std::floor(std::sqrt(2 * vehicle().acceleration() * std::ceil(compute_security_distance())));
        } else {
          speed = vehicle().speed() - vehicle().acceleration();
        }
        speed = speed >= 0 ? speed : 0;
      } else {
        speed = vehicle().speed();
      }
    } else if (vehicle().speed() < speed_limit()) {
      speed = vehicle().speed() + vehicle().acceleration();
    } else {
      speed = speed_limit();
    }
  }

//  std::cout << " ----> " << speed << std::endl;

  if (speed == vehicle().speed()) {
    update(t, t + 1, VehicleData::State::RUNNING, false, speed);
  } else if (speed > vehicle().speed()) {
    update(t, t + 1, VehicleData::State::IN_ACCELERATION, false, speed);
  } else {
    update(t, t + 1, VehicleData::State::IN_DECELERATION, false, speed);
  }

  unsigned int distance = floor(speed);

  if (distance > 0) {
    unsigned int next_cell_index = actual_cell_index + distance;

    _states.back().cell_index(next_cell_index);
  }
}

template<class Vehicle>
void CellVehicleDynamics<Vehicle>::process_disturbance(const artis::traffic::core::Time & /* t */,
                                                       const Disturbance & /* disturbance */, size_t /* index */,
                                                       VehicleDynamics<Vehicle> * /* follower */) {
}

template<class Vehicle>
void
CellVehicleDynamics<Vehicle>::update(const artis::traffic::core::Time &t, const artis::traffic::core::Time &next_time,
                                     const VehicleData::State::values &new_state,
                                     bool ready_to_exit, double final_speed) {
  artis::traffic::core::Time adjusted_next_time = std::abs(t - next_time) < 1e-6 ? t : next_time;
  unsigned int actual_cell_index = _states.empty() ? 0 : _states.back().cell_index();
  CellVehicleState next_state(new_state, ready_to_exit, t, vehicle().speed(), final_speed, 0, vehicle().acceleration());

  _states.push_back(next_state);
  update_vehicle_state(new_state);
  update_next_time(adjusted_next_time);
  update_vehicle_speed(final_speed);
  _states.back().cell_index(actual_cell_index);
}

}