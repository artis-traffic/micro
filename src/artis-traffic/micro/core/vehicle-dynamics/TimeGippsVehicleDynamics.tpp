/**
 * @file artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TimeGippsVehicleDynamics.hpp"

#include <cmath>

namespace artis::traffic::micro::core {


template<class Vehicle>
const double TimeGippsVehicleDynamics<Vehicle>::delta_t = 0.1;

template<class Vehicle>
double TimeGippsVehicleDynamics<Vehicle>::compute_position(const artis::traffic::core::Time &t) const {
  const std::deque<TimeGippsVehicleState> &vehicle_states = states();
  double position = 0;

  for (size_t k = 0; k < vehicle_states.size(); ++k) {
    const TimeGippsVehicleState &vehicle_state = vehicle_states[k];
    artis::traffic::core::Time next_time = k == vehicle_states.size() - 1 ? t : vehicle_states[k + 1].begin();
    artis::traffic::core::Time duration = next_time - vehicle_state.begin();

    if (vehicle_state.state() == VehicleData::State::RUNNING) {
      position += vehicle_state.initial_speed() * duration;
    } else if (vehicle_state.state() == VehicleData::State::IN_DECELERATION) {
      position += (vehicle_state.initial_speed() - 0.5 * vehicle_state.acceleration()
                                                   * duration) * duration;
    } else if (vehicle_state.state() == VehicleData::State::IN_ACCELERATION) {
      position += (vehicle_state.initial_speed() + 0.5 * vehicle_state.acceleration()
                                                   * duration) * duration;
    }
  }

  position = std::min(position, this->link_length());
  return position;
}

template<class Vehicle>
double TimeGippsVehicleDynamics<Vehicle>::compute_speed(const artis::traffic::core::Time &t) const {
  const auto &last_state = _states.back();

  if (last_state.state() == VehicleData::State::IN_ACCELERATION) {
    return last_state.initial_speed() + (t - last_state.begin()) * this->vehicle().acceleration();
  } else if (last_state.state() == VehicleData::State::IN_DECELERATION) {
    return last_state.initial_speed() - (t - last_state.begin()) * this->vehicle().acceleration();
  } else {
    return last_state.initial_speed();
  }
}

template<class Vehicle>
double TimeGippsVehicleDynamics<Vehicle>::compute_security_distance() const {
  return 0.5 * this->vehicle().speed() * this->vehicle().speed() / this->vehicle().acceleration();
}

template<class Vehicle>
void TimeGippsVehicleDynamics<Vehicle>::init(const artis::traffic::core::Time &t, double final_position,
                                             const VehicleDynamics<Vehicle> *leader) {

//  std::cout << t << ": vehicle = " << vehicle().index() << std::endl;

  this->update_vehicle_max_speed();
  process(t, leader, final_position, false);
}

template<class Vehicle>
bool TimeGippsVehicleDynamics<Vehicle>::critical_position(const artis::traffic::core::Time &t,
                                                          double final_position) const {
  // simple
//  double distance = link_length() - compute_position(t);

  // kinematic
  double position = compute_position(t);
  double future_position = compute_position(t + delta_t);
  artis::traffic::core::Time max_duration = compute_speed(t) / this->vehicle().max_acceleration();
  double max_deceleration_distance =
    (compute_speed(t) - 0.5 * this->vehicle().max_acceleration() * max_duration) * max_duration;
  double distance_to_final_position = final_position - position;
  double distance_to_final_position_future = final_position - future_position;
  double distance = distance_to_final_position - max_deceleration_distance;
  double distance_future = distance_to_final_position_future - max_deceleration_distance;

  return std::abs(distance) < 1e-6 or distance_future < 1e-6 or
         std::abs(compute_position(t) - this->link_length()) < 1e-6;
}

template<class Vehicle>
void
TimeGippsVehicleDynamics<Vehicle>::next_state(const artis::traffic::core::Time &t, double final_position,
                                              VehicleDynamics<Vehicle> *follower,
                                              const VehicleDynamics<Vehicle> *leader,
                                              bool go, const artis::traffic::core::Time & /* next_opening_time */,
                                              const artis::traffic::core::Time &next_closing_time) {
  double position = compute_position(t);
  bool apply_disturbance = this->apply_next_disturbance(t, follower);
  double distance_to_final_position = final_position - position;

//  std::cout << t << ": vehicle = " << vehicle().index() << " ==> position = " << position << " ; final position = "
//            << final_position << " ; distance to final = " << distance_to_final_position << " ; security distance = "
//            << compute_security_distance() << " ; speed = " << vehicle().speed() << " ; acceleration = "
//            << vehicle().acceleration() << " ; leader position = " << (leader ? leader->compute_position(t) : -1)
//            << " ; leader distance = " << (leader ? std::to_string(leader->compute_position(t) - position) : "+++++")
//            << " ; leader ID = " << (leader ? leader->vehicle().index() : -1) << " ; go = " << go << std::endl;

  if (not apply_disturbance) {
    const auto &current_state = _states.back();

    switch (current_state.state()) {
      case VehicleData::State::IN_ACCELERATION:
      case VehicleData::State::IN_DECELERATION:
      case VehicleData::State::RUNNING: {
        if (this->vehicleArrivedToLinkEnd(position) and go) {
          update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, this->vehicle().speed());
        } else if (this->canRun(t, final_position, leader, go, next_closing_time, position)) {
          process(t, leader, distance_to_final_position, go);
        } else {
          if (std::abs(distance_to_final_position) < 1e-6 or distance_to_final_position < 0) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          } else {
            process(t, leader, distance_to_final_position, go);
          }
        }
        break;
      }
      case VehicleData::State::RESTART: {
        if (go) {
          if (this->vehicleArrivedToLinkEnd(position)) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::RUNNING, true, this->vehicle().speed());
          } else {
            process(t, leader, distance_to_final_position, go);
          }
          if (follower and follower->last_state() == VehicleData::State::STOPPED) {
            follower->update(t, t + follower->vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
          }
        } else {
          if (this->vehicleArrivedToLinkEnd(position)) {
            update(t, artis::common::DoubleTime::infinity, VehicleData::State::STOPPED, false, 0);
          } else {
            process(t, leader, distance_to_final_position, go);
          }
        }
        if (follower and follower->last_state() == VehicleData::State::STOPPED) {
          follower->update(t, t + follower->vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
        }
        break;
      }
      case VehicleData::State::HALT:
      case VehicleData::State::STOPPED:
      case VehicleData::State::TRANSITION_STOPPED: {
        assert(false);
      }
    }
  }
}

template<class Vehicle>
void TimeGippsVehicleDynamics<Vehicle>::process(const traffic::core::Time &t, const VehicleDynamics<Vehicle> *leader,
                                                double /* distance_to_final_position */, bool go) {
  double speed = process_speed(t, leader, go);

  if (speed == this->vehicle().speed()) {
    update(t, t + delta_t, VehicleData::State::RUNNING, false, speed);
  } else if (speed > this->vehicle().speed()) {
    update(t, t + delta_t, VehicleData::State::IN_ACCELERATION, false, speed);
  } else {
    update(t, t + delta_t, VehicleData::State::IN_DECELERATION, false, speed);
  }
}

template<class Vehicle>
double
TimeGippsVehicleDynamics<Vehicle>::process_speed(const traffic::core::Time &t, const VehicleDynamics<Vehicle> *leader,
                                                 bool go) {
  if (leader) {
    double leader_speed = leader->vehicle().speed();
    double leader_position = leader->compute_position(t);

    // simplified gipps model
    double vn_e1 = std::min(this->vehicle().speed() + this->vehicle().acceleration() * delta_t,
                            this->vehicle().max_speed());

//    double a = -1. / (2 * this->vehicle().acceleration());
//    double b = -2 * this->vehicle().reaction_time();
//    double c = leader_position - (this->vehicle().length() + this->vehicle().gap()) - compute_position(t) +
//               0.5 * leader_speed * leader_speed / leader->vehicle().acceleration() -
//               this->vehicle().acceleration() * this->vehicle().reaction_time() * this->vehicle().reaction_time();
//    double delta = b * b - 4 * a * c;
//    double v1 = -b - sqrt(delta) / (2 * a);
//    double v2 = -b + sqrt(delta) / (2 * a);
    double v3 = sqrt(
      2 * this->vehicle().acceleration() *
      (leader_position - (this->vehicle().length() + this->vehicle().gap()) - compute_position(t) +
       0.5 * leader_speed * leader_speed / leader->vehicle().acceleration()));

    double vn_e3 = sqrt(2 * this->vehicle().acceleration() *
                        (this->link_length() - compute_position(t) - this->vehicle().speed() * delta_t +
                         0.5 * this->vehicle().acceleration() * pow(delta_t, 2)));

    return std::min(std::min(vn_e1, v3), vn_e3);

//    std::cout << t << ": [" << vehicle().index() << "] => " << vn_e1 << " " << vn_e2 << " " << vn_e3 << " " << v1
//              << " " << v3 << " => " << speed << std::endl;

  } else {
    double vn_e1 = std::min(this->vehicle().speed() + this->vehicle().acceleration() * delta_t,
                            this->vehicle().max_speed());
    double vn_e3 = sqrt(2 * this->vehicle().acceleration() *
                        (this->link_length() - compute_position(t) - this->vehicle().speed() * delta_t +
                         0.5 * this->vehicle().acceleration() * pow(delta_t, 2)));
    if (not go) {
      return std::min(vn_e1, vn_e3);
    } else {
      return vn_e1;
    }

//    std::cout << t << ": [" << vehicle().index() << "] => " << vn_e1 << " xxxxxxxx " << vn_e3 << " xxxxxxxx " << " => "
//              << speed << std::endl;

  }

}

template<class Vehicle>
void TimeGippsVehicleDynamics<Vehicle>::process_disturbance(const artis::traffic::core::Time & /* t */,
                                                            const Disturbance & /* disturbance */, size_t /* index */,
                                                            VehicleDynamics<Vehicle> * /* follower */) {
}

template<class Vehicle>
void TimeGippsVehicleDynamics<Vehicle>::update(const artis::traffic::core::Time &t,
                                               const artis::traffic::core::Time &next_time,
                                               const VehicleData::State::values &new_state,
                                               bool ready_to_exit, double final_speed) {
  artis::traffic::core::Time adjusted_next_time = std::abs(t - next_time) < 1e-6 ? t : next_time;
  TimeGippsVehicleState next_state(new_state, ready_to_exit, t, this->vehicle().speed(), final_speed,
                                   this->vehicle().acceleration());

  _states.push_back(next_state);
  this->update_vehicle_state(new_state);
  this->update_next_time(adjusted_next_time);
  this->update_vehicle_speed(final_speed);
}

template<class Vehicle>
void TimeGippsVehicleDynamics<Vehicle>::stop(const artis::traffic::core::Time &t,
                                             const VehicleDynamics<Vehicle> *follower,
                                             bool immediate) {
  auto current_state = _states.rbegin();
  double new_final_speed = follower->compute_speed(immediate ? t : t + this->vehicle().reaction_time());

  if (immediate or this->next_time() > t + this->vehicle().reaction_time()) {
    current_state->update_final_speed(new_final_speed);
    this->update_next_time(immediate ? t : t + this->vehicle().reaction_time());
    this->update_vehicle_speed(new_final_speed);
  }
}

}
