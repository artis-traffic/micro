/**
 * @file artis-traffic/micro/core/KinematicVehicleDynamics.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_DISTURBANCE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_DISTURBANCE_HPP

#include <artis-traffic/core/Base.hpp>

#include <memory>

namespace artis::traffic::micro::core {

struct DisturbanceTypes {
  enum values {
    ACCELERATION, DECELERATION
  };
};

struct Disturbance {
  artis::traffic::core::Time time;
  DisturbanceTypes::values type;
  double durations[3];
  unsigned int vehicle_index;

  bool operator==(const Disturbance &other) const {
    return time == other.time and type == other.type and durations[0] == other.durations[0] and
           durations[1] == other.durations[1] and durations[2] == other.durations[2] and
           vehicle_index == other.vehicle_index;
  }

  std::string to_string() const {
    return "disturbance<" + std::to_string(time) + " ; " + std::to_string(type) +
           " ; [" + std::to_string(durations[0]) + ", " + std::to_string(durations[1]) + ", " +
           std::to_string(durations[2]) + "] ;" + std::to_string(vehicle_index) + ">";
  }
};

struct DisturbanceParameters {
  std::vector<Disturbance> disturbances;
};

struct ActiveDisturbance {
  std::shared_ptr<Disturbance> disturbance;
  size_t index;
};

}

#endif
