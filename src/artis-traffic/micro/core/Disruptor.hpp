/**
 * @file artis-traffic/micro/core/Disruptor.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_DISRUPTOR_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_DISRUPTOR_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/micro/core/Disturbance.hpp>

namespace artis::traffic::micro::core {

class Disruptor
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Disruptor, DisturbanceParameters> {
public:
  struct outputs {
    enum values {
      OUT
    };
  };

  Disruptor(const std::string &name,
            const artis::pdevs::Context<artis::common::DoubleTime, Disruptor, DisturbanceParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Disruptor, DisturbanceParameters>(name, context),
    _disturbances(context.parameters().disturbances), _index(0) {
    output_ports({{outputs::OUT, "out"}});
  }

  ~Disruptor() override = default;

  void dint(const artis::traffic::core::Time & /* t */) override {
    ++_index;
  }

  void start(const artis::traffic::core::Time & /* t */) override {
    _index = 0;
  }

  artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
    if (_index < _disturbances.size()) {
      return _disturbances.at(_index).time - t;
    } else {
      return artis::common::DoubleTime::infinity;
    }
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override {
    artis::traffic::core::Bag bag;

    if (_index < _disturbances.size()) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, _disturbances.at(_index)));
    }
    return bag;
  }

private:
  std::vector<Disturbance> _disturbances;
  size_t _index;
};

}

#endif //ARTIS_TRAFFIC_CORE_DISRUPTOR_HPP
