/**
 * @file artis-traffic/micro/core/Roundabout.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_ROUNDABOUT_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_ROUNDABOUT_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>

namespace artis::traffic::micro::core {

struct RoundaboutParameters {
  double occupied_duration;
  int capacity;
  unsigned int in_number;
  unsigned int out_number;
};

template<class Vehicle>
struct roundabout_vehicles_data {
  std::deque<Vehicle> vehicles;
  std::deque<double> sigmas;
  int vehicle_number;
};

template<class Vehicle>
class Roundabout
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Roundabout<Vehicle>, RoundaboutParameters> {
public:
  struct inputs {
    enum values {
      IN, OPEN = 1000, CLOSE = 1500, STATE = 2000
    };
  };

  struct outputs {
    enum values {
      OUT, OPEN = 1000, CLOSE = 1500, STATE = 2000
    };
  };

  struct vars {
    enum values {
      OPEN, CLOSE, PHASE, VEHICLE_NUMBER
    };
  };

  Roundabout(const std::string &name,
             const artis::pdevs::Context<artis::common::DoubleTime, Roundabout<Vehicle>, RoundaboutParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Roundabout<Vehicle>, RoundaboutParameters>(
      name, context),
    _occupied_duration(context.parameters().occupied_duration),
    _capacity(context.parameters().capacity),
    _in_number(context.parameters().in_number),
    _out_number(context.parameters().out_number) {
    for (unsigned int i = 0; i < _in_number; ++i) {
      std::stringstream ss_in;
      ss_in << "in_" << (i + 1);
      this->input_port({inputs::IN + i, ss_in.str()});
    }
    for (unsigned int i = 0; i < _out_number; ++i) {
      std::stringstream ss_out;
      ss_out << "out_" << (i + 1);
      this->output_port({outputs::OUT + i, ss_out.str()});
    }

    for (unsigned int i = 0; i < _out_number; ++i) {
      std::stringstream ss_open;
      ss_open << "open_" << (i + 1);
      this->input_port({inputs::OPEN + i, ss_open.str()});
      std::stringstream ss_close;
      ss_close << "close_" << (i + 1);
      this->input_port({inputs::CLOSE + i, ss_close.str()});
    }

    this->input_port({inputs::STATE, "state"});

    for (unsigned int i = 0; i < _in_number; ++i) {
      std::stringstream ss_close;
      ss_close << "close_" << (i + 1);
      this->output_port({outputs::CLOSE + i, ss_close.str()});
      std::stringstream ss_open;
      ss_open << "open_" << (i + 1);
      this->output_port({outputs::OPEN + i, ss_open.str()});
    }

    this->output_port({outputs::STATE, "state"});

    this->observables({{vars::OPEN,           "open"},
                       {vars::CLOSE,          "close"},
                       {vars::PHASE,          "phase"},
                       {vars::VEHICLE_NUMBER, "vehicle_number"}});
  }

  ~Roundabout() override = default;

  void dconf(const artis::traffic::core::Time & /* t* */, const artis::traffic::core::Time & /* e */,
             const artis::traffic::core::Bag & /* bag */) override;

  void dint(const artis::traffic::core::Time & /* t */) override;

  void dext(const artis::traffic::core::Time & /* t */, const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t, unsigned int index) const override;

private:
  struct Phase {
    enum values {
      INIT,
      OPEN,
      CLOSE,
      SEND_OPEN,
      SEND_CLOSE,
      SEND_VEHICLE,
      SEND_OPEN_AFTER_ARRIVED,
      SEND_STATE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case OPEN:
          return "OPEN";
        case CLOSE:
          return "CLOSE";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
        case SEND_VEHICLE:
          return "SEND_VEHICLE";
        case SEND_OPEN_AFTER_ARRIVED:
          return "SEND_OPEN_AFTER_ARRIVED";
        case SEND_STATE:
          return "SEND_STATE";
      }
      return "";
    }
  };

  // parameters
  double _occupied_duration;
  int _capacity;
  unsigned int _in_number;
  unsigned int _out_number;

  // state
  typename Phase::values _phase;
  artis::traffic::core::Time _sigma;
  bool _arrived;
  roundabout_vehicles_data<Vehicle> _roundabout_vehicles_data;
  bool _next_close;
  unsigned int _out_index;
  int _path_send_index;

  typename Phase::values _stored_phase;
  artis::traffic::core::Time _stored_sigma;
  close_data _next_close_data;
  std::deque<close_data> _nexts_close_data;
  std::deque<bool> _nexts_close;
  std::deque<open_data> _nexts_open_data;
  artis::traffic::core::Time _last_time;
};

}

#include <artis-traffic/micro/core/Roundabout.tpp>

#endif