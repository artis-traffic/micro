/**
 * @file artis-traffic/micro/core/stop-state-machine/ProcessVehicleStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_PROCESS_VEHICLE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_PROCESS_VEHICLE_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class ProcessVehicleStateMachine
  : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  ProcessVehicleStateMachine(const typename Types::root_state_machine_type &root, const Parameters & /* parameters */) :
    artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root,
                                                                                    {typename Types::vehicle_type(),
                                                                                     false, 0}) {}

  virtual ~ProcessVehicleStateMachine() = default;

  void build(const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine);

// types
  typedef typename ProcessVehicleStateMachine<Types, Parameters, StateType>::template State<ProcessVehicleStateMachine<Types, Parameters, StateType>> State_t;
  typedef typename ProcessVehicleStateMachine<Types, Parameters, StateType>::template Transition<ProcessVehicleStateMachine<Types, Parameters, StateType>> Transition_t;

private:
// parameters
};

template<class Types, class Parameters, class StateType>
struct StartingCloseState : ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t {
  StartingCloseState(const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::STARTING_CLOSE, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override { return 0; }
};

template<class Types, class Parameters, class StateType>
struct CloseWithoutVehicleState : ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t {
  CloseWithoutVehicleState(const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::CLOSE_WITHOUT_VEHICLE, machine) {}
};

template<class Types, class Parameters, class StateType>
struct OpenStopState : ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t {
  OpenStopState(const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::OPEN, machine) {}
};

template<class Types, class Parameters, class StateType>
struct CloseWithVehicleState : ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t {
  CloseWithVehicleState(const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::CLOSE_WITH_VEHICLE, machine) {}
};

template<class Types, class Parameters, class StateType>
struct ProcessVehicleTransitionStartClose : ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t {
  ProcessVehicleTransitionStartClose(
    const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool no_event() const override { return true; }

  typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};

    std::vector<int> v = {-1};
    events.externals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::CLOSE, v});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct ProcessVehicleTransitionOpenStop : ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t {
  ProcessVehicleTransitionOpenStop(
    const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) override {
    this->_machine->state_()._waiting_vehicle_number++;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override { return event == StopEventIDs::ARRIVED; }

  typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};

    std::vector<int> v = {-1};
    events.externals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::OPEN, v});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct ProcessVehicleTransitionGetVehicle : ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t {
  typename Types::vehicle_type vehicle;

  ProcessVehicleTransitionGetVehicle(
    const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    value(vehicle);
    this->_machine->state_()._vehicle = vehicle;
    this->_machine->state_()._arrived = true;
    this->_machine->state_()._waiting_vehicle_number--;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override { return event == StopEventIDs::IN; }

  typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};
    std::vector<int> v = {-1};

    events.internals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::InternalEvent{{StopEventIDs::NEW_VEHICLE, {}},
                                                                                       Types::state_machine_IDs_type::STATE_GO});
    events.externals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::CLOSE, v});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct ProcessVehicleTransitionArrivedWithVehicle
  : ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t {
  ProcessVehicleTransitionArrivedWithVehicle(
    const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) override {
    this->_machine->state_()._waiting_vehicle_number++;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override { return event == StopEventIDs::ARRIVED; }
};

template<class Types, class Parameters, class StateType>
struct ProcessVehicleTransitionSendVehicleNoWaiting
  : ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t {
  typename Types::vehicle_type vehicle;

  ProcessVehicleTransitionSendVehicleNoWaiting(
    const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & t, const artis::common::event::Value & /* value */) override {
    vehicle = this->_machine->state_()._vehicle;
    this->_machine->state_()._vehicle = typename Types::vehicle_type();
    this->_machine->state_()._arrived = false;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override { return event == StopEventIDs::OK; }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
    return this->_machine->state_()._waiting_vehicle_number == 0;
  }

  typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};

    events.externals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::SEND_VEHICLE,
                                                                                       vehicle});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct ProcessVehicleTransitionSendVehicleWaiting
  : ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t {
  typename Types::vehicle_type vehicle;

  ProcessVehicleTransitionSendVehicleWaiting(
    const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
    ProcessVehicleStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & t, const artis::common::event::Value & /* value */) override {
    vehicle = this->_machine->state_()._vehicle;
    this->_machine->state_()._vehicle = typename Types::vehicle_type();
    this->_machine->state_()._arrived = false;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override { return event == StopEventIDs::OK; }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
    return this->_machine->state_()._waiting_vehicle_number > 0;
  }

  typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & t) const override {
    typename ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};

    std::vector<int> v = {-1};
    events.externals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::SEND_VEHICLE,
                                                                                       vehicle});
    events.externals.push_back(
      typename ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::OPEN, v});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
void ProcessVehicleStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new StartingCloseState(machine));
  this->state(new CloseWithoutVehicleState(machine));
  this->state(new OpenStopState(machine));
  this->state(new CloseWithVehicleState(machine));
  this->initial_state(StopStateIDs::STARTING_CLOSE);
  this->transition(StopStateIDs::STARTING_CLOSE, StopStateIDs::CLOSE_WITHOUT_VEHICLE,
                   new ProcessVehicleTransitionStartClose(machine));
  this->transition(StopStateIDs::CLOSE_WITHOUT_VEHICLE, StopStateIDs::OPEN,
                   new ProcessVehicleTransitionOpenStop(machine));
  this->transition(StopStateIDs::OPEN, StopStateIDs::CLOSE_WITH_VEHICLE,
                   new ProcessVehicleTransitionGetVehicle(machine));
  this->transition(StopStateIDs::CLOSE_WITH_VEHICLE, StopStateIDs::CLOSE_WITH_VEHICLE,
                   new ProcessVehicleTransitionArrivedWithVehicle(machine));
  this->transition(StopStateIDs::CLOSE_WITH_VEHICLE, StopStateIDs::CLOSE_WITHOUT_VEHICLE,
                   new ProcessVehicleTransitionSendVehicleNoWaiting(machine));
  this->transition(StopStateIDs::CLOSE_WITH_VEHICLE, StopStateIDs::OPEN,
                   new ProcessVehicleTransitionSendVehicleWaiting(machine));
}

}

#endif
