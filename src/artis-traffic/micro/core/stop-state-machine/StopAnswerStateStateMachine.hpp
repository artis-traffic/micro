/**
 * @file artis-traffic/micro/core/stop-state-machine/StopAnswerStateStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_ANSWER_STATE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_ANSWER_STATE_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStates.hpp>

#include <artis-traffic/micro/core/stop-state-machine/StopStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class StopAnswerStateStateMachine
  : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  StopAnswerStateStateMachine(const typename Types::root_state_machine_type &root, const Parameters &parameters) :
    artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
    _concurrent_stop_number(parameters.concurrent_stop_number) {}

  virtual ~StopAnswerStateStateMachine() = default;

  void build(const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine);

  unsigned int get_concurrent_stop_number() const { return _concurrent_stop_number; }

// types
  typedef typename StopAnswerStateStateMachine<Types, Parameters, StateType>::template State<StopAnswerStateStateMachine<Types, Parameters, StateType>> State_t;
  typedef typename StopAnswerStateStateMachine<Types, Parameters, StateType>::template Transition<StopAnswerStateStateMachine<Types, Parameters, StateType>> Transition_t;

private:
// parameters
  unsigned int _concurrent_stop_number;
};

template<class Types, class Parameters, class StateType>
struct StopWaitStateDataState : StopAnswerStateStateMachine<Types, Parameters, StateType>::State_t {
  StopWaitStateDataState(const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine) :
    StopAnswerStateStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAIT_STATE_DATA, machine) {}
};

template<class Types, class Parameters, class StateType>
struct StopSendStateDataState : StopAnswerStateStateMachine<Types, Parameters, StateType>::State_t {
  StopSendStateDataState(const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine)
    : StopAnswerStateStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::SEND_STATE_DATA, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override { return 0; }
};

template<class Types, class Parameters, class StateType>
struct StopClearStateDataState : StopAnswerStateStateMachine<Types, Parameters, StateType>::State_t {
  StopClearStateDataState(const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine)
    : StopAnswerStateStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::CLEAR_STATE_DATA, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override { return 0; }
};

template<class Types, class Parameters, class StateType>
struct StopAnswerStateTransitionGetState
  : StopAnswerStateStateMachine<Types, Parameters, StateType>::Transition_t {
  StopAnswerStateTransitionGetState(
    const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine) :
    StopAnswerStateStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    std::vector<unsigned int> indexes{};
    value(indexes);
    this->_machine->state_()._concurrents_answer_index = indexes;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::GET_STATE;
  }
};

template<class Types, class Parameters, class StateType>
struct StopAnswerStateTransitionSendStateData
  : StopAnswerStateStateMachine<Types, Parameters, StateType>::Transition_t {
  StopAnswerStateTransitionSendStateData(
    const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine)
    :
    StopAnswerStateStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time &/*t*/, const artis::common::event::Value & /* value */) override {
    const auto &state = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

    if (not state._arrived) {
      this->_machine->state_()._state_data = {typename Types::vehicle_type(), -1, -1, 0};
    } else {
      typename Types::vehicle_type vehicle = state._vehicle;

      this->_machine->state_()._state_data = {vehicle, 0, 0, 0};
    }
  }

  bool no_event() const override { return true; }

  typename StopAnswerStateStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopAnswerStateStateMachine<Types, Parameters, StateType>::Events events{};

    events.externals.push_back(
      typename StopAnswerStateStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::ANSWER_STATE_DATA,
                                                                                        this->_machine->state_()._state_data});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct StopAnswerStateTransitionClearStateData
  : StopAnswerStateStateMachine<Types, Parameters, StateType>::Transition_t {
  StopAnswerStateTransitionClearStateData(
    const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine) :
    StopAnswerStateStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time &/*t*/, const artis::common::event::Value & /* value */) override {
    this->_machine->state_()._concurrents_answer_index.clear();
  }

  bool no_event() const override { return true; }
};

template<class Types, class Parameters, class StateType>
void StopAnswerStateStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<StopAnswerStateStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new StopWaitStateDataState(machine));
  this->state(new StopSendStateDataState(machine));
  this->state(new StopClearStateDataState(machine));
  this->initial_state(StopStateIDs::WAIT_STATE_DATA);
  this->transition(StopStateIDs::WAIT_STATE_DATA, StopStateIDs::SEND_STATE_DATA,
                   new StopAnswerStateTransitionGetState(machine));
  this->transition(StopStateIDs::SEND_STATE_DATA, StopStateIDs::CLEAR_STATE_DATA,
                   new StopAnswerStateTransitionSendStateData(machine));
  this->transition(StopStateIDs::CLEAR_STATE_DATA, StopStateIDs::WAIT_STATE_DATA,
                   new StopAnswerStateTransitionClearStateData(machine));
  this->transition(StopStateIDs::CLEAR_STATE_DATA, StopStateIDs::SEND_STATE_DATA,
                   new StopAnswerStateTransitionGetState(machine));
}

}

#endif
