/**
 * @file artis-traffic/micro/core/stop-state-machine/StopRootStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_STOP_ROOT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_STOP_ROOT_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/Data.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStates.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStateMachine.hpp>

#include <artis-traffic/micro/core/stop-state-machine/ProcessVehicleStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopAnswerStateStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopDownOpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopDownOpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStateGoStateMachine.hpp>

#include <artis-star-addons/utils/RootStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters>
using StopStateMachineTypes = std::tuple<
  StopAnswerStateStateMachine<Types, Parameters, StopAnswerStateState<Types>>,
  StopDownOpenCloseStateMachine<Types, Parameters, StopDownOpenCloseState>,
  ProcessVehicleStateMachine<Types, Parameters, ProcessVehicleState<Types>>,
  StopStateGoStateMachine<Types, Parameters, StopStateGoState<Types>>>;

DEFINE_ROOT_STATE_MACHINE(StopRootStateMachine, StopStateMachineTypes)

}

#endif //ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_STOP_ROOT_STATE_MACHINE_HPP