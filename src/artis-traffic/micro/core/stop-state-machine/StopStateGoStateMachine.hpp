/**
 * @file artis-traffic/micro/core/stop-state-machine/StopStateGoStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_STATE_GO_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_STATE_GO_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStates.hpp>

#include <artis-traffic/micro/core/stop-state-machine/StopStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType = typename Types::root_state_machine_type::StopGoState>
class StopStateGoStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  StopStateGoStateMachine(const typename Types::root_state_machine_type &root, const Parameters &parameters)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root,
                                                                                      {std::vector<state_data<typename Types::vehicle_type>>(
                                                                                        parameters.concurrent_link_number +
                                                                                        parameters.concurrent_stop_number),
                                                                                       0, true}),
      _exits_links_speeds(parameters.exits_links_speeds),
      _concurrent_link_number(parameters.concurrent_link_number),
      _concurrent_stop_number(parameters.concurrent_stop_number),
      _priorities(parameters.priorities) {}

  ~StopStateGoStateMachine() override = default;

  void build(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine);

  std::vector<double> get_exits_links_speeds() const { return _exits_links_speeds; }

  unsigned int get_concurrent_link_number() const { return _concurrent_link_number; }

  unsigned int get_concurrent_stop_number() const { return _concurrent_stop_number; }

  std::vector<bool> get_priorities() const { return _priorities; }

// types
  typedef typename StopStateGoStateMachine<Types, Parameters, StateType>::template State<StopStateGoStateMachine<Types, Parameters, StateType>> State_t;
  typedef typename StopStateGoStateMachine<Types, Parameters, StateType>::template Transition<StopStateGoStateMachine<Types, Parameters, StateType>> Transition_t;

private:
  // parameters
  std::vector<double> _exits_links_speeds;
  unsigned int _concurrent_link_number;
  unsigned int _concurrent_stop_number;
  std::vector<bool> _priorities;
};

template<class Types, class Parameters, class StateType>
struct WaitingStopStateGoState : StopStateGoStateMachine<Types, Parameters, StateType>::State_t {
  WaitingStopStateGoState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAITING, machine) {}
};

template<class Types, class Parameters, class StateType>
struct WaitStopStopStateGoState : StopStateGoStateMachine<Types, Parameters, StateType>::State_t {
  WaitStopStopStateGoState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAIT_STOP, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override { return 5; }
};

template<class Types, class Parameters, class StateType>
struct NextCloseStopStateGoState : StopStateGoStateMachine<Types, Parameters, StateType>::State_t {
  NextCloseStopStateGoState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::NEXT_CLOSE, machine) {}
};

template<class Types, class Parameters, class StateType>
struct WaitStateStopStateGoState : StopStateGoStateMachine<Types, Parameters, StateType>::State_t {
  WaitStateStopStateGoState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAIT_STATE, machine) {}
};

template<class Types, class Parameters, class StateType>
struct CheckGoStopStateGoState : StopStateGoStateMachine<Types, Parameters, StateType>::State_t {
  CheckGoStopStateGoState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::CHECK_GO, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override { return 0; }
};

template<class Types, class Parameters, class StateType>
struct WaitOpenStopStateGoState : StopStateGoStateMachine<Types, Parameters, StateType>::State_t {
  WaitOpenStopStateGoState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAIT_OPEN, machine) {}
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNewVehicle : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionNewVehicle(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine)
    :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::NEW_VEHICLE;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNextClose : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionNextClose(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine)
    :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool no_event() const override { return true; }

  bool guard(const artis::traffic::core::Time & /*t*/, const artis::common::event::Value & /* value */) const override {
    const auto &state = this->_machine->root().template state_<StopDownOpenCloseState>(Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
    const auto &state2 = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

    return not(state._node_open and state._open.at(state2._vehicle.current_index()))
           and this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number() > 0;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionWaitState : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionWaitState(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine)
    :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool no_event() const override { return true; }

  bool guard(const artis::traffic::core::Time & /*t*/, const artis::common::event::Value & /* value */) const override {
    const auto &state = this->_machine->root().template state_<StopDownOpenCloseState>(Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
    const auto &state2 = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

    return (state._node_open and state._open.at(state2._vehicle.current_index())) and
           this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number() > 0;
  }

  typename StopStateGoStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopStateGoStateMachine<Types, Parameters, StateType>::Events events{};

    events.externals.push_back(
      typename StopStateGoStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::STATE, {}});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionGetStateData : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionGetStateData(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    state_data<typename Types::vehicle_type> data{};

    value(data);
    this->_machine->state_()._state_datas[data.index] = data;
    this->_machine->state_()._states_received++;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::GET_STATE_DATA;
  }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
    return this->_machine->state_()._states_received < (this->_machine->get_concurrent_link_number() +
                                                        this->_machine->get_concurrent_stop_number()) - 1;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionGetStateDataAll : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {

  StopStateGoTransitionGetStateDataAll(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
    state_data<typename Types::vehicle_type> data{};

    value(data);
    this->_machine->state_()._state_datas[data.index] = data;
    this->_machine->state_()._states_received = 0;
    this->_machine->state_()._go = true;

    double longer_duration = 0;
//      double stop_distance_to_max = 0.5 * this->_machine->root().process_vehicle_state()._vehicle.max_acceleration() *
//                                    ((this->_machine->get_exits_links_speeds()[this->_machine->root().process_vehicle_state()._vehicle.current_index()] /
//                                      this->_machine->root().process_vehicle_state()._vehicle.max_acceleration()) *
//                                     (this->_machine->get_exits_links_speeds()[this->_machine->root().process_vehicle_state()._vehicle.current_index()] /
//                                      this->_machine->root().process_vehicle_state()._vehicle.max_acceleration()));
//    double stop_distance_to_max = (this->_machine->root().process_vehicle_state()._vehicle.length() +
//                                   this->_machine->root().process_vehicle_state()._vehicle.gap()) /
//                                  (this->_machine->get_exits_links_speeds()[this->_machine->root().process_vehicle_state()._vehicle.current_index()]);
    const auto &state = this->_machine->root().template state_<StopDownOpenCloseState>(Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
    const auto &state2 = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);
    double stop_duration_to_max =
      this->_machine->get_exits_links_speeds()[state2._vehicle.current_index()] / state2._vehicle.max_acceleration();

    for (unsigned int j = 0;
         j < this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number(); j++) {
      double duration = this->_machine->state_()._state_datas[j].duration;
      double distance = this->_machine->state_()._state_datas[j].distance;
      if (j < this->_machine->get_concurrent_link_number()) {
        longer_duration = longer_duration < this->_machine->state_()._state_datas[j].duration ?
                          this->_machine->state_()._state_datas[j].duration : longer_duration;
        if (not(((duration == -1 and distance == -1) or duration >= stop_duration_to_max))) {
          this->_machine->state_()._go = false;
        }
      } else {
        if (duration != -1 and not this->_machine->get_priorities()[j - this->_machine->get_concurrent_link_number()]) {
          this->_machine->state_()._go = false;
        }
      }
    }

    if (not(state._node_open and state._open.at(state2._vehicle.current_index()))) {
      this->_machine->state_()._go = false;
    }
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::GET_STATE_DATA;
  }

  bool guard(const artis::traffic::core::Time & /*t*/, const artis::common::event::Value & value) const override {
    return (this->_machine->state_()._states_received >= (this->_machine->get_concurrent_link_number() +
                                                         this->_machine->get_concurrent_stop_number()) - 1);
  }
};


template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionCanGo : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {

  StopStateGoTransitionCanGo(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) override {
  }

  bool no_event() const override { return true; }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
    return this->_machine->state_()._go;
  }

  typename StopStateGoStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopStateGoStateMachine<Types, Parameters, StateType>::Events events{};

    events.internals.push_back(
      typename StopStateGoStateMachine<Types, Parameters, StateType>::InternalEvent{{StopEventIDs::OK, {}},
                                                                                    Types::state_machine_IDs_type::PROCESS_VEHICLE});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNoConcurrentCanGo : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {

  StopStateGoTransitionNoConcurrentCanGo(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) override {
  }

  bool no_event() const override { return true; }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
    const auto &state = this->_machine->root().template state_<StopDownOpenCloseState>(Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
    const auto &state2 = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

    return (state._node_open and state._open.at(state2._vehicle.current_index())) and
           this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number() == 0;
  }

  typename StopStateGoStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopStateGoStateMachine<Types, Parameters, StateType>::Events events{};

    events.internals.push_back(
      typename StopStateGoStateMachine<Types, Parameters, StateType>::InternalEvent{{StopEventIDs::OK, {}},
                                                                                    Types::state_machine_IDs_type::PROCESS_VEHICLE});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNoConcurrentCantGo : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionNoConcurrentCantGo(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool no_event() const override { return true; }

  bool guard(const artis::traffic::core::Time & /*t*/, const artis::common::event::Value & /* value */) const override {
    const auto &state = this->_machine->root().template state_<StopDownOpenCloseState>(Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
    const auto &state2 = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

    return not(state._node_open and state._open.at(state2._vehicle.current_index()))
           and this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number() == 0;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNoGo : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {

  StopStateGoTransitionNoGo(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) override {
  }

  bool no_event() const override { return true; }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
      bool has_concurrent_stop = this->_machine->get_concurrent_stop_number() > 0;
      bool can_go_stop = true;
      if (has_concurrent_stop) {
          for (unsigned int j = this->_machine->get_concurrent_link_number();
               j < this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number(); j++) {
              double duration = this->_machine->state_()._state_datas[j].duration;
              if (duration != -1 and not this->_machine->get_priorities()[j - this->_machine->get_concurrent_link_number()]) {
                  can_go_stop = false;
              }
          }
      }
      return (not this->_machine->state_()._go) and can_go_stop;
  }
};

    template<class Types, class Parameters, class StateType>
    struct StopStateGoTransitionNoGoStopBlock : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {

        StopStateGoTransitionNoGoStopBlock(const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
                StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) override {
        }

        bool no_event() const override { return true; }

        bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
            bool has_concurrent_stop = this->_machine->get_concurrent_stop_number() > 0;
            bool can_go_stop = true;
            if (has_concurrent_stop) {
                for (unsigned int j = this->_machine->get_concurrent_link_number();
                     j < this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number(); j++) {
                    double duration = this->_machine->state_()._state_datas[j].duration;
                    if (duration != -1 and not this->_machine->get_priorities()[j - this->_machine->get_concurrent_link_number()]) {
                        can_go_stop = false;
                    }
                }
            }
            return (not this->_machine->state_()._go) and not can_go_stop;
        }
    };


template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionWaitOpenNextOpen : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionWaitOpenNextOpen(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::NEXT_OPEN;
  }

  typename StopStateGoStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopStateGoStateMachine<Types, Parameters, StateType>::Events events{};

    events.externals.push_back(
      typename StopStateGoStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::STATE, {}});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNextCloseNextOpen : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionNextCloseNextOpen(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::NEXT_OPEN;
  }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {
    return this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number() > 0;
  }

  typename StopStateGoStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopStateGoStateMachine<Types, Parameters, StateType>::Events events{};

    events.externals.push_back(
      typename StopStateGoStateMachine<Types, Parameters, StateType>::ExternalEvent{StopEventIDs::STATE, {}});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
struct StopStateGoTransitionNoConcurrentNextCloseNextOpen
  : StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t {
  StopStateGoTransitionNoConcurrentNextCloseNextOpen(
    const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) :
    StopStateGoStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::NEXT_OPEN;
  }

  bool guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const override {

    return this->_machine->get_concurrent_link_number() + this->_machine->get_concurrent_stop_number() == 0;
  }

  typename StopStateGoStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopStateGoStateMachine<Types, Parameters, StateType>::Events events{};

    events.internals.push_back(
      typename StopStateGoStateMachine<Types, Parameters, StateType>::InternalEvent{{StopEventIDs::OK, {}},
                                                                                    Types::state_machine_IDs_type::PROCESS_VEHICLE});
    return events;
  }
};

template<class Types, class Parameters, class StateType>
void
StopStateGoStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<StopStateGoStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitingStopStateGoState(machine));
  this->state(new WaitStopStopStateGoState(machine));
  this->state(new NextCloseStopStateGoState(machine));
  this->state(new WaitStateStopStateGoState(machine));
  this->state(new CheckGoStopStateGoState(machine));
  this->state(new WaitOpenStopStateGoState(machine));
  this->initial_state(StopStateIDs::WAITING);
  this->transition(StopStateIDs::WAITING, StopStateIDs::WAIT_STOP,
                   new StopStateGoTransitionNewVehicle(machine));
  this->transition(StopStateIDs::WAIT_STOP, StopStateIDs::NEXT_CLOSE,
                   new StopStateGoTransitionNextClose(machine));
  this->transition(StopStateIDs::WAIT_STOP, StopStateIDs::WAIT_STATE,
                   new StopStateGoTransitionWaitState(machine));
  this->transition(StopStateIDs::WAIT_STOP, StopStateIDs::WAITING,
                   new StopStateGoTransitionNoConcurrentCanGo(machine));
  this->transition(StopStateIDs::WAIT_STOP, StopStateIDs::NEXT_CLOSE,
                   new StopStateGoTransitionNoConcurrentCantGo(machine));
  this->transition(StopStateIDs::WAIT_STATE, StopStateIDs::WAIT_STATE,
                   new StopStateGoTransitionGetStateData(machine));
  this->transition(StopStateIDs::WAIT_STATE, StopStateIDs::CHECK_GO,
                   new StopStateGoTransitionGetStateDataAll(machine));
  this->transition(StopStateIDs::CHECK_GO, StopStateIDs::WAITING,
                   new StopStateGoTransitionCanGo(machine));
  this->transition(StopStateIDs::CHECK_GO, StopStateIDs::WAIT_OPEN,
                   new StopStateGoTransitionNoGo(machine));
  this->transition(StopStateIDs::CHECK_GO, StopStateIDs::WAIT_STOP,
                   new StopStateGoTransitionNoGoStopBlock(machine));
  this->transition(StopStateIDs::WAIT_OPEN, StopStateIDs::WAIT_STATE,
                   new StopStateGoTransitionWaitOpenNextOpen(machine));
  this->transition(StopStateIDs::NEXT_CLOSE, StopStateIDs::WAIT_STATE,
                   new StopStateGoTransitionNextCloseNextOpen(machine));
  this->transition(StopStateIDs::NEXT_CLOSE, StopStateIDs::WAITING,
                   new StopStateGoTransitionNoConcurrentNextCloseNextOpen(machine));
}

}

#endif