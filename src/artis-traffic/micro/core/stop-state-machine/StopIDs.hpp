/**
 * @file artis-traffic/micro/core/stop-state-machine/StopIDs.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_STOP_IDS_HPP
#define ARTIS_TRAFFIC_MICRO_STOP_IDS_HPP

namespace artis::traffic::micro::core {

struct StopStateMachineIDs {
  enum values {
    ANSWER_STATE, DOWN_OPEN_CLOSE, PROCESS_VEHICLE, STATE_GO, LAST = STATE_GO
  };
};

struct StopStateIDs {
  enum values {
    STARTING_CLOSE, CLOSE_WITHOUT_VEHICLE, OPEN, CLOSE_WITH_VEHICLE, WAIT_OPEN_CLOSE, WAITING, WAIT_STOP, NEXT_CLOSE,
    WAIT_STATE, CHECK_GO, WAIT_OPEN, WAIT_STATE_DATA, SEND_STATE_DATA, CLEAR_STATE_DATA, LAST = CLEAR_STATE_DATA
  };
};

struct StopEventIDs {
  enum values {
    ARRIVED, OPEN, CLOSE, IN, SEND_VEHICLE, OK, DOWN_OPEN, DOWN_CLOSE, NEW_VEHICLE, STATE, GET_STATE_DATA, NEXT_OPEN,
    ANSWER_STATE_DATA, GET_STATE, LAST = GET_STATE
  };
};

}

#endif //ARTIS_TRAFFIC_MICRO_STOP_IDS_HPP
