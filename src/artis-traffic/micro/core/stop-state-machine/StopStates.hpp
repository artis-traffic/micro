/**
 * @file artis-traffic/micro/core/stop-state-machine/StopStates.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_STOP_STATES_HPP
#define ARTIS_TRAFFIC_MICRO_STOP_STATES_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>

#include <map>
#include <vector>

namespace artis::traffic::micro::core {

template<class Types>
struct ProcessVehicleState {
  typename Types::vehicle_type _vehicle;
  bool _arrived;
  unsigned int _waiting_vehicle_number;
};

struct StopDownOpenCloseState {
  bool _node_open;
  artis::traffic::core::Time _node_opening_time;
  artis::traffic::core::Time _node_closing_time;
  std::map<uint, bool> _open;
  std::map<uint, artis::traffic::core::Time> _opening_times;
  std::map<uint, artis::traffic::core::Time> _closing_times;
};

template<class Types>
struct StopStateGoState {
  std::vector<state_data<typename Types::vehicle_type>> _state_datas;
  unsigned int _states_received;
  bool _go;
};

template<class Types>
struct StopAnswerStateState {
  std::vector<unsigned int> _concurrents_answer_index;
  state_data<typename Types::vehicle_type> _state_data;
};

}

#endif //ARTIS_TRAFFIC_MICRO_STOP_STATES_HPP
