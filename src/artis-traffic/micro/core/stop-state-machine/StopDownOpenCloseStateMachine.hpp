/**
 * @file artis-traffic/micro/core/stop-state-machine/StopDownOpenCloseStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_DOWN_OPEN_CLOSE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_STOP_STATE_MACHINE_DOWN_OPEN_CLOSE_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStates.hpp>

#include <artis-traffic/micro/core/stop-state-machine/StopStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class StopDownOpenCloseStateMachine
  : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  StopDownOpenCloseStateMachine(const typename Types::root_state_machine_type &root,
                                const Parameters & /* parameters */)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root,
                                                                                      {false, -1, -1, {}, {}, {}}) {}

  ~StopDownOpenCloseStateMachine() override = default;

  void build(const std::shared_ptr<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> &machine);

// types
  typedef typename StopDownOpenCloseStateMachine<Types, Parameters, StateType>::template State<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> State_t;
  typedef typename StopDownOpenCloseStateMachine<Types, Parameters, StateType>::template Transition<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> Transition_t;
};

template<class Types, class Parameters, class StateType>
struct WaitStopDownOpenCloseState : StopDownOpenCloseStateMachine<Types, Parameters, StateType>::State_t {
  WaitStopDownOpenCloseState(
    const std::shared_ptr<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> &machine) :
    StopDownOpenCloseStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAIT_OPEN_CLOSE, machine) {}
};

template<class Types, class Parameters, class StateType>
struct StopDownOpenCloseT1 : StopDownOpenCloseStateMachine<Types, Parameters, StateType>::Transition_t {
  StopDownOpenCloseT1(const std::shared_ptr<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> &machine) :
    StopDownOpenCloseStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /*t*/, const artis::common::event::Value &value) override {
    close_data data;

    value(data);
    if (data.index == -1) {
      this->_machine->state_()._node_open = false;
      this->_machine->state_()._node_opening_time = data.time;
      this->_machine->state_()._node_closing_time = -1;
    } else {
      this->_machine->state_()._open[data.index] = false;
      this->_machine->state_()._opening_times[data.index] = data.time;
      this->_machine->state_()._closing_times[data.index] = -1;
    }
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::DOWN_CLOSE;
  }
};

template<class Types, class Parameters, class StateType>
struct StopDownOpenCloseT2 : StopDownOpenCloseStateMachine<Types, Parameters, StateType>::Transition_t {
  open_data data;

  StopDownOpenCloseT2(const std::shared_ptr<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> &machine) :
    StopDownOpenCloseStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    value(data);
    if (data.index == -1) {
      this->_machine->state_()._node_open = true;
      this->_machine->state_()._node_closing_time = data.time;
      this->_machine->state_()._node_opening_time = -1;
    } else {
      this->_machine->state_()._open[data.index] = true;
      this->_machine->state_()._closing_times[data.index] = data.time;
      this->_machine->state_()._opening_times[data.index] = -1;
    }
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == StopEventIDs::DOWN_OPEN;
  }

  typename StopDownOpenCloseStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename StopDownOpenCloseStateMachine<Types, Parameters, StateType>::Events events{};
    const auto &state = this->_machine->root().template state_<ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

    if (data.index != -1) {
      std::vector<int> v = {data.index};

      events.externals.push_back(
        typename StopDownOpenCloseStateMachine<Types, Parameters, StateType>::ExternalEvent{{StopEventIDs::OPEN, v}});
      if (data.index == state._vehicle.current_index()) {
        events.internals.push_back(
          typename StopDownOpenCloseStateMachine<Types, Parameters, StateType>::InternalEvent{
            {StopEventIDs::NEXT_OPEN, {}},
            Types::state_machine_IDs_type::STATE_GO});
      }
    }
    return events;
  }
};

template<class Types, class Parameters, class StateType>
void StopDownOpenCloseStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<StopDownOpenCloseStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitStopDownOpenCloseState(machine));
  this->initial_state(StopStateIDs::WAIT_OPEN_CLOSE);
  this->transition(StopStateIDs::WAIT_OPEN_CLOSE, StopStateIDs::WAIT_OPEN_CLOSE,
                   new StopDownOpenCloseT1(machine));
  this->transition(StopStateIDs::WAIT_OPEN_CLOSE, StopStateIDs::WAIT_OPEN_CLOSE,
                   new StopDownOpenCloseT2(machine));
}

}

#endif