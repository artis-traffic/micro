/**
 * @file artis-traffic/micro/core/Collector.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_COLLECTOR_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_COLLECTOR_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include <artis-traffic/core/Base.hpp>
#include "Vehicle.hpp"

namespace artis::traffic::micro::core {

struct CollectorParameters {
  double occupied_duration;
};

class Collector
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Collector, CollectorParameters> {
public:
  struct inputs {
    enum values {
      IN_PRIMARY, IN_SECONDARY, OPEN, CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT, OPEN, CLOSE
    };
  };

  struct vars {
    enum values {
      OPEN, CLOSE, PHASE, VEHICLE_NUMBER
    };
  };

  Collector(const std::string &name,
            const artis::pdevs::Context<artis::common::DoubleTime,
              Collector,
              CollectorParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Collector, CollectorParameters>(
      name, context),
    _occupied_duration(context.parameters().occupied_duration) {
    input_ports({{inputs::IN_PRIMARY,   "in_primary"},
                 {inputs::IN_SECONDARY, "in_secondary"},
                 {inputs::CLOSE,        "close"},
                 {inputs::OPEN,         "open"}});
    output_ports({{outputs::OUT,   "out"},
                  {outputs::CLOSE, "close"},
                  {outputs::OPEN,  "open"}});
    observables({{vars::OPEN,           "open"},
                 {vars::CLOSE,          "close"},
                 {vars::PHASE,          "phase"},
                 {vars::VEHICLE_NUMBER, "vehicle_number"}});
  }

  ~Collector() override = default;

  void dconf(const artis::traffic::core::Time & /* t* */,
             const artis::traffic::core::Time & /* e */,
             const artis::traffic::core::Bag & /* bag */) override;

  void dint(const artis::traffic::core::Time & /* t */) override;

  void dext(const artis::traffic::core::Time & /* t */,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override;

private:
  struct Phase {
    enum values {
      INIT,
      OPEN,
      CLOSE,
      SEND,
      SEND_OPEN,
      SEND_CLOSE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case OPEN:
          return "OPEN";
        case CLOSE:
          return "CLOSE";
        case SEND:
          return "SEND";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
      }
      return "";
    }
  };

  // parameters
  double _occupied_duration;

  // state
  Phase::values _phase;
  artis::traffic::core::Time _sigma;
  bool _arrived;
  Vehicle _vehicle;
  bool _next_close;
};

}

#endif