/**
 * @file artis-traffic/micro/core/Stop.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_STOP_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_STOP_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopTypes.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopRootStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStateMachine.hpp>

#include <artis-star-addons/pdevs/StateMachineDynamics.hpp>

#include <artis-traffic/core/Base.hpp>

namespace artis::traffic::micro::core {

struct StopParameters {
  std::vector<double> exits_links_speeds;
  unsigned int concurrent_link_number;
  unsigned int concurrent_stop_number;
  std::vector<bool> priorities; // true if the stop has priority over concurrent stop i
};

template<class Model, class Parameters, class Types>
class StopImplementation
        : public artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, Model, Parameters,
                typename Types::root_state_machine_type> {
public:
  struct inputs {
    enum values {
      IN, OPEN, CLOSE, STATE, STATE_DATA = 1000, ARRIVED = 2000, LAST = ARRIVED + 1
    };
  };

  struct outputs {
    enum values {
      OUT, OPEN, CLOSE, STATE, STATE_DATA = 1000, LAST = STATE_DATA + 1000
    };
  };

  struct vars {
    enum values {
      VEHICLE_NUMBER, VEHICLE_INDEX, LAST = VEHICLE_INDEX
    };
  };

  StopImplementation(const std::string &name,
       const artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters> &context) :
    artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, Model, Parameters,
      typename Types::root_state_machine_type>(name, context),
    _exits_links_speeds(context.parameters().exits_links_speeds),
    _concurrent_link_number(context.parameters().concurrent_link_number),
    _concurrent_stop_number(context.parameters().concurrent_stop_number),
    _priorities(context.parameters().priorities) {
    // input ports
    for (unsigned int i = 0; i < _concurrent_stop_number; i++) {
      std::stringstream ss_in_state;
      ss_in_state << "state_" << (i + 1);
      this->input_port({inputs::STATE + i, ss_in_state.str()});
    }
    for (unsigned int i = 0; i < _concurrent_link_number + _concurrent_stop_number; i++) {
      std::stringstream ss_in_state;
      ss_in_state << "state_data_" << (i + 1);
      this->input_port({inputs::STATE_DATA + i, ss_in_state.str()});
    }
    // output ports
    for (unsigned int i = 0; i < _concurrent_link_number + _concurrent_stop_number; i++) {
      std::stringstream ss_out_state;
      ss_out_state << "state_" << (i + 1);
      this->output_port({outputs::STATE + i, ss_out_state.str()});
    }
    for (unsigned int i = 0; i < _concurrent_stop_number; i++) {
      std::stringstream ss_out_state;
      ss_out_state << "state_data_" << (i + 1);
      this->output_port({outputs::STATE_DATA + i, ss_out_state.str()});
    }

    this->input_ports({{inputs::IN,      "in"},
                       {inputs::CLOSE,   "close"},
                       {inputs::OPEN,    "open"},
                       {inputs::ARRIVED, "arrived"}});
    this->output_ports({{outputs::OUT,   "out"},
                        {outputs::CLOSE, "close"},
                        {outputs::OPEN,  "open"}});

    this->observables({{vars::VEHICLE_NUMBER, "vehicle_number"},
                       {vars::VEHICLE_INDEX,  "vehicle_index"}});
  }

  virtual ~StopImplementation() = default;

  void dext(const artis::traffic::core::Time & /* t */,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t, unsigned int index) const override;

private:
// parameter
  std::vector<double> _exits_links_speeds;
  unsigned int _concurrent_link_number;
  unsigned int _concurrent_stop_number;
  std::vector<bool> _priorities;
};

template<class Types, class Parameters>
    class Stop : public StopImplementation<Stop<Types, Parameters>, Parameters, Types> {
    public:
        Stop(const std::string &name,
             const artis::pdevs::Context<artis::common::DoubleTime, Stop<Types, Parameters>, StopParameters> &context)
                : StopImplementation<Stop<Types, Parameters>, StopParameters, Types>(name, context) {
        }

        ~Stop() override = default;
    };

}

#include <artis-traffic/micro/core/Stop.tpp>

#endif