/**
 * @file artis-traffic/micro/core/Roundabout.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/Roundabout.hpp>
#include <artis-traffic/micro/core/Data.hpp>

namespace artis::traffic::micro::core {

template<class Vehicle>
void Roundabout<Vehicle>::dconf(const artis::traffic::core::Time &t,
                                const artis::traffic::core::Time &e,
                                const artis::traffic::core::Bag &bag) {

//  std::cout << t << " [" << get_full_name() << "] =====> dconf [BEFORE]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

  // when receive CLOSE and phase = SEND_OPEN_AFTER_ARRIVED
  dint(t);
  dext(t, e, bag);

//  std::cout << t << " [" << get_full_name() << "] =====> dconf [AFTER]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

}

template<class Vehicle>
void Roundabout<Vehicle>::dint(const artis::traffic::core::Time &t) {

//  std::cout << t << " [" << get_full_name() << "] =====> dint [BEFORE]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

  switch (_phase) {
    case Phase::INIT: {
      _phase = Phase::SEND_OPEN;
      break;
    }
    case Phase::SEND_VEHICLE: {
      _roundabout_vehicles_data.vehicles.pop_front();
      _roundabout_vehicles_data.sigmas.pop_front();
      _roundabout_vehicles_data.vehicle_number--;
      if (_roundabout_vehicles_data.vehicle_number == _capacity - 1) {
        _phase = Phase::SEND_OPEN;
        _sigma = 0;
      } else {
        _phase = Phase::OPEN;
        if (_roundabout_vehicles_data.vehicle_number > 0) {
          _sigma = _roundabout_vehicles_data.sigmas.front();
        } else {
          _sigma = artis::common::DoubleTime::infinity;
        }
      }
      break;
    }
    case Phase::OPEN: {
      _phase = Phase::SEND_VEHICLE;
      _sigma = 0;
      break;
    }
    case Phase::CLOSE: {
      _phase = Phase::SEND_VEHICLE;
      _sigma = 0;
      break;
    }
    case Phase::SEND_OPEN_AFTER_ARRIVED: {
      _phase = Phase::OPEN;
      break;
    }
    case Phase::SEND_OPEN: {
      _phase = Phase::OPEN;
      _sigma = artis::common::DoubleTime::infinity;
      break;
    }
    case Phase::SEND_CLOSE: {
      _phase = Phase::CLOSE;
      if (_roundabout_vehicles_data.vehicle_number == 1) {
        _sigma = _roundabout_vehicles_data.sigmas.front();
      }
      break;
    }
    case Phase::SEND_STATE: {
      _phase = _stored_phase;
      _sigma = _stored_sigma;
      break;
    }
  }
  _last_time = t;

//  std::cout << t << " [" << get_full_name() << "] =====> dint [AFTER]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

}

template<class Vehicle>
void Roundabout<Vehicle>::dext(const artis::traffic::core::Time &t,
                               const artis::traffic::core::Time &e,
                               const artis::traffic::core::Bag &bag) {

//  std::cout << t << " [" << get_full_name() << "] =====> dext [BEFORE]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

  std::for_each(bag.begin(), bag.end(),
                [t, e, this](const artis::traffic::core::ExternalEvent &event) {
                  for (unsigned int i = 0; i < _in_number; i++) {
                    if (event.on_port(inputs::IN + i)) {
                      Vehicle vehicle{};

                      assert(_phase == Phase::OPEN and _roundabout_vehicles_data.vehicle_number < _capacity);

                      event.data()(vehicle);

//                    std::cout << "[" << get_full_name() << "] at " << t << " => " << vehicle.index() << std::endl;

                      _roundabout_vehicles_data.vehicles.push_back(vehicle);
                      _roundabout_vehicles_data.sigmas.push_back((vehicle.current_index() + 1) * _occupied_duration);
                      _roundabout_vehicles_data.vehicle_number++;
                      _arrived = true;
                      if (_roundabout_vehicles_data.vehicle_number == _capacity) {
                        _phase = Phase::SEND_CLOSE;
                        _sigma = 0;
                      } else {
                        if (_roundabout_vehicles_data.vehicle_number == 1) {
                          _sigma = _roundabout_vehicles_data.sigmas.front();
                        } else {
                          _sigma -= e;
                        }
                      }
                      _out_index = _roundabout_vehicles_data.vehicles.back().current_index();
                    }
                    if (event.on_port(inputs::CLOSE + i)) {
                      _phase = Phase::SEND_CLOSE;

                      _nexts_close[i] = true;
                      event.data()(_nexts_close_data[i]);
                      _path_send_index = i;
                    }
                    if (event.on_port(inputs::OPEN + i)) {
                      if (_roundabout_vehicles_data.vehicle_number < _capacity) {
                        _phase = Phase::SEND_OPEN;
                      }
                      _nexts_close[i] = false;
                      _nexts_close_data[i] = {close_type::SECURITY, -1, 0};
                      _path_send_index = i;
                    }

                  }
                });

  std::for_each(bag.begin(), bag.end(),
                [e, bag, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::STATE)) {
                    if (bag.size() > 1) {
                      _stored_phase = _phase;
                      _stored_sigma = _sigma;
                      _phase = Phase::SEND_STATE;
                    } else {
                      _stored_phase = _phase;
                      _stored_sigma = _sigma - e;
                      _phase = Phase::SEND_STATE;
                    }
                  }
                });
  _last_time = t;

//  std::cout << t << " [" << get_full_name() << "] =====> dext [AFTER]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

//  assert((_arrived and not _next_close) or (not _arrived and _next_close)
//             or (not _next_close and not _arrived));
}

template<class Vehicle>
void Roundabout<Vehicle>::start(const artis::traffic::core::Time &t) {
  _arrived = false;
  _roundabout_vehicles_data = {{}, {}, 0};
  _next_close = false;
  _sigma = 0;
  _phase = Phase::INIT;
  _next_close_data = {close_type::SECURITY, -1, 0};
  _last_time = t;
  for (unsigned int i = 0; i < _out_number; i++) {
    _nexts_close_data.push_back({close_type::SECURITY, -1, 0});
    _nexts_close.push_back(false);
    _nexts_open_data.push_back({-1, -1});
  }
  _path_send_index = 0;
}

template<class Vehicle>
artis::traffic::core::Time Roundabout<Vehicle>::ta(const artis::traffic::core::Time & /* t */) const {
  switch (_phase) {
    case Phase::OPEN:
    case Phase::CLOSE:
      return _sigma;
    case Phase::INIT:
    case Phase::SEND_OPEN_AFTER_ARRIVED:
    case Phase::SEND_OPEN:
    case Phase::SEND_CLOSE:
    case Phase::SEND_VEHICLE:
    case Phase::SEND_STATE:
      return 0;
  }
  return artis::common::DoubleTime::infinity;
}

template<class Vehicle>
artis::traffic::core::Bag Roundabout<Vehicle>::lambda(const artis::traffic::core::Time &t) const {
  artis::traffic::core::Bag bag;

//  std::cout << t << " [" << get_full_name() << "] =====> lambda: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << std::endl;

  if (_phase == Phase::SEND_VEHICLE) {
    Vehicle vehicle = _roundabout_vehicles_data.vehicles.front();
    vehicle.forward();
    if (_out_number == 1) {
      bag.push_back(
        artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
    } else {
      bag.push_back(
        artis::traffic::core::ExternalEvent(outputs::OUT + _out_index, vehicle));
    }
  } else if (_phase == Phase::SEND_CLOSE) {
    if (_roundabout_vehicles_data.vehicle_number == _capacity) {

//      std::cout << t << " => CLOSE OCCUPIED -> " << t + _occupied_duration << std::endl;
      for (unsigned int i = 0; i < _out_number; i++) {
        bag.push_back(
          artis::traffic::core::ExternalEvent(
            outputs::CLOSE + i,
            close_data{close_type::OCCUPIED, t + _roundabout_vehicles_data.sigmas.front()}));
      }
    } else {

      if (_nexts_close_data[_path_send_index].type == close_type::CAPACITY and
          _nexts_close_data[_path_send_index].time != -1) {

//        std::cout << t << " => CLOSE CAPACITY -> " << _next_close_data.time << std::endl;
        if (_out_number == 1) {
          bag.push_back(
            artis::traffic::core::ExternalEvent(
              outputs::CLOSE, close_data{close_type::CAPACITY, _next_close_data.time, 0}));
        } else {
          bag.push_back(
            artis::traffic::core::ExternalEvent(
              outputs::CLOSE + _path_send_index,
              close_data{close_type::CAPACITY, _nexts_close_data[_path_send_index].time, 0}));
        }
      }
    }
  } else if (_phase == Phase::SEND_OPEN or _phase == Phase::SEND_OPEN_AFTER_ARRIVED) {

//    std::cout << t << " => *** OPEN -> " << t + _fixed_duration_sigma << std::endl;

    if (_out_number == 1) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN, open_data{-1, -1}));
    } else {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN + _path_send_index, open_data{-1, -1}));
    }
  } else if (_phase == Phase::SEND_STATE) {
    if (_arrived) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE, (double) 0.0));
    } else {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE, (double) -1.0));
    }
  }
  return bag;
}

template<class Vehicle>
artis::common::event::Value Roundabout<Vehicle>::observe(const artis::traffic::core::Time & /* t */,
                                                         unsigned int index) const {
  switch (index) {
    case vars::OPEN:
      return (bool) (_phase == Phase::OPEN);
    case vars::CLOSE:
      return (bool) (_phase == Phase::CLOSE);
    case vars::PHASE:
      return Phase::to_string(_phase);
    case vars::VEHICLE_NUMBER:
      return (unsigned int) (_roundabout_vehicles_data.vehicle_number);
    default:
      return {};
  }
}

}
