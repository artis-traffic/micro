/**
 * @file artis-traffic/micro/core/Inserter.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Inserter.hpp"

namespace artis::traffic::micro::core {

void Inserter::dconf(const artis::traffic::core::Time &t,
                     const artis::traffic::core::Time &e,
                     const artis::traffic::core::Bag &bag) {
  dint(t);
  dext(t, e, bag);
}

void Inserter::dint(const artis::traffic::core::Time & /*t */) {
  switch (_phase) {
    case Phase::INIT: {
      _sigma = artis::common::DoubleTime::infinity;
      break;
    }
    case Phase::OPEN:

      assert(false);

      break;
    case Phase::CLOSE: {

      assert(false);

      _phase = Phase::SEND_OPEN;
      _sigma = 0;
      break;
    }
    case Phase::SEND_OPEN: {
      if (_arrived) {
        _phase = Phase::SEND_STATE;
        _sigma = 0;
      } else {
        _phase = Phase::OPEN;
        _sigma = artis::common::DoubleTime::infinity;
      }
      break;
    }
    case Phase::SEND_CLOSE: {
      if (_arrived) {
        if (not _next_close) {
          _phase = Phase::SEND_STATE;
          _sigma = 0;
        } else {
          _phase = Phase::WAIT;
          _sigma = 5;
        }
      } else {
        if (_delay) {
          _phase = Phase::WAIT_DELAY;
          _sigma = 5;
        } else {
          _phase = Phase::CLOSE;
          _sigma = artis::common::DoubleTime::infinity;
        }
      }
      break;
    }
    case Phase::SEND_STATE: {
      _phase = Phase::WAIT_STATE;
      _sigma = artis::common::DoubleTime::infinity;
      break;
    }
    case Phase::SEND_VEHICLE: {
      _arrived = false;
      if (_next_close) {
        _phase = Phase::CLOSE;
        _sigma = artis::common::DoubleTime::infinity;
      } else {
        _phase = Phase::WAIT_DELAY;
        _sigma = 5;
        _delay = true;
      }
      break;
    }
    case Phase::WAIT_STATE: {

      assert(false);

      break;
    }
    case Phase::WAIT_DELAY: {
      _phase = Phase::SEND_OPEN;
      _sigma = 0;
      _delay = false;
      break;
    }
    case Phase::WAIT: {
      if (not _next_close) {
        _phase = Phase::SEND_STATE;
        _sigma = 0;
      } else {
        _phase = Phase::WAIT;
        _sigma = 5;
      }
      break;
    }
  }
}

void Inserter::dext(const artis::traffic::core::Time &t,
                    const artis::traffic::core::Time & /* e */,
                    const artis::traffic::core::Bag &bag) {
  double distance = -1;

  std::for_each(bag.begin(), bag.end(),
                [this, t, &distance](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {

                    assert(_phase == Phase::OPEN and not _arrived);

                    event.data()(_vehicle);
                    _arrived = true;
                    _phase = Phase::SEND_CLOSE;
                    _sigma = 0;
                  } else if (event.on_port(inputs::CLOSE)) {

                    assert(not _next_close);

                    _next_close = true;
                    _phase = Phase::SEND_CLOSE;
                    _sigma = 0;
                  } else if (event.on_port(inputs::OPEN)) {

                    assert(_next_close);

                    _next_close = false;
                    if (_phase == Phase::CLOSE or _phase == Phase::INIT) {
                      _phase = Phase::SEND_OPEN;
                      _sigma = 0;
                    } else if (_phase == Phase::WAIT) {
                      _phase = Phase::SEND_STATE;
                      _sigma = 5;
                    } else if (_phase == Phase::WAIT_DELAY) {
                      _sigma = 5;
                    } else {
                      _sigma = artis::common::DoubleTime::infinity;
                    }
                  } else if (event.on_port(inputs::STATE)) {
                    if (not _next_close) {
                      double new_distance;

                      event.data()(new_distance);
                      if ((distance == -1 and new_distance >= 0)
                          or (distance != -1 and new_distance < distance and new_distance != -1)) {
                        distance = new_distance;
                      }
                      if (distance >= 0) {
                        if (_arrived and distance > _threshold) {
                          _phase = Phase::SEND_VEHICLE;
                          _sigma = 0;
                        } else {
                          _phase = Phase::WAIT;
                          // TODO
                          _sigma = 5;
                        }
                      } else { // no vehicle
                        if (_arrived) {
                          _phase = Phase::SEND_VEHICLE;
                          _sigma = 0;
                        }
                      }
                    } else {
                      _phase = Phase::WAIT_STATE;
                      // TODO
                      _sigma = 5;
                    }
                  }
                });
}

void Inserter::start(const artis::traffic::core::Time & /* t */) {
  _arrived = false;
  _next_close = true;
  _delay = false;
  _sigma = 0;
  _phase = Phase::INIT;
}

artis::traffic::core::Time Inserter::ta(const artis::traffic::core::Time & /* t */) const {
  return _sigma;
}

artis::traffic::core::Bag Inserter::lambda(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Bag bag;

  if (_phase == Phase::SEND_CLOSE) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::CLOSE));
  } else if (_phase == Phase::SEND_VEHICLE) {

    assert(_arrived);

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, _vehicle));
  } else if (_phase == Phase::SEND_OPEN) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN));
  } else if (_phase == Phase::SEND_STATE) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE));
  }
  return bag;
}

artis::common::event::Value
Inserter::observe(const artis::traffic::core::Time & /* t */, unsigned int index) const {
  switch (index) {
    case vars::OPEN:
      return (bool) (_phase == Phase::OPEN);
    case vars::CLOSE:
      return (bool) (_phase == Phase::CLOSE);
    case vars::PHASE:
      return Phase::to_string(_phase);
    case vars::VEHICLE_NUMBER:
      return (unsigned int) (_arrived ? 1 : 0);
    case vars::VEHICLE_INDEX:
      return (int) (_arrived ? _vehicle.index() : -1);
    default:
      return {};
  }
}

}