/**
 * @file artis-traffic/micro/core/Node.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Node.hpp"

namespace artis::traffic::micro::core {

template<class Vehicle>
void Node<Vehicle>::dconf(const artis::traffic::core::Time &t,
                          const artis::traffic::core::Time &e,
                          const artis::traffic::core::Bag &bag) {

//  std::cout << t << " [" << get_full_name() << "] =====> dconf [BEFORE]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

  // when receive CLOSE and phase = SEND_OPEN_AFTER_ARRIVED
  dint(t);
  dext(t, e, bag);

//  std::cout << t << " [" << get_full_name() << "] =====> dconf [AFTER]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

}

template<class Vehicle>
void Node<Vehicle>::dint(const artis::traffic::core::Time &t) {

//  std::cout << t << " [" << get_full_name() << "] =====> dint [BEFORE]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived
//            << " ; " << _last_time << std::endl;

  _fixed_duration_sigma -= t - _last_time;
  if (std::abs(_fixed_duration_sigma) < 1e-6) {
    _fixed_duration_sigma = 0;
  }
  if (_arrived and _phase != Phase::SEND_CLOSE and _phase != Phase::SEND_STATE) {
    delete _vehicle;
    _vehicle = nullptr;
  }
  switch (_phase) {
    case Phase::INIT: {
      _phase = _delay_start == 0 ? Phase::SEND_OPEN : Phase::SEND_FIRST_CLOSE;
      break;
    }
    case Phase::OPEN: {
      if (not _arrived) {
        if (not _next_close) {
          _phase = Phase::SEND_CLOSE;
        } else {
          _phase = Phase::CLOSE;
          _sigma = _close_duration;
        }
      }
      break;
    }
    case Phase::CLOSE: {
      if (not _arrived) {
        if (not _next_close) {
          _phase = Phase::SEND_OPEN;
        } else if (_fixed_duration_sigma > 0) {
          _phase = Phase::OPEN;
          _sigma = _open_duration;
        } else {
          _phase = Phase::SEND_CLOSE;
          _sigma = 0;
          _fixed_duration_sigma = _close_duration;
        }
      } else {
        _arrived = false;
        _sigma -= _occupied_duration;
        _sigma = _sigma < 0 ? 0 : _sigma;
        _phase = Phase::SEND_OPEN_AFTER_ARRIVED;
      }
      break;
    }
    case Phase::SEND_OPEN_AFTER_ARRIVED: {
      _phase = Phase::OPEN;
      break;
    }
    case Phase::SEND_OPEN: {
      _phase = Phase::OPEN;
      if (_fixed_duration_sigma == 0) {
        _sigma = _open_duration;
        _fixed_duration_sigma = _open_duration;
      }
      break;
    }
    case Phase::SEND_CLOSE: {
      _phase = Phase::CLOSE;
      if (not _arrived) {
        if (not _next_close) {
          _sigma = _close_duration;
          _fixed_duration_sigma = _close_duration;
        } else {
          _sigma = _close_duration;
        }
      }
      break;
    }
    case Phase::SEND_FIRST_CLOSE: {
      _phase = Phase::CLOSE;
      if (not _arrived) {
        if (not _next_close) {
          _sigma = _delay_start;
        }
      }
      break;
    }
    case Phase::SEND_STATE: {
      _phase = _stored_phase;
      _sigma = _stored_sigma;
      break;
    }
  }
  _last_time = t;

//  std::cout << t << " [" << get_full_name() << "] =====> dint [AFTER]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

}

template<class Vehicle>
void Node<Vehicle>::dext(const artis::traffic::core::Time &t,
                         const artis::traffic::core::Time &e,
                         const artis::traffic::core::Bag &bag) {

//  std::cout << t << " [" << get_full_name() << "] =====> dext [BEFORE]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

  _fixed_duration_sigma -= e;
  if (std::abs(_fixed_duration_sigma) < 1e-6) {
    _fixed_duration_sigma = 0;
  }
  std::for_each(bag.begin(), bag.end(),
                [t, e, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {
                    Vehicle vehicle{};

                    assert(_phase == Phase::OPEN and _vehicle == nullptr);

                    event.data()(vehicle);

//                    std::cout << "[" << get_full_name() << "] at " << t << " => " << vehicle.index() << std::endl;

                    _vehicle = new Vehicle(vehicle);
                    _arrived = true;
                    _phase = Phase::SEND_CLOSE;
                    _sigma -= e;
                    _out_index = _vehicle->current_index();
                  } else if (event.on_port(inputs::CLOSE)) {

//                    std::cout << "[" << get_full_name() << "] at " << t << " => CLOSE" << std::endl;

                    if (_phase == Phase::OPEN) {
                      _phase = Phase::SEND_CLOSE;
                    }
                    _next_close = true;
                    event.data()(_next_close_data);
                  } else if (event.on_port(inputs::OPEN)) {
                    double p = std::floor((t - _delay_start) / (_open_duration + _close_duration));

//                    std::cout << "[" << get_full_name() << "] at " << t << " => OPEN" << std::endl;

                    if (_phase == Phase::CLOSE and not _arrived and
                        t < _delay_start + p * (_open_duration + _close_duration) + _open_duration) {
                      _phase = Phase::SEND_OPEN;
                    } else {
                      _sigma -= e;
                    }
                    _next_close = false;
                    _next_close_data = {close_type::SECURITY, -1, -1};
                  }
                });

  std::for_each(bag.begin(), bag.end(),
                [e, bag, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::STATE)) {
                    if (bag.size() > 1) {
                      _stored_phase = _phase;
                      _stored_sigma = _sigma;
                      _phase = Phase::SEND_STATE;
                    } else {
                      _stored_phase = _phase;
                      _stored_sigma = _sigma - e;
                      _phase = Phase::SEND_STATE;
                    }
                  }
                });
  _last_time = t;

//  std::cout << t << " [" << get_full_name() << "] =====> dext [AFTER]: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

//  assert((_arrived and not _next_close) or (not _arrived and _next_close)
//             or (not _next_close and not _arrived));
}

template<class Vehicle>
void Node<Vehicle>::start(const artis::traffic::core::Time &t) {
  _arrived = false;
  _vehicle = nullptr;
  _next_close = false;
  _sigma = _delay_start > 0 ? _delay_start : _open_duration;
  _phase = Phase::INIT;
  _next_close_data = {close_type::SECURITY, -1, 0};
  _fixed_duration_sigma = _delay_start > 0 ? _delay_start : _open_duration;
  _last_time = t;
}

template<class Vehicle>
artis::traffic::core::Time Node<Vehicle>::ta(const artis::traffic::core::Time & /* t */) const {

//  std::cout << t << " [" << get_full_name() << "] =====> ta: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << " ; " << _arrived << std::endl;

  if (_arrived) {
    if (_phase == Phase::SEND_CLOSE or _phase == Phase::SEND_STATE) {
      return 0;
    } else {
      return _occupied_duration;
    }
  } else {
    switch (_phase) {
      case Phase::OPEN:
      case Phase::CLOSE:
        return std::min(_sigma, _fixed_duration_sigma);
      case Phase::INIT:
      case Phase::SEND_OPEN_AFTER_ARRIVED:
      case Phase::SEND_OPEN:
      case Phase::SEND_CLOSE:
      case Phase::SEND_FIRST_CLOSE:
      case Phase::SEND_STATE:
        return 0;
    }
  }
  return artis::common::DoubleTime::infinity;
}

template<class Vehicle>
artis::traffic::core::Bag Node<Vehicle>::lambda(const artis::traffic::core::Time &t) const {
  artis::traffic::core::Bag bag;

//  std::cout << t << " [" << get_full_name() << "] =====> lambda: " << Phase::to_string(_phase)
//            << " ; sigma = " << _sigma << " ; " << _fixed_duration_sigma << std::endl;

  if (_arrived and _phase != Phase::SEND_CLOSE and _phase != Phase::SEND_STATE) {
    Vehicle out_vehicle;
    if (_vehicle->path().has_next()) {
      _vehicle->forward();
      out_vehicle = *_vehicle;
    } else {
      out_vehicle = Vehicle(_vehicle->index(), _vehicle->length(), _vehicle->gap(), _vehicle->speed(),
                            _vehicle->max_speed(),
                            _vehicle->acceleration(), _vehicle->max_acceleration(), _vehicle->reaction_time(),
                            {0}, _vehicle->data());
    }
    if (_out_number == 1) {
      bag.push_back(
        artis::traffic::core::ExternalEvent(outputs::OUT, out_vehicle));
    } else {
      bag.push_back(
        artis::traffic::core::ExternalEvent(outputs::OUT + _out_index, out_vehicle));
    }
  } else if (_phase == Phase::SEND_CLOSE or _phase == Phase::SEND_FIRST_CLOSE) {
    if (_vehicle != nullptr) {

//      std::cout << t << " => CLOSE OCCUPIED -> " << t + _occupied_duration << std::endl;

      bag.push_back(
        artis::traffic::core::ExternalEvent(
          outputs::CLOSE, close_data{close_type::OCCUPIED, t + _occupied_duration, -1}));
    } else {
      double p = std::floor((t - _delay_start) / (_open_duration + _close_duration));

      if (_next_close_data.type == close_type::CAPACITY and _next_close_data.time != -1 and
          t < _delay_start + p * (_open_duration + _close_duration) + _open_duration) {

//        std::cout << t << " => CLOSE CAPACITY -> " << _next_close_data.time << std::endl;

        bag.push_back(
          artis::traffic::core::ExternalEvent(
            outputs::CLOSE, close_data{close_type::CAPACITY, _next_close_data.time, -1}));
      } else {
        if (_phase == Phase::SEND_FIRST_CLOSE) {

//          std::cout << t << " => *** CLOSE -> " << t + _delay_start << std::endl;

          bag.push_back(
            artis::traffic::core::ExternalEvent(
              outputs::CLOSE, close_data{close_type::ABSOLUTE, t + _delay_start, -1}));
        } else {

//          std::cout << t << " => *** CLOSE -> " << t + _close_duration << std::endl;

          bag.push_back(
            artis::traffic::core::ExternalEvent(
              outputs::CLOSE, close_data{close_type::ABSOLUTE, t + _close_duration, -1}));
        }
      }
    }
  } else if (_phase == Phase::SEND_OPEN or _phase == Phase::SEND_OPEN_AFTER_ARRIVED) {

//    std::cout << t << " => *** OPEN -> " << t + _fixed_duration_sigma << std::endl;

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN, open_data{t + _fixed_duration_sigma, -1}));
  } else if (_phase == Phase::SEND_STATE) {
    if (_arrived) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE, (double) 0.0));
    } else {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE, (double) -1.0));
    }
  }
  return bag;
}

template<class Vehicle>
artis::common::event::Value Node<Vehicle>::observe(const artis::traffic::core::Time & /* t */,
                                                   unsigned int index) const {
  switch (index) {
    case vars::OPEN:
      return (bool) (_phase == Phase::OPEN);
    case vars::CLOSE:
      return (bool) (_phase == Phase::CLOSE);
    case vars::PHASE:
      return Phase::to_string(_phase);
    case vars::VEHICLE_NUMBER:
      return (unsigned int) (_arrived ? 1 : 0);
    default:
      return {};
  }
}

}