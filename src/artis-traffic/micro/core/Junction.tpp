/**
 * @file artis-traffic/micro/core/Junction.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/Junction.hpp>
#include <artis-traffic/micro/core/Data.hpp>

//#include <iostream>

namespace artis::traffic::micro::core {

template<class Vehicle>
void Junction<Vehicle>::dconf(const artis::traffic::core::Time &t,
                              const artis::traffic::core::Time &e,
                              const artis::traffic::core::Bag &bag) {
  dext(t, e, bag);
  dint(t);
}

template<class Vehicle>
void Junction<Vehicle>::dint(const artis::traffic::core::Time & /* t */) {
  switch (_vehicle_phase) {
    case VehiclePhase::INIT: {
      _vehicle_phase = VehiclePhase::WAIT;
      break;
    }
    case VehiclePhase::SEND_VEHICLE: {
      _vehicle.reset(nullptr);
      _vehicle_phase = VehiclePhase::WAIT;
      break;
    }
    default:
      break;
  }
  switch (_open_phase) {
    case OpenPhase::INIT: {
      _open_phase = OpenPhase::WAIT;
      break;
    }
    case OpenPhase::SEND_OPEN: {
      _open_phase = OpenPhase::WAIT;
      _open_send_indexes.clear();
      break;
    }
    default:
      break;
  }
  switch (_close_phase) {
    case ClosePhase::INIT: {
      _close_phase = ClosePhase::WAIT;
      break;
    }
    case ClosePhase::SEND_CLOSE: {
      _close_phase = ClosePhase::WAIT;
      _close_send_indexes.clear();
      break;
    }
    default:
      break;
  }
}

template<class Vehicle>
void Junction<Vehicle>::dext(const artis::traffic::core::Time &t,
                             const artis::traffic::core::Time & /* e */,
                             const artis::traffic::core::Bag &bag) {
  std::for_each(bag.begin(), bag.end(),
                [this, t](const artis::traffic::core::ExternalEvent &event) {
                  if (event.port_index() >= inputs::IN and event.port_index() < inputs::OPEN) {
                    Vehicle vehicle{};

                    event.data()(vehicle);

                    assert(_vehicle_phase == VehiclePhase::WAIT and _vehicle == nullptr);

                    _vehicle.reset(new Vehicle(vehicle));
                    _vehicle_phase = VehiclePhase::SEND_VEHICLE;
                    _out_index = _vehicle->current_index();
                  } else if (event.port_index() >= inputs::OPEN and event.port_index() < inputs::CLOSE) {
                    int index = event.port_index() - inputs::OPEN;

//                    std::cout << t << ": " << this->get_name() << " -> OPEN " << index << std::endl;

                    _open_phase = OpenPhase::SEND_OPEN;
                    _next_close[index] = false;
                    _next_close_data[index] = {close_type::SECURITY, -1, index};
                    _open_send_indexes.push_back(index);
                  } else if (event.port_index() >= inputs::CLOSE and event.port_index() < inputs::STATE) {
                    int index = event.port_index() - inputs::CLOSE;

//                    std::cout << t << ": " << this->get_name() << " -> CLOSE " << index << std::endl;

                    _close_phase = ClosePhase::SEND_CLOSE;
                    _next_close[index] = true;
                    event.data()(_next_close_data[index]);
                    _next_close_data[index].index = index;
                    _close_send_indexes.push_back(index);
                  }
                });
}

template<class Vehicle>
void Junction<Vehicle>::start(const artis::traffic::core::Time & /* t */) {
  // vehicle state machine
  _vehicle_phase = VehiclePhase::INIT;
  _vehicle = nullptr;
  _out_index = 0;

  // open state machine
  _open_phase = OpenPhase::INIT;
  _open_send_indexes.clear();

  // close state machine
  _close_phase = ClosePhase::INIT;
  for (unsigned int i = 0; i < _out_number; i++) {
    _next_close_data.push_back({close_type::SECURITY, -1, 0});
    _next_close.push_back(false);
  }
  _close_send_indexes.clear();
}

template<class Vehicle>
artis::traffic::core::Time Junction<Vehicle>::ta(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Time vehicle_ta = (_vehicle_phase == VehiclePhase::INIT or
                                           _vehicle_phase == VehiclePhase::SEND_VEHICLE ? 0
                                                                                        : artis::common::DoubleTime::infinity);
  artis::traffic::core::Time open_ta = (_open_phase == OpenPhase::INIT or
                                        _open_phase == OpenPhase::SEND_OPEN ? 0
                                                                            : artis::common::DoubleTime::infinity);
  artis::traffic::core::Time close_ta = (_close_phase == ClosePhase::INIT or
                                         _close_phase == ClosePhase::SEND_CLOSE ? 0
                                                                                : artis::common::DoubleTime::infinity);

  return std::min({vehicle_ta, open_ta, close_ta});
}

template<class Vehicle>
artis::traffic::core::Bag Junction<Vehicle>::lambda(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Bag bag;

  if (_open_phase == OpenPhase::INIT) {
    for (unsigned int i = 0; i < _in_number; i++) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN + i, open_data{-1.0, -1}));
    }
  }
  if (_vehicle_phase == VehiclePhase::SEND_VEHICLE) {
    Vehicle out_vehicle;

    if (_vehicle->path().has_next()) {
      _vehicle->forward();
      out_vehicle = *_vehicle;
    } else {
      out_vehicle = Vehicle(_vehicle->index(), _vehicle->length(), _vehicle->gap(), _vehicle->speed(),
                            _vehicle->max_speed(), _vehicle->acceleration(), _vehicle->max_acceleration(),
                            _vehicle->reaction_time(), {0}, _vehicle->data());
    }
    if (_out_number == 1) {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, out_vehicle));
    } else {
      bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT + (int) _out_index, out_vehicle));
    }
  }
  if (_close_phase == ClosePhase::SEND_CLOSE) {
    for (unsigned int i = 0; i < _in_number; i++) {
      for (int _path_send_index: _close_send_indexes) {
        bag.push_back(artis::traffic::core::ExternalEvent(outputs::CLOSE + i, close_data{close_type::CAPACITY,
                                                                                         _next_close_data[_path_send_index].time,
                                                                                         _path_send_index}));
      }
    }
  }
  if (_open_phase == OpenPhase::SEND_OPEN) {
    for (unsigned int i = 0; i < _in_number; i++) {
      for (int _path_send_index: _open_send_indexes) {
        bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN + i, open_data{-1.0, _path_send_index}));
      }
    }
  }
  return bag;
}

template<class Vehicle>
artis::common::event::Value Junction<Vehicle>::observe(const artis::traffic::core::Time & /* t */,
                                                       unsigned int index) const {
  switch (index) {
    case vars::VEHICLE_NUMBER:
      return (unsigned int) (_vehicle != nullptr ? 1 : 0);
    case vars::VEHICLE_INDEX:
      return (int) (_vehicle != nullptr ? _vehicle->index() : -1);
    default:
      return {};
  }
}

}