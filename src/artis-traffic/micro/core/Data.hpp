#ifndef ARTIS_TRAFFIC_MICRO_CORE_DATA_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_DATA_HPP

#include <string>

namespace artis::traffic::micro::core {

struct close_type {
  enum values {
    CAPACITY, OCCUPIED, ABSOLUTE, SECURITY
  };
};

struct close_data {
  close_type::values type{};
  double time{};
  int index{};

  bool operator==(const close_data &other) const {
    return type == other.type and time == other.time and index == other.index;
  }

  std::string to_string() const {
    return "close_data<" + std::to_string(type) + " ; " + std::to_string(time) + " ; " + std::to_string(index) + ">";
  }
};

struct open_data {
  double time{};
  int index{};

  bool operator==(const open_data &other) const {
    return time == other.time and index == other.index;
  }

  std::string to_string() const {
    return "open_data<" + std::to_string(time) + " ; " + std::to_string(index) + ">";
  }
};

template<class Vehicle>
struct state_data {
  Vehicle vehicle{};
  double distance{};
  double duration{};
  unsigned int index{};

  bool operator==(const state_data<Vehicle> &other) const {
    return vehicle == other.vehicle and distance == other.distance and duration == other.duration and
           index == other.index;
  }

  std::string to_string() const {
    return "state_data<" + vehicle.to_string() + " ; " + std::to_string(distance) + " ; " + std::to_string(duration) +
           " ; " + std::to_string(index) + ">";
  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_CORE_DATA_HPP
