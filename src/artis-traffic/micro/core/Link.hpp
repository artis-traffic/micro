/**
 * @file artis-traffic/micro/core/Link.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_HPP

#include <artis-traffic/micro/core/link-state-machine/LinkIDs.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkTypes.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkRootStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkStateMachine.hpp>

#include <artis-star-addons/pdevs/StateMachineDynamics.hpp>

#include <artis-traffic/core/Base.hpp>

namespace artis::traffic::micro::core {

struct LinkParameters {
  double max_speed;
  double length;
  unsigned int downstream_link_number;
  unsigned int concurrent_link_number; // number of concurrent links without stop
  unsigned int concurrent_stop_number; // number of concurrent links with stop
  std::vector<bool> priorities; // true if the link has priority over concurrent link i
};

template<class Model, class Parameters, class Types>
class LinkImplementation
  : public artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, Model, Parameters,
    typename Types::root_state_machine_type> {
public:
  struct inputs {
    enum values {
      IN, OPEN = 1000, CLOSE = 1500, STATE = 2000, STATE_DATA = 2500, DISRUPT = 3000, LAST = DISRUPT + 500
    };
  };

  struct outputs {
    enum values {
      OUT, OPEN, CLOSE, STATE, STATE_DATA = 500, ARRIVED = 1000, LAST = ARRIVED + 500
    };
  };

  struct vars {
    enum values {
      VEHICLE_NUMBER, VEHICLE_POSITIONS, VEHICLE_INDEXES, VEHICLE_ACCELERATIONS, VEHICLE_SPEEDS,
      VEHICLE_STATES, EVENT_COUNT, EVENT_FREQUENCY, LAST = EVENT_FREQUENCY
    };
  };

  LinkImplementation(const std::string &name,
                     const artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters> &context) :
    artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, Model, Parameters,
      typename Types::root_state_machine_type>(name, context),
    _max_speed(context.parameters().max_speed),
    _length(context.parameters().length),
    _downstream_link_number(context.parameters().downstream_link_number),
    _concurrent_link_number(context.parameters().concurrent_link_number),
    _concurrent_stop_number(context.parameters().concurrent_stop_number),
    _priorities(context.parameters().priorities) {
    // input ports
    this->input_port({inputs::IN, "in"});
    for (unsigned int i = 0; i < _concurrent_link_number; ++i) {
      std::stringstream ss_state_data;
      ss_state_data << "state_data_" << (i + 1);
      this->input_port({inputs::STATE_DATA + i, ss_state_data.str()});
    }
    for (unsigned int i = 0; i < _concurrent_link_number + _concurrent_stop_number; ++i) {
      std::stringstream ss_state;
      ss_state << "state_" << (i + 1);
      this->input_port({inputs::STATE + i, ss_state.str()});
    }

    // output ports
    for (unsigned int i = 0; i < _concurrent_link_number; ++i) {
      std::stringstream ss_state;
      ss_state << "state_" << (i + 1);
      this->output_port({outputs::STATE + i, ss_state.str()});
    }
    for (unsigned int i = 0; i < _concurrent_link_number + _concurrent_stop_number; ++i) {
      std::stringstream ss_state_data;
      ss_state_data << "state_data_" << (i + 1);
      this->output_port({outputs::STATE_DATA + i, ss_state_data.str()});
    }
    this->input_ports({{inputs::OPEN,    "open"},
                       {inputs::CLOSE,   "close"},
                       {inputs::DISRUPT, "disrupt"}});
    this->output_ports({{outputs::OUT,     "out"},
                        {outputs::CLOSE,   "close"},
                        {outputs::OPEN,    "open"},
                        {outputs::ARRIVED, "arrived"}});

    // observables
    this->observables({{vars::VEHICLE_NUMBER,        "vehicle_number"},
                       {vars::VEHICLE_POSITIONS,     "vehicle_positions"},
                       {vars::VEHICLE_INDEXES,       "vehicle_indexes"},
                       {vars::VEHICLE_ACCELERATIONS, "vehicle_accelerations"},
                       {vars::VEHICLE_SPEEDS,        "vehicle_speeds"},
                       {vars::VEHICLE_STATES,        "vehicle_states"},
                       {vars::EVENT_COUNT,           "event_count"},
                       {vars::EVENT_FREQUENCY,       "event_frequency"}});
  }

  virtual ~LinkImplementation() = default;

  void dext(const artis::traffic::core::Time & /* t */,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override;

private:
// parameters
  double _max_speed;
  double _length;
  unsigned int _downstream_link_number;
  unsigned int _concurrent_link_number;
  unsigned int _concurrent_stop_number;
  std::vector<bool> _priorities;
};

template<class Types, class Parameters>
class Link : public LinkImplementation<Link<Types, Parameters>, Parameters, Types> {
public:
  Link(const std::string &name,
       const artis::pdevs::Context<artis::common::DoubleTime, Link<Types, Parameters>, LinkParameters> &context)
    : LinkImplementation<Link<Types, Parameters>, LinkParameters, Types>(name, context) {
  }

  ~Link() override = default;
};

}

#include <artis-traffic/micro/core/Link.tpp>

#endif