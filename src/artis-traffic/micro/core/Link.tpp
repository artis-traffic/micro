/**
 * @file artis-traffic/micro/core/Link.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/Data.hpp>
#include <artis-traffic/micro/core/Link.hpp>

namespace artis::traffic::micro::core {

template<class Model, class Parameters, class Types>
void LinkImplementation<Model, Parameters, Types>::dext(const artis::traffic::core::Time &t,
                                                        const artis::traffic::core::Time & /* e */,
                                                        const artis::traffic::core::Bag &bag) {

//  std::cout << "[" << get_full_name() << "] dext at " << t << " => " << bag.to_string() << std::endl;

  std::vector<unsigned int> indexes;

  std::for_each(bag.begin(), bag.end(), [&indexes](const artis::traffic::core::ExternalEvent &event) {
    if (event.port_index() >= inputs::STATE and event.port_index() < inputs::STATE_DATA) {
      unsigned int index = event.port_index() - inputs::STATE;

//      std::cout << " => ANSWER STATE => " << index << std::endl;

      indexes.push_back(index);
    }
  });
  if (not indexes.empty()) {
    this->root().transition(t, Types::state_machine_IDs_type::ANSWER_STATE,
                            typename Types::state_machine_type::ExternalEvent{Types::event_IDs_type::GET_STATE,
                                                                              indexes});
  }
  std::for_each(bag.begin(), bag.end(),
                [t, this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {
                    typename Types::vehicle_type vehicle{};

                    event.data()(vehicle);

//                    std::cout << t << ": " << this->get_name() << " -> ADD VEHICLE = " << vehicle.to_string() << index
//                              << std::endl;

                    this->root().transition(t, Types::state_machine_IDs_type::INPUT,
                                            typename Types::state_machine_type::ExternalEvent{
                                              Types::event_IDs_type::IN_VEHICLE, event.data()});
                  } else if (event.on_port(inputs::CLOSE)) {
                    close_data data{};

                    event.data()(data);

//                    std::cout << t << ": " << this->get_name() << " => CLOSE = " << data.index << std::endl;

                    this->root().transition(t, Types::state_machine_IDs_type::DOWN_OPEN_CLOSE,
                                            typename Types::state_machine_type::ExternalEvent{
                                              Types::event_IDs_type::DOWN_CLOSE, data});
                  } else if (event.on_port(inputs::OPEN)) {
                    open_data data{};

                    event.data()(data);

//                    std::cout << t << ": " << this->get_name() << " => OPEN = " << data.index << std::endl;

                    this->root().transition(t, Types::state_machine_IDs_type::DOWN_OPEN_CLOSE,
                                            typename Types::state_machine_type::ExternalEvent{
                                              Types::event_IDs_type::DOWN_OPEN,
                                              data});
//                    this->root().transition(t, Types::state_IDs_type::OUTPUT,
//                                     artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>::ExternalEvent{Types::event_IDs_type::DOWN_OPEN,
//                                                                     down_open_data{{data.time}, index}});
                  } else if (event.port_index() >= inputs::STATE_DATA and event.port_index() < inputs::DISRUPT) {
                    unsigned int index = event.port_index() - inputs::STATE_DATA;
                    state_data<typename Types::vehicle_type> data{};

                    event.data()(data);

//                    std::cout << " => RECEIVE STATE => " << data.index << std::endl;

                    data.index = index;
                    this->root().transition(t, Types::state_machine_IDs_type::STATE,
                                            typename Types::state_machine_type::ExternalEvent{
                                              Types::event_IDs_type::RECEIVE_STATE, data});
                  }
                });
}

template<class Model, class Parameters, class Types>
artis::traffic::core::Bag
LinkImplementation<Model, Parameters, Types>::lambda(const artis::traffic::core::Time &t) const {
  artis::traffic::core::Bag bag;
  const typename Types::root_state_machine_type::external_events_type &events = this->root().outbox();

//  std::cout << "[" << get_full_name() << "] lambda at " << t << std::endl;

  std::for_each(events.cbegin(), events.cend(), [t, this, &bag](const auto &e) {
    switch (e.id) {
      case Types::event_IDs_type::OPEN: {

//        std::cout << this->get_full_name() << " => SEND OPEN " << " t=" << t << std::endl;

        bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN, open_data{-1, 0}));
        break;
      }
      case Types::event_IDs_type::CLOSE: {
        artis::traffic::core::Time duration;

        e.data(duration);

//        std::cout << t << ": " << this->get_name() << " -> CLOSE " << index << std::endl;

        bag.push_back(artis::traffic::core::ExternalEvent(outputs::CLOSE, close_data{close_type::CAPACITY,
                                                                                     t + duration, 0}));
        break;
      }
      case Types::event_IDs_type::SEND_VEHICLE: {
        typename Types::vehicle_type vehicle;

        e.data(vehicle);

//        std::cout << t << ": " << this->get_name() << " => SEND VEHICLE = " << vehicle.to_string() << std::endl;

        bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
        break;
      }
      case Types::event_IDs_type::ARRIVED: {
        typename Types::vehicle_type vehicle;

        e.data(vehicle);

//        std::cout << " => ARRIVED = " << vehicle.to_string() << std::endl;

        bag.push_back(artis::traffic::core::ExternalEvent(outputs::ARRIVED));
        break;
      }
      case Types::event_IDs_type::ANSWER_STATE_DATA: {
        const auto &state = this->root().template state_<AnswerStateState<Types>>(
          Types::state_machine_IDs_type::ANSWER_STATE);

        std::for_each(state._concurrents_answer_index.cbegin(), state._concurrents_answer_index.cend(),
                      [state, &bag](const auto &index) {

//            std::cout << " => ANSWER_STATE_DATA => " << index << std::endl;

                        bag.push_back(
                          artis::traffic::core::ExternalEvent(outputs::STATE_DATA + index, state._state_data));
                      });
        break;
      }
      case Types::event_IDs_type::SEND_STATE: {
        for (int i = 0; i < (int) _concurrent_link_number; ++i) {

//          std::cout << " => STATE => " << i << std::endl;

          bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE + i));
        }
        break;
      }
      default: {
      }
    }
  });
  return bag;
}

template<class Model, class Parameters, class Types>
artis::common::event::Value LinkImplementation<Model, Parameters, Types>::observe(const artis::traffic::core::Time &t,
                                                                                  unsigned int index) const {
  const auto &state = this->root().template state_<ProcessState<Types>>(Types::state_machine_IDs_type::PROCESS);

  switch (index) {
    case vars::VEHICLE_NUMBER:
      return (unsigned int) state._vehicles.size();
    case vars::VEHICLE_POSITIONS: {
      std::vector<double> positions;

      if (not state._old_indexes.empty()) {
        for (size_t i = 0; i < state._old_indexes.size(); ++i) {
          positions.push_back(-1);
        }
      }
      if (not state._vehicles.empty()) {
        for (const typename Types::vehicle_entry_type &entry: state._vehicles) {
          positions.push_back(entry.compute_position(t));
        }
      }
      return positions;
    }
    case vars::VEHICLE_INDEXES: {
      std::vector<unsigned int> indexes;

      if (not state._old_indexes.empty()) {
        for (unsigned long old_index: state._old_indexes) {
          indexes.push_back(old_index);
        }
      }
      if (not state._vehicles.empty()) {
        for (const typename Types::vehicle_entry_type &entry: state._vehicles) {
          indexes.push_back(entry.vehicle().index());
        }
      }
      return indexes;
    }
    case vars::VEHICLE_ACCELERATIONS: {
      std::vector<double> accelerations;

      if (not state._old_indexes.empty()) {
        for (size_t i = 0; i < state._old_indexes.size(); ++i) {
          accelerations.push_back(-1);
        }
      }
      if (not state._vehicles.empty()) {
        for (const typename Types::vehicle_entry_type &entry: state._vehicles) {
          accelerations.push_back(entry.vehicle().acceleration());
        }
      }
      return accelerations;
    }
    case vars::VEHICLE_SPEEDS: {
      std::vector<double> speeds;

      if (not state._old_indexes.empty()) {
        for (size_t i = 0; i < state._old_indexes.size(); ++i) {
          speeds.push_back(-1);
        }
      }
      if (not state._vehicles.empty()) {
        for (const typename Types::vehicle_entry_type &entry: state._vehicles) {
          speeds.push_back(entry.compute_speed(t));
        }
      }
      return speeds;
    }
    case vars::VEHICLE_STATES: {
      std::vector<int> states;

      if (not state._old_indexes.empty()) {
        for (size_t i = 0; i < state._old_indexes.size(); ++i) {
          states.push_back(VehicleData::State::STOPPED);
        }
      }
      if (not state._vehicles.empty()) {
        for (const typename Types::vehicle_entry_type &entry: state._vehicles) {
          const std::deque<typename Types::vehicle_state_type> &vehicle_states = entry.states();
          const typename Types::vehicle_state_type &last_vehicle_state = vehicle_states.back();

          states.push_back(last_vehicle_state.state());
        }
      }
      return states;
    }
    case vars::EVENT_COUNT: {
      return state._event_calls;
    }
    case vars::EVENT_FREQUENCY: {
      if (state._old_indexes.size() + state._vehicles.size() > 0) {
        return (double) state._event_calls /
               (state._old_indexes.size() + state._vehicles.size());
      } else {
        return 0;
      }
    }
    default:
      return {};
  }
}

}