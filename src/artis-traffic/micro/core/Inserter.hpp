/**
 * @file artis-traffic/micro/core/Inserter.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_INSERTER_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_INSERTER_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Vehicle.hpp>

namespace artis::traffic::micro::core {

struct InserterParameters {
  double threshold;
};

class Inserter
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Inserter, InserterParameters> {
public:
  struct inputs {
    enum values {
      IN, OPEN, CLOSE, STATE
    };
  };

  struct outputs {
    enum values {
      OUT, OPEN, CLOSE, STATE
    };
  };

  struct vars {
    enum values {
      OPEN, CLOSE, PHASE, VEHICLE_NUMBER, VEHICLE_INDEX
    };
  };

  Inserter(const std::string &name,
           const artis::pdevs::Context<artis::common::DoubleTime,
             Inserter,
             InserterParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Inserter, InserterParameters>(
      name, context),
    _threshold(context.parameters().threshold) {
    input_ports({{inputs::IN,    "in"},
                 {inputs::CLOSE, "close"},
                 {inputs::OPEN,  "open"},
                 {inputs::STATE, "state"}});
    output_ports({{outputs::OUT,   "out"},
                  {outputs::CLOSE, "close"},
                  {outputs::OPEN,  "open"},
                  {outputs::STATE, "state"}});
    observables({{vars::OPEN,           "open"},
                 {vars::CLOSE,          "close"},
                 {vars::PHASE,          "phase"},
                 {vars::VEHICLE_NUMBER, "vehicle_number"},
                 {vars::VEHICLE_INDEX,  "vehicle_index"}});
  }

  ~Inserter() override = default;

  void dconf(const artis::traffic::core::Time & /* t* */,
             const artis::traffic::core::Time & /* e */,
             const artis::traffic::core::Bag & /* bag */) override;

  void dint(const artis::traffic::core::Time & /* t */) override;

  void dext(const artis::traffic::core::Time & /* t */,
            const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override;

private:
  struct Phase {
    enum values {
      INIT,
      OPEN,
      CLOSE,
      SEND_OPEN,
      SEND_CLOSE,
      SEND_STATE,
      SEND_VEHICLE,
      WAIT_DELAY,
      WAIT_STATE,
      WAIT
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case OPEN:
          return "OPEN";
        case CLOSE:
          return "CLOSE";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
        case SEND_STATE:
          return "SEND_STATE";
        case SEND_VEHICLE:
          return "SEND_VEHICLE";
        case WAIT_DELAY:
          return "WAIT_DELAY";
        case WAIT_STATE:
          return "WAIT_STATE";
        case WAIT:
          return "WAIT";
      }
      return "";
    }
  };

  // parameter
  double _threshold;

  // state
  Phase::values _phase;
  artis::traffic::core::Time _sigma;
  bool _arrived;
  Vehicle _vehicle;
  bool _next_close;
  bool _delay;
};

}

#endif