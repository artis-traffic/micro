/**
 * @file artis-traffic/micro/core/link-state-machine/DownOpenCloseStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_DOWN_OPEN_CLOSE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_DOWN_OPEN_CLOSE_STATE_MACHINE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>
#include <artis-star-addons/utils/StateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class DownOpenCloseStateMachine
  : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  DownOpenCloseStateMachine(const typename Types::root_state_machine_type &root, const Parameters & /* parameters */)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root,
                                                                                      {false, -1, -1, {}, {}, {}}) {}

  void build(const std::shared_ptr<DownOpenCloseStateMachine<Types, Parameters, StateType>> &machine);

// types
  DECLARE_STATE_TRANSITION_TYPES(DownOpenCloseStateMachine)
};

DEFINE_STATE_MACHINE_STATE(WaitDownOpenCloseState, Types::state_IDs_type::WAIT_OPEN_CLOSE, DownOpenCloseStateMachine)

DEFINE_STATE_MACHINE_TRANSITION(DownOpenCloseT1, DownOpenCloseStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
void DownOpenCloseT1<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                           const artis::common::event::Value &value) {
  close_data data{};

  value(data);
  if (data.index == -1) {
    this->_machine->state_()._node_open = false;
    this->_machine->state_()._node_opening_time = data.time;
    this->_machine->state_()._node_closing_time = -1;
  } else {
    this->_machine->state_()._open[data.index] = false;
    this->_machine->state_()._opening_times[data.index] = data.time;
    this->_machine->state_()._closing_times[data.index] = -1;
  }
}

template<class Types, class Parameters, class StateType>
bool DownOpenCloseT1<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */,
                                                          int event) {
  return event == Types::event_IDs_type::DOWN_CLOSE;
}

template<class Types, class Parameters, class StateType>
typename DownOpenCloseStateMachine<Types, Parameters, StateType>::Events
DownOpenCloseT1<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename DownOpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.internals.push_back(
    typename DownOpenCloseStateMachine<Types, Parameters, StateType>::InternalEvent{
      {Types::event_IDs_type::DOWN_CLOSE, {}},
      Types::state_machine_IDs_type::PROCESS});
  return events;
}

DEFINE_STATE_MACHINE_TRANSITION(DownOpenCloseT2, DownOpenCloseStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
void DownOpenCloseT2<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                           const artis::common::event::Value &value) {
  open_data data;

  value(data);
  if (data.index == -1) {
    this->_machine->state_()._node_open = true;
    this->_machine->state_()._node_closing_time = data.time;
    this->_machine->state_()._node_opening_time = -1;
  } else {
    this->_machine->state_()._open[data.index] = true;
    this->_machine->state_()._closing_times[data.index] = data.time;
    this->_machine->state_()._opening_times[data.index] = -1;
  }
}

template<class Types, class Parameters, class StateType>
bool DownOpenCloseT2<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */,
                                                          int event) {
  return event == Types::event_IDs_type::DOWN_OPEN;
}

template<class Types, class Parameters, class StateType>
typename DownOpenCloseStateMachine<Types, Parameters, StateType>::Events
DownOpenCloseT2<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename DownOpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.internals.push_back(
    typename DownOpenCloseStateMachine<Types, Parameters, StateType>::InternalEvent{
      {Types::event_IDs_type::DOWN_OPEN, {}},
      Types::state_machine_IDs_type::PROCESS});
  return events;
}

template<class Types, class Parameters, class StateType>
void DownOpenCloseStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<DownOpenCloseStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitDownOpenCloseState<Types, Parameters, StateType>(machine));
  this->initial_state(Types::state_IDs_type::WAIT_OPEN_CLOSE);
  this->transition(Types::state_IDs_type::WAIT_OPEN_CLOSE,
                   Types::state_IDs_type::WAIT_OPEN_CLOSE,
                   new DownOpenCloseT1<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::WAIT_OPEN_CLOSE,
                   Types::state_IDs_type::WAIT_OPEN_CLOSE,
                   new DownOpenCloseT2<Types, Parameters, StateType>(machine));
}

}

#endif