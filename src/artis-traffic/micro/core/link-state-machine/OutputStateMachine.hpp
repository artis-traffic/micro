/**
 * @file artis-traffic/micro/core/link-state-machine/OutputStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_OUTPUT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_OUTPUT_STATE_MACHINE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>
#include <artis-star-addons/utils/StateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class OutputStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  OutputStateMachine(const typename Types::root_state_machine_type &root, const Parameters & /* parameters */)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}) {}

  void build(const std::shared_ptr<OutputStateMachine<Types, Parameters, StateType>> &machine);

// types
  DECLARE_STATE_TRANSITION_TYPES(OutputStateMachine)
};

DEFINE_STATE_MACHINE_STATE(WaitReadyToExitState, Types::state_IDs_type::WAIT_READY_TO_EXIT, OutputStateMachine)

DEFINE_STATE_MACHINE_STATE(WaitOpenState, Types::state_IDs_type::WAIT_OPEN, OutputStateMachine)

DEFINE_STATE_MACHINE_TRANSITION(OutputT1, OutputStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
void OutputT1<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                    const artis::common::event::Value &value) {
  value(this->_machine->state_()._vehicle);
}

template<class Types, class Parameters, class StateType>
bool OutputT1<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */,
                                                   int event) { return event == Types::event_IDs_type::READY_TO_EXIT; }

template<class Types, class Parameters, class StateType>
typename OutputStateMachine<Types, Parameters, StateType>::Events
OutputT1<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename OutputStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename OutputStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::SEND_VEHICLE,
                                                                             this->_machine->state_()._vehicle});
  return events;
}

template<class Types, class Parameters, class StateType>
void
OutputStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<OutputStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitReadyToExitState<Types, Parameters, StateType>(machine));
  this->state(new WaitOpenState<Types, Parameters, StateType>(machine));
  this->initial_state(Types::state_IDs_type::WAIT_READY_TO_EXIT);
  this->transition(Types::state_IDs_type::WAIT_READY_TO_EXIT,
                   Types::state_IDs_type::WAIT_READY_TO_EXIT,
                   new OutputT1<Types, Parameters, StateType>(machine));
}

}

#endif