/**
 * @file artis-traffic/micro/core/link-state-machine/StateStateMachine.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/link-state-machine/StateStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
void StateStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<StateStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new Idle2State<Types, Parameters, StateType>(machine));
  this->state(new WaitStateState<Types, Parameters, StateType>(machine));
  this->initial_state(Types::state_IDs_type::IDLE);
  this->transition(Types::state_IDs_type::IDLE, Types::state_IDs_type::WAIT_STATE,
                   new StateT1<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::WAIT_STATE, Types::state_IDs_type::WAIT_STATE,
                   new StateT2<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::WAIT_STATE, Types::state_IDs_type::IDLE,
                   new StateT3<Types, Parameters, StateType>(machine));
}

template<class Types, class Parameters, class StateType>
bool StateT1<Types, Parameters, StateType>::event(
  const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::GET_STATE;
}

template<class Types, class Parameters, class StateType>
typename StateStateMachine<Types, Parameters, StateType>::Events
StateT1<Types, Parameters, StateType>::output(
  const artis::traffic::core::Time & t) const {
  typename StateStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename StateStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::SEND_STATE, {}});
  return events;
}

template<class Types, class Parameters, class StateType>
void StateT2<Types, Parameters, StateType>::action(const artis::traffic::core::Time & t,
                                                   const artis::common::event::Value &value) {
  state_data<typename Types::vehicle_type> data{};

  value(data);
  this->_machine->state_()._state_datas.push_back(data);
}

template<class Types, class Parameters, class StateType>
bool StateT2<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::RECEIVE_STATE;
}

template<class Types, class Parameters, class StateType>
void StateT3<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                   const artis::common::event::Value &value) {
  state_data<typename Types::vehicle_type> data{};

  value(data);
  this->_machine->state_()._state_datas.push_back(data);
}

template<class Types, class Parameters, class StateType>
bool StateT3<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::RECEIVE_STATE;
}

template<class Types, class Parameters, class StateType>
typename StateStateMachine<Types, Parameters, StateType>::Events
StateT3<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename StateStateMachine<Types, Parameters, StateType>::Events events{};

  events.internals.push_back(typename StateStateMachine<Types, Parameters, StateType>::InternalEvent{
    {Types::event_IDs_type::RECEIVE_STATE, this->_machine->state_()._state_datas},
    Types::state_machine_IDs_type::PROCESS});
  return events;
}

}
