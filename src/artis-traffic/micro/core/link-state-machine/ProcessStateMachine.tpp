/**
 * @file artis-traffic/micro/core/link-state-machine/ProcessStateMachine.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/VehicleData.hpp>
#include <artis-traffic/micro/core/link-state-machine/ProcessStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
void ProcessStateMachine<Types, Parameters, StateType>::add_new_vehicle(
  const artis::traffic::core::Time &t,
  const typename Types::vehicle_type &vehicle) {
  this->_state._indexes.push_back(vehicle.index());
  this->_state._vehicles.emplace_back(vehicle, artis::common::DoubleTime::infinity, _length, _max_speed,
                                      this->_state._indexes.size() == 1);

  typename Types::vehicle_entry_type &entry = this->_state._vehicles.back();
  double distance = final_position(t, entry.vehicle().index());
  auto it = this->_state._vehicles.crbegin();

  entry.init(t, distance, it + 1 != this->_state._vehicles.crend() ? &(*(it + 1)) : nullptr);
}

template<class Types, class Parameters, class StateType>
void ProcessStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<ProcessStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new IdleState<Types, Parameters, StateType>(machine));
  this->state(new ProcessVehiclesState<Types, Parameters, StateType>(machine));
  this->initial_state(Types::state_IDs_type::IDLE);
  this->transition(Types::state_IDs_type::IDLE, Types::state_IDs_type::PROCESS_VEHICLES,
                   new ProcessT1<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::PROCESS_VEHICLES, Types::state_IDs_type::PROCESS_VEHICLES,
                   new ProcessT1<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::PROCESS_VEHICLES, Types::state_IDs_type::PROCESS_VEHICLES,
                   new ProcessT2<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::PROCESS_VEHICLES, Types::state_IDs_type::PROCESS_VEHICLES,
                   new ProcessT3<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::PROCESS_VEHICLES, Types::state_IDs_type::PROCESS_VEHICLES,
                   new ProcessT4<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::PROCESS_VEHICLES, Types::state_IDs_type::PROCESS_VEHICLES,
                   new ProcessT5<Types, Parameters, StateType>(machine));
}

template<class Types, class Parameters, class StateType>
double ProcessStateMachine<Types, Parameters, StateType>::final_position(const artis::traffic::core::Time &t,
                                                                         size_t index) const {
  auto it = this->_state._vehicles.begin();
  double min_distance_to_final = 0;

  while (it != this->_state._vehicles.end() and it->vehicle().index() != index) {
    min_distance_to_final += it->vehicle().length() + it->vehicle().gap();
    ++it;
  }

  assert(it != this->_state._vehicles.end());

  double d = _length;
  bool stop = false;

  while (not stop and it != this->_state._vehicles.begin()) {
    --it;
    if (it->states().back().state() == VehicleData::State::STOPPED or
        it->states().back().state() == VehicleData::State::RESTART) {
      d = it->compute_position(t);
      stop = true;
    } else if (it->states().back().state() == VehicleData::State::IN_DECELERATION) {
      double deceleration_distance = (it->compute_speed(t) * it->compute_speed(t)) / (2 * it->vehicle().acceleration());

      d = it->compute_position(t) + deceleration_distance;
      stop = true;
    } else if (it->states().back().state() == VehicleData::State::RUNNING) {
      double running_distance = it->compute_speed(t) * (it->next_time() - t);
      double deceleration_distance = (it->compute_speed(t) * it->compute_speed(t)) / (2 * it->vehicle().acceleration());

      d = it->compute_position(t) + deceleration_distance + running_distance;
      stop = true;
    } else if (it->states().back().state() == VehicleData::State::IN_ACCELERATION) {
      double acceleration_duration = it->next_time() - t;
      double final_speed = it->compute_speed(t) + acceleration_duration * it->vehicle().acceleration();
      double acceleration_distance = it->compute_speed(t) * acceleration_duration +
                                     0.5 * it->vehicle().acceleration() * acceleration_duration * acceleration_duration;
      double deceleration_distance = (final_speed * final_speed) / (2 * it->vehicle().acceleration());

      d = it->compute_position(t) + deceleration_distance + acceleration_distance;
      stop = true;
    }
  }
  while (it != this->_state._vehicles.end() and it->vehicle().index() != index) {
    d -= it->vehicle().length() + it->vehicle().gap();
    ++it;
  }
  if (std::abs(d) < 1e-10) {
    d = 0;
  }
  if (std::abs(_length - min_distance_to_final) < 1e-10) {
    min_distance_to_final = _length;
  }

  return std::abs(d - _length + min_distance_to_final) > 1e-6 and d < _length - min_distance_to_final ? d : _length -
                                                                                                            min_distance_to_final;
}

template<class Types, class Parameters, class StateType>
bool ProcessStateMachine<Types, Parameters, StateType>::is_full(const artis::traffic::core::Time &t) const {
  if (this->_state._vehicles.empty()) {
    return false;
  } else {
    const typename Types::vehicle_type &last_vehicle = this->_state._vehicles.back().vehicle();

    return final_position(t, last_vehicle.index()) - last_vehicle.gap() - last_vehicle.length() <=
           (_max_speed * _max_speed / (2 * last_vehicle.acceleration()));
  }
}

template<class Types, class Parameters, class StateType>
void ProcessStateMachine<Types, Parameters, StateType>::next_vehicle_state(
  const artis::traffic::core::Time &t,
  const typename std::deque<typename Types::vehicle_entry_type>::iterator &it,
  bool go, double node_next_opening_time,
  double node_next_closing_time) {
  this->_state._event_calls++;
  double fp = final_position(t, it->vehicle().index());

  it->next_state(t, fp, it + 1 != this->_state._vehicles.end() ? &(*(it + 1)) : nullptr,
                 it != this->_state._vehicles.begin() ? &(*(it - 1)) : nullptr,
                 go, node_next_opening_time, node_next_closing_time);
}

template<class Types, class Parameters, class StateType>
void ProcessStateMachine<Types, Parameters, StateType>::output_vehicle(
  const typename Types::vehicle_type &vehicle) {
  this->_state._out_vehicle = vehicle;
  this->_state._ready_to_exit = true;
  this->_state._vehicles.pop_front();
  this->_state._old_indexes.push_back(this->_state._indexes.front());
  this->_state._indexes.pop_front();
  if (not this->_state._vehicles.empty()) {
    this->_state._vehicles.front().first();
  }
}

template<class Types, class Parameters, class StateType>
std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator>
ProcessStateMachine<Types, Parameters, StateType>::waked_vehicles(
  const artis::traffic::core::Time &t) {
  std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator> list;

  auto it = this->_state._vehicles.begin();

  while (it != this->_state._vehicles.end()) {
    if (std::abs(t - it->next_time()) < 1e-6 and not it->states().back().ready_to_exit()) {
      list.push_back(it);
    }
    ++it;
  }
  return list;
}

template<class Types, class Parameters, class StateType>
void ProcessT2<Types, Parameters, StateType>::action(
  const artis::traffic::core::Time &t,
  const artis::common::event::Value & /* value */) {
  std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator> waked_vehicles = this->_machine->waked_vehicles(
    t);

  this->_machine->state_()._ready_to_exit = false;
  this->_machine->state_()._full = this->_machine->is_full(t);
  if (waked_vehicles.empty()) {
    if (this->_machine->state_()._vehicles.front().states().back().ready_to_exit()) {
      const typename Types::vehicle_type &vehicle = this->_machine->state_()._vehicles.front().vehicle();
      const auto &state = this->_machine->root().template state_<DownOpenCloseState>(
        Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);

      if (state._node_open and state._open.at(vehicle.current_index())) {
        this->_machine->output_vehicle(vehicle);
      }
    }
  } else {
    const typename Types::vehicle_type &leader = this->_machine->state_()._vehicles.front().vehicle();

    for (const auto &it: waked_vehicles) {
      if (not(this->_machine->get_concurrent_link_number() > 0 and
              it->critical_position(t, this->_machine->final_position(t, it->vehicle().index())))) {
        const auto &state = this->_machine->root().template state_<DownOpenCloseState>(
          Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
        bool go = state._node_open and state._open.at(leader.current_index());

        this->_machine->next_vehicle_state(t, it, go, state._node_opening_time, state._node_closing_time);
        if (this->_machine->state_()._vehicles.front().vehicle().index() == it->vehicle().index() and
            this->_machine->state_()._vehicles.front().states().back().ready_to_exit()) {
          const typename Types::vehicle_type &vehicle = this->_machine->state_()._vehicles.front().vehicle();

          if (state._node_open and state._open.at(vehicle.current_index())) {
            this->_machine->output_vehicle(vehicle);
          }
        }
      }
    }
  }
}

template<class Types, class Parameters, class StateType>
typename ProcessStateMachine<Types, Parameters, StateType>::Events
ProcessT2<Types, Parameters, StateType>::output(
  const artis::traffic::core::Time &t) const {
  typename ProcessStateMachine<Types, Parameters, StateType>::Events events{};

  if (this->_machine->state_()._ready_to_exit) {
    events.internals.push_back(
      typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{
        {Types::event_IDs_type::READY_TO_EXIT, this->_machine->state_()._out_vehicle},
        Types::state_machine_IDs_type::OUTPUT});
    if (this->_machine->state_()._full and not this->_machine->is_full(t)) {

      events.internals.push_back(
        typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{{Types::event_IDs_type::NO_FULL, {}},
                                                                                  Types::state_machine_IDs_type::OPEN_CLOSE});
    }
  } else {
    auto leader = this->_machine->state_()._vehicles.cbegin();

    if (std::abs(leader->compute_position(t) - leader->link_length()) < 1e-6 and
        std::abs(leader->states().back().begin() - t) < 1e-6) {
      events.externals.push_back(
        typename ProcessStateMachine<Types, Parameters, StateType>::ExternalEvent{
          {Types::event_IDs_type::ARRIVED, leader->vehicle()}});
    }

    std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator> waked_vehicles = this->_machine->waked_vehicles(
      t);
    for (const auto &it: waked_vehicles) {
      if (std::abs(it->next_time() - t) < 1e-6 and it->states().back().state() != VehicleData::State::STOPPED and
          this->_machine->get_concurrent_link_number() > 0 and
          it->critical_position(t, this->_machine->final_position(t, it->vehicle().index()))) {
        events.internals.push_back(
          typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{
            {Types::event_IDs_type::GET_STATE, {}},
            Types::state_machine_IDs_type::STATE});
      }
    }
  }
  return events;
}

template<class Types, class Parameters, class StateType>
void ProcessT3<Types, Parameters, StateType>::action(
  const artis::traffic::core::Time &t,
  const artis::common::event::Value & /* value */) {
  if (not this->_machine->state_()._vehicles.empty()) {
    typename Types::vehicle_entry_type &entry = this->_machine->state_()._vehicles.front();

    if (std::abs(entry.compute_position(t) - entry.link_length()) < 1e-6) {
      const auto &state = this->_machine->root().template state_<DownOpenCloseState>(
        Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);

      if (state._node_open and state._open.at(entry.vehicle().current_index())) {
        entry.update(t, t + entry.vehicle().reaction_time(), VehicleData::State::RESTART, false, 0);
      }
    }
  }
}

template<class Types, class Parameters, class StateType>
void ProcessT4<Types, Parameters, StateType>::action(
  const artis::traffic::core::Time &t,
  const artis::common::event::Value &value) {
  std::vector<state_data<typename Types::vehicle_type>>
    data{};

  value(data);
  std::for_each(data.cbegin(), data.cend(), [this](const auto &e) {
    this->_machine->state_()._concurrent_links_data[e.index] = e;
  });

  this->_machine->state_()._ready_to_exit = false;
  std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator> waked_vehicles = this->_machine->waked_vehicles(
    t);
  for (const auto &it: waked_vehicles) {
    double fp = this->_machine->final_position(t, it->vehicle().index());

    if (it->critical_position(t, fp)) {
//    auto it = this->_machine->state_()._vehicles.begin();
      double distance_to_final_position = fp - it->compute_position(t);
      bool go = true;
      const auto &state = this->_machine->root().template state_<DownOpenCloseState>(
        Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);

      for (auto concurrent_link_state: this->_machine->state_()._concurrent_links_data) {
        if (concurrent_link_state.distance != -1) {
          bool has_priority = this->_machine->get_priorities()[concurrent_link_state.index];

          go = state._node_open and state._open.at(it->vehicle().current_index())
               and (distance_to_final_position - concurrent_link_state.distance < -1e-6 or
                    (std::abs(distance_to_final_position - concurrent_link_state.distance) < 1e-6 and
                     has_priority));
          if (not go) { break; }
        } else {
          go = state._node_open and state._open.at(it->vehicle().current_index());
        }
      }

//    if (go or (it->states().back().state() != VehicleData::State::STOPPED)) {
      this->_machine->next_vehicle_state(t, it, go, state._node_opening_time, state._node_closing_time);
//    }
      if (it->states().back().ready_to_exit()) {
        const typename Types::vehicle_type &vehicle = it->vehicle();

        if (go and state._node_open and state._open.at(vehicle.current_index())) {
          this->_machine->output_vehicle(vehicle);
        }
      }
    }
  }
}

template<class Types, class Parameters, class StateType>
typename ProcessStateMachine<Types, Parameters, StateType>::Events
ProcessT4<Types, Parameters, StateType>::output(const artis::traffic::core::Time &t) const {
  typename ProcessStateMachine<Types, Parameters, StateType>::Events events{};

  if (this->_machine->state_()._ready_to_exit) {
    events.internals.push_back(typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{
      {Types::event_IDs_type::READY_TO_EXIT, this->_machine->state_()._out_vehicle},
      Types::state_machine_IDs_type::OUTPUT});
    if (this->_machine->state_()._full and not this->_machine->is_full(t)) {

      events.internals.push_back(
        typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{{Types::event_IDs_type::NO_FULL, {}},
                                                                                  Types::state_machine_IDs_type::OPEN_CLOSE});
    }
  }
  return events;
}

template<class Types, class Parameters, class StateType>
void ProcessT5<Types, Parameters, StateType>::action(
  const artis::traffic::core::Time &t,
  const artis::common::event::Value & /* value */) {
  if (not this->_machine->state_()._vehicles.empty()) {
    const auto &state = this->_machine->root().template state_<DownOpenCloseState>(
      Types::state_machine_IDs_type::DOWN_OPEN_CLOSE);
    auto it = this->_machine->state_()._vehicles.begin();

    if (it->critical_position(t, this->_machine->final_position(t, it->vehicle().index())) and
        not state._open.at(it->vehicle().current_index()) and
        (it->states().back().state() == VehicleData::State::IN_ACCELERATION or
         it->states().back().state() == VehicleData::State::RUNNING)) {
      this->_machine->next_vehicle_state(t, it, false, state._node_opening_time, state._node_closing_time);
    }
  }
}

}