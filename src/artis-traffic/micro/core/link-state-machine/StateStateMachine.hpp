/**
 * @file artis-traffic/micro/core/link-state-machine/StateStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_STATE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_STATE_STATE_MACHINE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>
#include <artis-star-addons/utils/StateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class StateStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  StateStateMachine(const typename Types::root_state_machine_type &root, const Parameters &parameters) :
    artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
    _concurrent_link_number(parameters.concurrent_link_number) {}

  virtual ~StateStateMachine() = default;

  void build(const std::shared_ptr<StateStateMachine<Types, Parameters, StateType>> &machine);

  unsigned int get_concurrent_link_number() const { return _concurrent_link_number; }

// types
  DECLARE_STATE_TRANSITION_TYPES(StateStateMachine)

private:
// parameters
  unsigned int _concurrent_link_number;
};

DEFINE_STATE_MACHINE_STATE(Idle2State, Types::state_IDs_type::IDLE, StateStateMachine)

DEFINE_STATE_MACHINE_STATE(WaitStateState, Types::state_IDs_type::WAIT_STATE, StateStateMachine)

DEFINE_STATE_MACHINE_TRANSITION(StateT1, StateStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
void StateT1<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                   const artis::common::event::Value & /* value */) {
  this->_machine->state_()._state_datas.clear();
}

DEFINE_STATE_MACHINE_TRANSITION(StateT2, StateStateMachine, NO_ATTRIBUTE, true, true, true, false, false)

template<class Types, class Parameters, class StateType>
bool StateT2<Types, Parameters, StateType>::guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const {
  return this->_machine->state_()._state_datas.size() < this->_machine->get_concurrent_link_number() - 1;
}

DEFINE_STATE_MACHINE_TRANSITION(StateT3, StateStateMachine, NO_ATTRIBUTE, true, true, true, false, true)

template<class Types, class Parameters, class StateType>
bool StateT3<Types, Parameters, StateType>::guard(const artis::traffic::core::Time & /* t */, const artis::common::event::Value & /* value */) const {
  return this->_machine->state_()._state_datas.size() == this->_machine->get_concurrent_link_number() - 1;
}

}

#include <artis-traffic/micro/core/link-state-machine/StateStateMachine.tpp>

#endif
