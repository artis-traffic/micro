/**
 * @file artis-traffic/micro/core/link-state-machine/ProcessStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_PROCESS_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_PROCESS_STATE_MACHINE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkIDs.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkStates.hpp>
#include <artis-star-addons/utils/StateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class ProcessStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  ProcessStateMachine(const typename Types::root_state_machine_type &root, const Parameters &parameters) :
    artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(
      root, {{}, {}, {}, {}, false, false,
             std::vector<state_data<typename Types::vehicle_type>>(parameters.concurrent_link_number), 0}),
    _length(parameters.length), _max_speed(parameters.max_speed),
    _concurrent_link_number(parameters.concurrent_link_number), _priorities(parameters.priorities) {}

  virtual ~ProcessStateMachine() = default;

  void add_new_vehicle(const artis::traffic::core::Time &t, const typename Types::vehicle_type &vehicle);

  void build(const std::shared_ptr<ProcessStateMachine<Types, Parameters, StateType>> &machine);

  double final_position(const artis::traffic::core::Time &t, size_t index) const;

  unsigned int get_concurrent_link_number() const { return _concurrent_link_number; }

  double get_max_speed() const { return _max_speed; }

  const std::vector<bool> &get_priorities() const { return _priorities; }

  bool is_full(const artis::traffic::core::Time &t) const;

  void next_vehicle_state(const artis::traffic::core::Time &t,
                          const typename std::deque<typename Types::vehicle_entry_type>::iterator &it,
                          bool go, double node_next_opening_time, double node_next_closing_time);

  void output_vehicle(const typename Types::vehicle_type &vehicle);

  std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator>
  waked_vehicles(const artis::traffic::core::Time &t);

// types
  DECLARE_STATE_TRANSITION_TYPES(ProcessStateMachine)

private:
// parameters
  double _length;
  double _max_speed;
  unsigned int _concurrent_link_number;
  std::vector<bool> _priorities;
};

DEFINE_STATE_MACHINE_STATE(IdleState, Types::state_IDs_type::IDLE, ProcessStateMachine)

DEFINE_STATE_MACHINE_STATE_WITH_TA(ProcessVehiclesState, Types::state_IDs_type::PROCESS_VEHICLES, ProcessStateMachine)

template<class Types, class Parameters, class StateType>
artis::traffic::core::Time
ProcessVehiclesState<Types, Parameters, StateType>::ta(const artis::traffic::core::Time &t) const {
  const std::deque<typename Types::vehicle_entry_type> &vehicles = this->_machine->state_()._vehicles;

  if (vehicles.empty()) {
    return artis::common::DoubleTime::infinity;
  } else {
    auto it = vehicles.cbegin();

    if (std::abs(it->next_time() - t) == 0 and it->states().back().state() != VehicleData::State::STOPPED and
        this->_machine->get_concurrent_link_number() > 0 and
        it->critical_position(t, this->_machine->final_position(t, it->vehicle().index()))) {
      return artis::common::DoubleTime::infinity;
    } else {
      double min_next_time = std::min_element(vehicles.cbegin(), vehicles.cend(),
                                              [](const typename Types::vehicle_entry_type &e1,
                                                 const typename Types::vehicle_entry_type &e2) {
                                                return e1.next_time() < e2.next_time();
                                              })->next_time();

      return std::abs(min_next_time - t) < 1e-6 ? 0 : min_next_time - t;
    }
  }
}

DEFINE_STATE_MACHINE_TRANSITION(ProcessT1, ProcessStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
void ProcessT1<Types, Parameters, StateType>::action(const artis::traffic::core::Time &t,
                                                     const artis::common::event::Value &value) {
  typename Types::vehicle_type vehicle;

  value(vehicle);
  this->_machine->add_new_vehicle(t, vehicle);
  if (this->_machine->is_full(t)) {
    this->_machine->state_()._full = true;
  }
}

template<class Types, class Parameters, class StateType>
bool ProcessT1<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::NEW_VEHICLE;
}

template<class Types, class Parameters, class StateType>
typename ProcessStateMachine<Types, Parameters, StateType>::Events
ProcessT1<Types, Parameters, StateType>::output(const artis::traffic::core::Time &t) const {
  typename ProcessStateMachine<Types, Parameters, StateType>::Events events{};

  if (this->_machine->is_full(t)) {
    events.internals.push_back(
      typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{
        {Types::event_IDs_type::FULL, {}},
        Types::state_machine_IDs_type::OPEN_CLOSE});
  } else {
    auto it = this->_machine->state_()._vehicles.end() - 1;
    double duration = 0;
    if (it->states().back().state() == VehicleData::State::RUNNING) {
      duration = (it->vehicle().gap() + it->vehicle().length()) / it->vehicle().max_speed();
    } else {
      double speed_difference = this->_machine->get_max_speed() - it->states().back().initial_speed();
      double acceleration_duration = speed_difference / it->vehicle().acceleration();
      double future_position =
        (it->states().back().initial_speed() + 0.5 * it->vehicle().acceleration() * acceleration_duration) *
        acceleration_duration;
      if ((it->vehicle().gap() + it->vehicle().length()) - future_position > 1e-6) {
        double position_difference = (it->vehicle().gap() + it->vehicle().length()) - future_position;
        duration = acceleration_duration + position_difference / this->_machine->get_max_speed();
      } else {
        duration = acceleration_duration;
      }
    }
    events.internals.push_back(
      typename ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{
        {Types::event_IDs_type::CLOSE, (artis::traffic::core::Time) duration},
        Types::state_machine_IDs_type::OPEN_CLOSE});
  }
  return events;
}

DEFINE_STATE_MACHINE_TRANSITION(ProcessT2, ProcessStateMachine, NO_ATTRIBUTE, true, false, false, true, true)

DEFINE_STATE_MACHINE_TRANSITION(ProcessT3, ProcessStateMachine, NO_ATTRIBUTE, true, true, false, false, false)

template<class Types, class Parameters, class StateType>
bool ProcessT3<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::DOWN_OPEN;
}

DEFINE_STATE_MACHINE_TRANSITION(ProcessT4, ProcessStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
bool ProcessT4<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::RECEIVE_STATE;
}

DEFINE_STATE_MACHINE_TRANSITION(ProcessT5, ProcessStateMachine, NO_ATTRIBUTE, true, true, false, false, false)

template<class Types, class Parameters, class StateType>
bool ProcessT5<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::DOWN_CLOSE;
}

}

#include <artis-traffic/micro/core/link-state-machine/ProcessStateMachine.tpp>

#endif