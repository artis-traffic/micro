/**
 * @file artis-traffic/micro/core/link-state-machine/AnswerStateStateMachine.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/link-state-machine/AnswerStateStateMachine.hpp>
#include <artis-traffic/micro/core/VehicleData.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
void AnswerStateStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<AnswerStateStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitStateDataState<Types, Parameters, StateType>(machine));
  this->state(new SendStateDataState<Types, Parameters, StateType>(machine));
  this->state(new ClearStateDataState<Types, Parameters, StateType>(machine));

  this->initial_state(Types::state_IDs_type::WAIT_STATE_DATA);

  this->transition(Types::state_IDs_type::WAIT_STATE_DATA,
                   Types::state_IDs_type::SEND_STATE_DATA,
                   new AnswerStateTransitionGetState<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::SEND_STATE_DATA,
                   Types::state_IDs_type::CLEAR_STATE_DATA,
                   new AnswerStateTransitionSendStateData<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::CLEAR_STATE_DATA,
                   Types::state_IDs_type::WAIT_STATE_DATA,
                   new AnswerStateTransitionClearStateData<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::CLEAR_STATE_DATA,
                   Types::state_IDs_type::SEND_STATE_DATA,
                   new AnswerStateTransitionGetState<Types, Parameters, StateType>(machine));
}

template<class Types, class Parameters, class StateType>
void AnswerStateTransitionGetState<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                                         const artis::common::event::Value &value) {
  std::vector<unsigned int> indexes{};
  value(indexes);
  this->_machine->state_()._concurrents_answer_index = indexes;
}

template<class Types, class Parameters, class StateType>
bool AnswerStateTransitionGetState<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */,
                                                                        int event) {
  return event == Types::event_IDs_type::GET_STATE;
}

template<class Types, class Parameters, class StateType>
void AnswerStateTransitionSendStateData<Types, Parameters, StateType>::action(
  const artis::traffic::core::Time &t,
  const artis::common::event::Value & /* value */) {
  const auto &state = this->_machine->root().template state_<ProcessState<Types>>(Types::state_machine_IDs_type::PROCESS);

  if (state._vehicles.empty()) {
    this->_machine->state_()._state_data = {{}, -1, -1, 0};
  } else {
    const typename Types::vehicle_entry_type &entry = state._vehicles.front();
    const typename Types::vehicle_type &vehicle = entry.vehicle();
    double distance_to_final = this->_machine->get_length() - entry.compute_position(t);
    double duration_to_final = -1;

    switch (entry.states().back().state()) {
      case VehicleData::State::RUNNING: {
        duration_to_final = distance_to_final / vehicle.speed();
        break;
      }
      case VehicleData::State::IN_ACCELERATION: {
        double delta = vehicle.speed() * vehicle.speed() + 2 * vehicle.acceleration() * distance_to_final;

        duration_to_final = (-vehicle.speed() + std::sqrt(delta)) / vehicle.acceleration();
        break;
      }
      default:
        duration_to_final = -1;
    }
    this->_machine->state_()._state_data =
      state_data<typename Types::vehicle_type>{vehicle, distance_to_final, duration_to_final, 0};
  }
}

template<class Types, class Parameters, class StateType>
typename AnswerStateStateMachine<Types, Parameters, StateType>::Events
AnswerStateTransitionSendStateData<Types, Parameters, StateType>::output(
  const artis::traffic::core::Time & /* t */) const {
  typename AnswerStateStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename AnswerStateStateMachine<Types, Parameters, StateType>::ExternalEvent{
      Types::event_IDs_type::ANSWER_STATE_DATA, this->_machine->state_()._state_data});
  return events;
}

template<class Types, class Parameters, class StateType>
void
AnswerStateTransitionClearStateData<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                                          const artis::common::event::Value & /* value */) {
  this->_machine->state_()._concurrents_answer_index.clear();
}

}
