/**
 * @file artis-traffic/micro/core/link-state-machine/LinkIDs.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_LINK_IDS_HPP
#define ARTIS_TRAFFIC_MICRO_LINK_IDS_HPP

namespace artis::traffic::micro::core {

struct StateMachineIDs {
  enum values {
    ANSWER_STATE, DOWN_OPEN_CLOSE, INPUT, OPEN_CLOSE, OUTPUT, PROCESS, STATE, LAST = STATE
  };
};

struct StateIDs {
  enum values {
    IDLE,
    PROCESS_VEHICLES,
    WAIT_VEHICLE,
    OPENING,
    OPEN,
    CLOSE,
    WAIT_READY_TO_EXIT,
    WAIT_OPEN,
    WAIT_OPEN_CLOSE,
    WAIT_STATE_DATA,
    SEND_STATE_DATA,
    CLEAR_STATE_DATA,
    WAIT_STATE,
    LAST = WAIT_STATE
  };
};

struct EventIDs {
  enum values {
    IN_VEHICLE, NEW_VEHICLE, OPEN, CLOSE, READY_TO_EXIT, FULL, NO_FULL, SEND_VEHICLE, DOWN_CLOSE, DOWN_OPEN,
    GET_STATE, ANSWER_STATE_DATA, ARRIVED, SEND_STATE, RECEIVE_STATE, LAST = RECEIVE_STATE
  };
};

}

#endif //ARTIS_TRAFFIC_MICRO_LINK_IDS_HPP
