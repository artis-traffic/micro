/**
 * @file artis-traffic/micro/core/link-state-machine/LinkRootStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_LINK_ROOT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_LINK_ROOT_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/Data.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkStates.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkStateMachine.hpp>

#include <artis-traffic/micro/core/link-state-machine/AnswerStateStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/DownOpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/InputStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/OpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/OutputStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/ProcessStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/StateStateMachine.hpp>

#include <artis-star-addons/utils/RootStateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters>
using StateMachineTypes = std::tuple<
  AnswerStateStateMachine<Types, Parameters, AnswerStateState<Types>>,
  DownOpenCloseStateMachine<Types, Parameters, DownOpenCloseState>,
  InputStateMachine<Types, Parameters, InputState>,
  OpenCloseStateMachine<Types, Parameters, OpenCloseState>,
  OutputStateMachine<Types, Parameters, OutputState<Types>>,
  ProcessStateMachine<Types, Parameters, ProcessState<Types>>,
  StateStateMachine<Types, Parameters, StateState<Types>>>;

DEFINE_ROOT_STATE_MACHINE(LinkRootStateMachine, StateMachineTypes)

}

#endif //ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_LINK_ROOT_STATE_MACHINE_HPP
