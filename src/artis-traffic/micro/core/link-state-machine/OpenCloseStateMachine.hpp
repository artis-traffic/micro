/**
 * @file artis-traffic/micro/core/link-state-machine/OpenCloseStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_OPEN_CLOSE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_OPEN_CLOSE_STATE_MACHINE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>
#include <artis-star-addons/utils/StateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class OpenCloseStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  OpenCloseStateMachine(const typename Types::root_state_machine_type &root, const Parameters & /* parameters */)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}) {}

  void build(const std::shared_ptr<OpenCloseStateMachine<Types, Parameters, StateType>> &machine);

// types
  DECLARE_STATE_TRANSITION_TYPES(OpenCloseStateMachine)
};

DEFINE_STATE_MACHINE_STATE_WITH_NULL_TA(OpeningState, Types::state_IDs_type::OPENING, OpenCloseStateMachine)

DEFINE_STATE_MACHINE_STATE(OpenState, Types::state_IDs_type::OPEN, OpenCloseStateMachine)

DEFINE_STATE_MACHINE_STATE_WITH_TA(CloseState, Types::state_IDs_type::CLOSE, OpenCloseStateMachine)

template<class Types, class Parameters, class StateType>
artis::traffic::core::Time
CloseState<Types, Parameters, StateType>::ta(const artis::traffic::core::Time & /* t */) const {
  return this->_machine->state_()._duration;
}

DEFINE_STATE_MACHINE_TRANSITION(OpenCloseT1, OpenCloseStateMachine, NO_ATTRIBUTE, false, false, false, true, true)

template<class Types, class Parameters, class StateType>
typename OpenCloseStateMachine<Types, Parameters, StateType>::Events
OpenCloseT1<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename OpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename OpenCloseStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::OPEN, {}});
  return events;
}

DEFINE_STATE_MACHINE_TRANSITION(OpenCloseT2, OpenCloseStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
bool OpenCloseT2<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::CLOSE;
}

template<class Types, class Parameters, class StateType>
void OpenCloseT2<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                       const artis::common::event::Value &value) {
  value(this->_machine->state_()._duration);
}

template<class Types, class Parameters, class StateType>
typename OpenCloseStateMachine<Types, Parameters, StateType>::Events
OpenCloseT2<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename OpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename OpenCloseStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::CLOSE,
                                                                                this->_machine->state_()._duration});
  return events;
}

DEFINE_STATE_MACHINE_TRANSITION(OpenCloseT3, OpenCloseStateMachine, NO_ATTRIBUTE, false, false, false, true, true)

template<class Types, class Parameters, class StateType>
typename OpenCloseStateMachine<Types, Parameters, StateType>::Events
OpenCloseT3<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename OpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename OpenCloseStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::OPEN, {}});
  return events;
}

DEFINE_STATE_MACHINE_TRANSITION(OpenCloseT4, OpenCloseStateMachine, NO_ATTRIBUTE, true, true, false, false, true)

template<class Types, class Parameters, class StateType>
bool OpenCloseT4<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::FULL;
}

template<class Types, class Parameters, class StateType>
void OpenCloseT4<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                       const artis::common::event::Value & /* value */) {
  this->_machine->state_()._duration = artis::common::DoubleTime::infinity;
}

template<class Types, class Parameters, class StateType>
typename OpenCloseStateMachine<Types, Parameters, StateType>::Events
OpenCloseT4<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename OpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename OpenCloseStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::CLOSE,
                                                                                this->_machine->state_()._duration});
  return events;
}

DEFINE_STATE_MACHINE_TRANSITION(OpenCloseT5, OpenCloseStateMachine, NO_ATTRIBUTE, false, true, false, false, true)

template<class Types, class Parameters, class StateType>
bool OpenCloseT5<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::NO_FULL;
}

template<class Types, class Parameters, class StateType>
typename OpenCloseStateMachine<Types, Parameters, StateType>::Events
OpenCloseT5<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename OpenCloseStateMachine<Types, Parameters, StateType>::Events events{};

  events.externals.push_back(
    typename OpenCloseStateMachine<Types, Parameters, StateType>::ExternalEvent{Types::event_IDs_type::OPEN, {}});
  return events;
}

template<class Types, class Parameters, class StateType>
void
OpenCloseStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<OpenCloseStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new OpeningState<Types, Parameters, StateType>(machine));
  this->state(new OpenState<Types, Parameters, StateType>(machine));
  this->state(new CloseState<Types, Parameters, StateType>(machine));
  this->initial_state(Types::state_IDs_type::OPENING);
  this->transition(Types::state_IDs_type::OPENING, Types::state_IDs_type::OPEN,
                   new OpenCloseT1<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::OPEN, Types::state_IDs_type::CLOSE,
                   new OpenCloseT2<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::OPEN, Types::state_IDs_type::CLOSE,
                   new OpenCloseT4<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::CLOSE, Types::state_IDs_type::OPEN,
                   new OpenCloseT3<Types, Parameters, StateType>(machine));
  this->transition(Types::state_IDs_type::CLOSE, Types::state_IDs_type::OPEN,
                   new OpenCloseT5<Types, Parameters, StateType>(machine));
}

}

#endif