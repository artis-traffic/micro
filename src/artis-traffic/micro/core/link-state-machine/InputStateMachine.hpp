/**
 * @file artis-traffic/micro/core/link-state-machine/InputStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_INPUT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_LINK_STATE_MACHINE_INPUT_STATE_MACHINE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Data.hpp>
#include <artis-star-addons/utils/StateMachine.hpp>

namespace artis::traffic::micro::core {

template<class Types, class Parameters, class StateType>
class InputStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  InputStateMachine(const typename Types::root_state_machine_type &root, const Parameters & /* parameters */)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}) {}

  void build(const std::shared_ptr<InputStateMachine<Types, Parameters, StateType>> &machine);

// types
  DECLARE_STATE_TRANSITION_TYPES(InputStateMachine)
};

DEFINE_STATE_MACHINE_STATE(WaitVehicleState, Types::state_IDs_type::WAIT_VEHICLE, InputStateMachine)

DEFINE_STATE_MACHINE_TRANSITION(InputT1, InputStateMachine, ATTRIBUTE(typename Types::vehicle_type, vehicle), true,
                                true, false, false, true)

template<class Types, class Parameters, class StateType>
void InputT1<Types, Parameters, StateType>::action(const artis::traffic::core::Time & /* t */,
                                                   const artis::common::event::Value &value) {
  value(vehicle);
}

template<class Types, class Parameters, class StateType>
bool InputT1<Types, Parameters, StateType>::event(const artis::traffic::core::Time & /* t */, int event) {
  return event == Types::event_IDs_type::IN_VEHICLE;
}

template<class Types, class Parameters, class StateType>
typename InputStateMachine<Types, Parameters, StateType>::Events
InputT1<Types, Parameters, StateType>::output(const artis::traffic::core::Time & /* t */) const {
  typename InputStateMachine<Types, Parameters, StateType>::Events events{};

  events.internals.push_back(
    typename InputStateMachine<Types, Parameters, StateType>::InternalEvent{
      {Types::event_IDs_type::NEW_VEHICLE, vehicle}, Types::state_machine_IDs_type::PROCESS});
  return events;
}

template<class Types, class Parameters, class StateType>
void InputStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<InputStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitVehicleState<Types, Parameters, StateType>(machine));
  this->initial_state(Types::state_IDs_type::WAIT_VEHICLE);
  this->transition(Types::state_IDs_type::WAIT_VEHICLE, Types::state_IDs_type::WAIT_VEHICLE,
                   new InputT1<Types, Parameters, StateType>(machine));
}

}

#endif