/**
 * @file artis-traffic/micro/core/Junction.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_JUNCTION_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_JUNCTION_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>

#include "Data.hpp"

namespace artis::traffic::micro::core {

struct JunctionParameters {
  unsigned int in_number;
  unsigned int out_number;
};

template<class Vehicle>
class Junction
  : public artis::pdevs::Dynamics<artis::common::DoubleTime, Junction<Vehicle>, JunctionParameters> {
public:
  struct inputs {
    enum values {
      IN, OPEN = 1000, CLOSE = 1500, STATE = 2000
    };
  };

  struct outputs {
    enum values {
      OUT, OPEN = 1000, CLOSE = 1500, STATE = 2000
    };
  };

  struct vars {
    enum values {
      VEHICLE_NUMBER, VEHICLE_INDEX
    };
  };

  Junction(const std::string &name,
           const artis::pdevs::Context<artis::common::DoubleTime, Junction<Vehicle>, JunctionParameters> &context)
    :
    artis::pdevs::Dynamics<artis::common::DoubleTime, Junction<Vehicle>, JunctionParameters>(
      name, context),
    _in_number(context.parameters().in_number),
    _out_number(context.parameters().out_number) {
    for (unsigned int i = 0; i < _in_number; ++i) {
      std::stringstream ss_in;
      ss_in << "in_" << (i + 1);
      this->input_port({inputs::IN + i, ss_in.str()});
    }
    for (unsigned int i = 0; i < _out_number; ++i) {
      std::stringstream ss_out;
      ss_out << "out_" << (i + 1);
      this->output_port({outputs::OUT + i, ss_out.str()});
    }

    for (unsigned int i = 0; i < _out_number; ++i) {
      std::stringstream ss_open;
      ss_open << "open_" << (i + 1);
      this->input_port({inputs::OPEN + i, ss_open.str()});
      std::stringstream ss_close;
      ss_close << "close_" << (i + 1);
      this->input_port({inputs::CLOSE + i, ss_close.str()});
    }
    this->input_port({inputs::STATE, "state"});

    for (unsigned int i = 0; i < _in_number; ++i) {
      std::stringstream ss_close;
      ss_close << "close_" << (i + 1);
      this->output_port({outputs::CLOSE + i, ss_close.str()});
      std::stringstream ss_open;
      ss_open << "open_" << (i + 1);
      this->output_port({outputs::OPEN + i, ss_open.str()});
    }
    this->output_port({outputs::STATE, "state"});

    this->observables({{vars::VEHICLE_NUMBER, "vehicle_number"},
                       {vars::VEHICLE_INDEX,  "vehicle_index"}});
  }

  ~Junction() override = default;

  void dconf(const artis::traffic::core::Time & /* t* */, const artis::traffic::core::Time & /* e */,
             const artis::traffic::core::Bag & /* bag */) override;

  void dint(const artis::traffic::core::Time & /* t */) override;

  void dext(const artis::traffic::core::Time & /* t */, const artis::traffic::core::Time & /* e */,
            const artis::traffic::core::Bag & /* bag*/) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Time ta(const artis::traffic::core::Time & /* t */) const override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t, unsigned int index) const override;

private:
  struct VehiclePhase {
    enum values {
      INIT,
      WAIT,
      SEND_VEHICLE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case WAIT:
          return "WAIT";
        case SEND_VEHICLE:
          return "SEND_VEHICLE";
      }
      return "";
    }
  };

  struct OpenPhase {
    enum values {
      INIT,
      WAIT,
      SEND_OPEN
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case WAIT:
          return "WAIT";
        case SEND_OPEN:
          return "SEND_OPEN";
      }
      return "";
    }
  };

  struct ClosePhase {
    enum values {
      INIT,
      WAIT,
      SEND_CLOSE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case WAIT:
          return "WAIT";
        case SEND_CLOSE:
          return "SEND_CLOSE";
      }
      return "";
    }
  };

  // parameters
  unsigned int _in_number;
  unsigned int _out_number;

  // vehicle state machine
  typename VehiclePhase::values _vehicle_phase;
  std::unique_ptr<Vehicle> _vehicle;
  unsigned int _out_index;

  // open state machine
  typename OpenPhase::values _open_phase;
  std::deque<int> _open_send_indexes;

  // close state machine
  typename ClosePhase::values _close_phase;
  std::deque<close_data> _next_close_data;
  std::deque<bool> _next_close;
  std::deque<int> _close_send_indexes;
};

}

#include <artis-traffic/micro/core/Junction.tpp>

#endif