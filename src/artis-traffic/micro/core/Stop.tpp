/**
 * @file artis-traffic/micro/core/Stop.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/core/Stop.hpp>
#include <artis-traffic/micro/core/Vehicle.hpp>

namespace artis::traffic::micro::core {

template<class Model, class Parameters, class Types>
void StopImplementation<Model, Parameters, Types>::dext(const artis::traffic::core::Time &t,
                                   const artis::traffic::core::Time & /* e */,
                                   const artis::traffic::core::Bag &bag) {

//    std::cout << "[" << get_full_name() << "] dext at " << t << " => " << bag.to_string() << std::endl;

  std::vector<unsigned int> indexes;

  std::for_each(bag.begin(), bag.end(), [&indexes](const artis::traffic::core::ExternalEvent &event) {
    if (event.port_index() >= inputs::STATE and event.port_index() < inputs::STATE_DATA) {
      unsigned int index = event.port_index() - inputs::STATE;

//        std::cout << " => ANSWER STATE => " << index << std::endl;

      indexes.push_back(index);
    }
  });
  if (not indexes.empty()) {
    this->root().transition(t, Types::state_machine_IDs_type::ANSWER_STATE,
                            typename Types::state_machine_type::ExternalEvent{StopEventIDs::GET_STATE, indexes});
  }
  std::for_each(bag.begin(), bag.end(),
                [t, this](
                  const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN)) {
                      typename Types::vehicle_type vehicle;

                    event.data()(vehicle);
//                      std::cout << " => STOP ADD VEHICLE = " << vehicle.to_string() << std::endl;
                    this->root().transition(t, Types::state_machine_IDs_type::PROCESS_VEHICLE,
                                            typename Types::state_machine_type::ExternalEvent{StopEventIDs::IN,
                                                                                              {vehicle}});
                  } else if (event.on_port(inputs::ARRIVED)) {
                    const auto &state = this->root().template state_<ProcessVehicleState<Types>>(
                      Types::state_machine_IDs_type::PROCESS_VEHICLE);

//                      std::cout << " => STOP ARRIVED  " << std::endl;
                    if (state._arrived) {
                      this->root().transition(t, Types::state_machine_IDs_type::PROCESS_VEHICLE,
                                              typename Types::state_machine_type::ExternalEvent{StopEventIDs::ARRIVED,
                                                                                                {}});
                    } else {
                      this->root().transition(t, Types::state_machine_IDs_type::PROCESS_VEHICLE,
                                              typename Types::state_machine_type::ExternalEvent{StopEventIDs::ARRIVED,
                                                                                                {}});
                    }
                  } else if (event.on_port(inputs::CLOSE)) {
                    close_data data{};

                    event.data()(data);

//                      std::cout << " => CLOSE = " << data.index << std::endl;

                    this->root().transition(t, Types::state_machine_IDs_type::DOWN_OPEN_CLOSE,
                                            typename Types::state_machine_type::ExternalEvent{StopEventIDs::DOWN_CLOSE,
                                                                                              data});
                  } else if (event.on_port(inputs::OPEN)) {
                    open_data data{};

                    event.data()(data);

//                      std::cout << " => OPEN = " << data.index << std::endl;

                    this->root().transition(t, Types::state_machine_IDs_type::DOWN_OPEN_CLOSE,
                                            typename Types::state_machine_type::ExternalEvent{
                                              StopEventIDs::DOWN_OPEN, data});
                  } else if (event.port_index() >= inputs::STATE_DATA and event.port_index() < inputs::ARRIVED) {
                    unsigned int index = event.port_index() - inputs::STATE_DATA;
                    state_data<typename Types::vehicle_type> data{};

                    event.data()(data);

//                      std::cout << " => RECEIVE STATE DATA => " << data.index << std::endl;

                    data.index = index;
                    this->root().transition(t, Types::state_machine_IDs_type::STATE_GO,
                                            typename Types::state_machine_type::ExternalEvent{
                                              StopEventIDs::GET_STATE_DATA, data});
                  }
                });
}

template<class Model, class Parameters, class Types>
artis::traffic::core::Bag StopImplementation<Model, Parameters, Types>::lambda(const artis::traffic::core::Time &t) const {
  artis::traffic::core::Bag bag;
  const typename Types::root_state_machine_type::external_events_type &events = this->root().outbox();

//    std::cout << "[" << get_full_name() << "] lambda at " << t << std::endl;

  std::for_each(events.cbegin(), events.cend(), [t, this, &bag](const auto &e) {
    switch (e.id) {
      case StopEventIDs::CLOSE: {
        std::vector<int> indexes;

        e.data(indexes);

        for (int &index: indexes) {

//            std::cout << "[" << get_full_name() << "] at " << t << " SEND CLOSE: " << indexes[i] << std::endl;

          bag.push_back(artis::traffic::core::ExternalEvent(
            outputs::CLOSE, close_data{close_type::ABSOLUTE, artis::common::DoubleTime::infinity, index}));
        }
        break;
      }
      case StopEventIDs::OPEN: {
        std::vector<int> indexes;

        e.data(indexes);

        for (int &index: indexes) {

//            std::cout << "[" << get_full_name() << "] at " << t << " SEND OPEN: " << indexes[i] << std::endl;

          bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN, open_data{t, index}));
        }
        break;
      }
      case StopEventIDs::SEND_VEHICLE: {
          typename Types::vehicle_type vehicle;

        e.data(vehicle);

//          std::cout << " => SEND VEHICLE = " << vehicle.to_string() << std::endl;

        bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, vehicle));
        break;
      }
      case StopEventIDs::STATE: {
//          std::cout << " => SEND STATE " << std::endl;
        for (unsigned int i = 0; i < _concurrent_link_number + _concurrent_stop_number; i++) {
          bag.push_back(artis::traffic::core::ExternalEvent(outputs::STATE + i));
        }
        break;
      }
      case StopEventIDs::ANSWER_STATE_DATA: {
        const auto &state = this->root().template state_<StopAnswerStateState<Types>>(Types::state_machine_IDs_type::ANSWER_STATE);

        std::for_each(state._concurrents_answer_index.cbegin(), state._concurrents_answer_index.cend(),
                      [state, &bag](const auto &index) {

//                          std::cout << " => ANSWER_STATE_DATA => " << index << std::endl;

                        bag.push_back(
                          artis::traffic::core::ExternalEvent(outputs::STATE_DATA + index, state._state_data));
                      });
        break;
      }
      default: {
      }
    }
  });
  return bag;
}

template<class Model, class Parameters, class Types>
artis::common::event::Value
StopImplementation<Model, Parameters, Types>::observe(const artis::traffic::core::Time & /* t */,
                                 unsigned int index) const {
  const auto &state = this->root().template state_<ProcessVehicleState<Types>>(
    Types::state_machine_IDs_type::PROCESS_VEHICLE);

  switch (index) {
    case vars::VEHICLE_NUMBER:
      return (unsigned int) (state._arrived ? 1 : 0);
    case vars::VEHICLE_INDEX:
      return (int) (state._arrived ? state._vehicle.index() : -1);
    default:
      return {};
  }
}

}