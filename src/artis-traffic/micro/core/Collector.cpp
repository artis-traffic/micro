/**
 * @file artis-traffic/micro/core/Collector.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/core/Collector.hpp>

namespace artis::traffic::micro::core {

void Collector::dconf(const artis::traffic::core::Time &t,
                      const artis::traffic::core::Time &e,
                      const artis::traffic::core::Bag &bag) {
  dint(t);
  dext(t, e, bag);
}

void Collector::dint(const artis::traffic::core::Time & /* t */) {
  switch (_phase) {
    case Phase::INIT: {
      _sigma = artis::common::DoubleTime::infinity;
      break;
    }
    case Phase::OPEN:

      assert(false);

      break;
    case Phase::CLOSE: {
      _phase = Phase::SEND;
      _sigma = 0;
      break;
    }
    case Phase::SEND: {
      _phase = Phase::SEND_OPEN;
      _sigma = 0;
      break;
    }
    case Phase::SEND_OPEN: {
      _arrived = false;
      _phase = Phase::OPEN;
      _sigma = artis::common::DoubleTime::infinity;
      break;
    }
    case Phase::SEND_CLOSE: {
      _phase = Phase::CLOSE;
      if (_arrived) {
        _sigma = _occupied_duration;
      } else {
        _sigma = artis::common::DoubleTime::infinity;
      }
      break;
    }
  }
}

void Collector::dext(const artis::traffic::core::Time & /* t */,
                     const artis::traffic::core::Time & /* e */,
                     const artis::traffic::core::Bag &bag) {
  std::for_each(bag.begin(), bag.end(),
                [this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN_PRIMARY) or event.on_port(inputs::IN_SECONDARY)) {

                    assert(_phase == Phase::OPEN and not _arrived);

                    event.data()(_vehicle);
                    _arrived = true;
                    _phase = Phase::SEND_CLOSE;
                    _sigma = 0;
                  } else if (event.on_port(inputs::CLOSE)) {

                    assert(not _next_close);

                    _next_close = true;
                    _phase = Phase::SEND_CLOSE;
                    _sigma = 0;
                  } else if (event.on_port(inputs::OPEN)) {

                    assert(_next_close);

                    _next_close = false;
                    if (_arrived) {
                      _phase = Phase::SEND;
                      _sigma = 0;
                    } else {
                      _phase = Phase::SEND_OPEN;
                      _sigma = 0;
                    }
                  }
                });
}

void Collector::start(const artis::traffic::core::Time & /* t */) {
  _arrived = false;
  _next_close = true;
  _sigma = 0;
  _phase = Phase::INIT;
}

artis::traffic::core::Time Collector::ta(const artis::traffic::core::Time & /* t */) const {
  return _sigma;
}

artis::traffic::core::Bag Collector::lambda(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Bag bag;

  if (_phase == Phase::SEND_CLOSE) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::CLOSE));
  } else if (_phase == Phase::SEND) {

    assert(_arrived);

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT, _vehicle));
  } else if (_phase == Phase::SEND_OPEN) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OPEN));
  }
  return bag;
}

artis::common::event::Value
Collector::observe(const artis::traffic::core::Time & /* t */, unsigned int index) const {
  switch (index) {
    case vars::OPEN:
      return (bool) (_phase == Phase::OPEN);
    case vars::CLOSE:
      return (bool) (_phase == Phase::CLOSE);
    case vars::PHASE:
      return Phase::to_string(_phase);
    case vars::VEHICLE_NUMBER:
      return (unsigned int) (_arrived ? 1 : 0);
    default:
      return {};
  }
}

}