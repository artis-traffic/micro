/**
 * @file artis-traffic/micro/core/VehicleData.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_CORE_VEHICLE_DATA_HPP
#define ARTIS_TRAFFIC_MICRO_CORE_VEHICLE_DATA_HPP

#include <string>

namespace artis::traffic::micro::core {

struct VehicleData {
  struct State {
    enum values {
      RUNNING, IN_ACCELERATION, IN_DECELERATION, STOPPED, RESTART, HALT, TRANSITION_STOPPED
    };

    static std::string to_string(const State::values &value) {
      switch (value) {
        case RUNNING:
          return "RUNNING";
        case IN_ACCELERATION:
          return "IN_ACCELERATION";
        case IN_DECELERATION:
          return "IN_DECELERATION";
        case STOPPED:
          return "STOPPED";
        case RESTART:
          return "RESTART";
        case HALT:
          return "HALT";
        case TRANSITION_STOPPED:
          return "TRANSITION_STOPPED";
        default:
          return "";
      }
    }
  };

  State::values state;

  std::string to_json() const {
    return "{ \"state\": \"" + State::to_string(state) + "\" }";
  }

  std::string to_string() const {
    return State::to_string(state);
  }
};

}

#endif